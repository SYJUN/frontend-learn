;(function ($) {
    var Carousel = function (poster) {
        var _t = this;
        //保存单个旋转木马对象
        this.poster = poster;
        this.posterItemMain = poster.find('ul.poster-list');
        this.nextBtn = poster.find('div.poster-next-btn');
        this.prevBtn = poster.find('div.poster-prev-btn');

        this.posterItems = poster.find('li.poster-item');
        if(this.posterItems.size()%2 == 0){
            this.posterItemMain.append(this.posterItems.eq(0).clone());
            this.posterItems = this.posterItemMain.children();
        }

        this.posterFirstItem = this.posterItems.first();
        this.posterLastItem = this.posterItems.last();

        this.rotateFlag = true;


        //默认配置参数
        this.setting = {
            "width": 1000,       //幻灯片的宽度
            "height": 270,       //幻灯片的高度
            "posterWidth": 640,  //幻灯片第一帧的宽度
            "posterHeight": 270, //幻灯片第一帧的高度
            "scale": 0.9,           //记录显示比例关系
            "speed": 500,           //切换速度
            "autoPlay":false,       //是否自动播放
            "delay":5000,           //间隔多少秒播放一次
            "verticalAlign": "middle"       //垂直对齐方式 [top | bottom]，默认为:垂直中间对齐(middle)
        };
        $.extend(this.setting, this.getSetting());

        //设置配置参数值
        this.setSettingValue();
        this.setPosterPos();

        //左旋转按钮
        this.nextBtn.click(function(){
            if(_t.rotateFlag){
                _t.rotateFlag = false;
                _t.carouseRotate('left');
            }
        });
        //右旋转按钮
        this.prevBtn.click(function(){
            if(_t.rotateFlag){
                _t.rotateFlag = false;
                _t.carouseRotate('right');
            }
        });
        //是否开启自动播放
        if(this.setting.autoPlay){
            this.autoPlay();
            this.poster.hover(function(){
                window.clearInterval(_t.timer);
            },function(){
                _t.autoPlay();
            });
        }
    };
    Carousel.prototype = {
        autoPlay:function(){
            var _t = this;
            this.timer = window.setInterval(function(){
                _t.nextBtn.click();
            },this.setting.delay);
        },
        //旋转
        carouseRotate:function(dir){
            var _t = this,
                zIndexArr = [];
            if(dir === 'left'){
                this.posterItems.each(function(){
                    var self = $(this),
                        prev = self.prev().get(0) ? self.prev() : _t.posterLastItem,
                        width = prev.width(),
                        height = prev.height(),
                        zIndex = prev.css('zIndex'),
                        opacity = prev.css('opacity'),
                        left = prev.css('left'),
                        top = prev.css('top');
                    zIndexArr.push(zIndex);
                    self.animate({
                        width : width,
                        height : height,
                        opacity:opacity,
                        left:left,
                        top:top
                    }, _t.setting.speed,function(){
                        _t.rotateFlag = true;
                    });
                });
                this.posterItems.each(function(i){
                    $(this).css('zIndex',zIndexArr[i]);
                });
            }else if(dir === 'right'){
                this.posterItems.each(function(){
                    var self = $(this),
                        next = self.next().get(0) ? self.next() : _t.posterFirstItem,
                        width = next.width(),
                        height = next.height(),
                        zIndex = next.css('zIndex'),
                        opacity = next.css('opacity'),
                        left = next.css('left'),
                        top = next.css('top');
                    zIndexArr.push(zIndex);
                    self.animate({
                        width : width,
                        height : height,
                        opacity:opacity,
                        left:left,
                        top:top
                    }, _t.setting.speed,function(){
                        _t.rotateFlag = true;
                    });
                });
                this.posterItems.each(function(i){
                    $(this).css('zIndex',zIndexArr[i]);
                });
            }
        },
        //设置剩余的帧的位置关系
        setPosterPos:function(){
            var _t = this,
                sliceItems = this.posterItems.slice(1),
                sliceSize = sliceItems.size()/ 2,
                rightSlice = sliceItems.slice(0,sliceSize),
                level = Math.floor(this.posterItems.size()/2),
                leftSlice = sliceItems.slice(sliceSize);
            //设置右边帧的位置关系和宽度高度top
            var rw = this.setting.posterWidth,
                rh = this.setting.posterHeight,
                gap = ((this.setting.width - this.setting.posterWidth)/2)/level,
                firstLeft = (this.setting.width - this.setting.posterWidth)/ 2,
                fixOffsetLeft = firstLeft + rw;
            //设置右边的位置关系
            rightSlice.each(function(i){
                level--;
                rw *= _t.setting.scale;
                rh *= _t.setting.scale;
                var j = i;
                $(this).css({
                    zIndex: level,
                    width: rw,
                    height: rh,
                    opacity: 1/(++j),
                    left: fixOffsetLeft + (++i) * gap - rw,
                    top:_t.setVerticalAlign(rh)
                });
            });
            //设置左边的位置关系
            var lw = rightSlice.last().width(),
                lh = rightSlice.last().height(),
                oloop = Math.floor(this.posterItems.size()/2);
            leftSlice.each(function(i){
                $(this).css({
                    zIndex: i,
                    width: lw,
                    height: lh,
                    opacity: 1/oloop,
                    left: i * gap,
                    top:_t.setVerticalAlign(lh)
                });
                lw = lw/_t.setting.scale;
                lh = lh/_t.setting.scale;
                oloop--;
            });
        },
        //设置垂直排列对齐
        setVerticalAlign:function(h){
            var verticalType = this.setting.verticalAlign,
                top = 0;
            switch (verticalType){
                case 'middle': top = (this.setting.height - h)/2; break;
                case 'top': top = 0; break;
                case 'bottom': top = this.setting.height - h; break;
                default:
                    top = top = (this.setting.height - h)/2; break;
            };
            return top;
        },
        //设置配置参数值去控制基本的宽度高度
        setSettingValue: function () {
            this.poster.css({
                width: this.setting.width,
                height: this.setting.height
            });
            this.posterItemMain.css({
                width: this.setting.width,
                height: this.setting.height
            });
            //计算上下切换按钮的宽度
            var w = (this.setting.width - this.setting.posterWidth)/2;
            this.nextBtn.css({
                width:w,
                height:this.setting.height,
                zIndex:Math.ceil(this.posterItems.size()/2)
            });
            this.prevBtn.css({
                width:w,
                height:this.setting.height,
                zIndex:Math.ceil(this.posterItems.size()/2)
            });

            this.posterFirstItem.css({
                width:this.setting.posterWidth,
                height:this.setting.posterHeight,
                left:w,
                zIndex:Math.floor(this.posterItems.size()/2)
            });
        },
        //获取人工配置参数
        getSetting: function () {
            var setting = this.poster.attr('data-setting');
            if (setting && setting != '') {
                return $.parseJSON(setting);
            } else {
                return {};
            }

        }
    };

    Carousel.init = function (posters) {
        var _t = this;
        posters.each(function () {
            new _t($(this));
        });
    };
    window['Carousel'] = Carousel;
})(jQuery);

