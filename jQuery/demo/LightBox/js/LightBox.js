;(function($){
    var LightBox = function(settings){
        var self = this;
        this.settings = {
            speed:500,
            maskOpacity:0.5
        };
        $.extend(this.settings,settings || {});

        //创建遮罩和弹出框
        this.popupMask = $('<div id="G-lightbox-mask"></div>');
        this.popupWin = $('<div id="G-lightbox-popup"></div>');

        //设置遮罩层的透明度
        this.popupMask.css({
            opacity:self.settings.maskOpacity,
            filter:'alpha(opacity='+ (self.settings.maskOpacity*100) +')'
        });

        //保存 body
        this.bodyNode = $(document.body);
        //渲染剩余的DOM 并且插入到 body
        this.renderDOM();

        this.picViewArea = this.popupWin.find('div.lightbox-pic-view');     //图片预览区域
        this.popupPic = this.popupWin.find('img.lightbox-image');           //图片
        this.picCaptionArea = this.popupWin.find('div.lightbox-pic-caption');   //图片描述区域
        this.nextBtn = this.popupWin.find('span.lightbox-next-btn');
        this.prevBtn = this.popupWin.find('span.lightbox-prev-btn');

        this.captionText = this.popupWin.find('p.lightbox-pic-desc');       //图片描述
        this.currentIndex = this.popupWin.find('span.lightbox-of-index');   //图片当前索引
        this.closeBtn = this.popupWin.find('span.lightbox-close-btn');      //关闭按钮

        //定义组名
        this.groupName = null;
        //定义一个空数组，用来存储组名数据
        this.groupData = [];        //放置同一组数据
        //准备开发事件委托，获取组数据
        this.bodyNode.on('click','.js-lightbox,*[data-role=lightbox]',function(e){
            //阻止事件冒泡
            e.stopPropagation();
            //获取当前组名
            var currentGroupName = $(this).attr('data-group');
            if(currentGroupName != self.groupName){
                self.groupName = currentGroupName;
                //根据当前组名获取同一组数据
                self.getGroup();
            }
            //初始化弹出
            self.initPopup($(this));
        });

        //关闭弹出功能
        this.popupMask.click(function(){
            $(this).fadeOut();
            self.popupWin.fadeOut();
            self.clear = false;
        });
        this.closeBtn.click(function(){
            self.popupMask.fadeOut();
            self.popupWin.fadeOut();
            self.clear = false;
        });

        //绑定上下切换按钮事件
        this.flag = true;
        this.nextBtn.hover(function(){
                                if(!$(this).hasClass('disabled') && self.groupData.length > 1){
                                    $(this).addClass('lightbox-next-btn-show');
                                }
                            },function(){
                                if(!$(this).hasClass('disabled') && self.groupData.length > 1){
                                    $(this).removeClass('lightbox-next-btn-show');
                                }
                            }).click(function(e){
                                if(!$(this).hasClass('disabled') && self.flag){
                                    self.flag = false;
                                    e.stopPropagation();
                                    self.goTo('next');
                                }
                            });
        this.prevBtn.hover(function(){
                                if(!$(this).hasClass('disabled') && self.groupData.length > 1){
                                    $(this).addClass('lightbox-prev-btn-show');
                                }
                            },function(){
                                if(!$(this).hasClass('disabled') && self.groupData.length > 1){
                                    $(this).removeClass('lightbox-prev-btn-show');
                                }
                            }).click(function(e){
                                if(!$(this).hasClass('disabled') && self.flag){
                                    self.flag = false;
                                    e.stopPropagation();
                                    self.goTo('prev');
                                }
                            });

        //判断是否是IE6
        this.isIE6 = /MSIE 6.0/gi.test(window.navigator.userAgent);

        //绑定窗口调整事件
        var timer = null;
        this.clear = false;     //当图片隐藏时不执行窗口
        $(window).resize(function(){
            if(self.clear){
                window.clearTimeout(timer);
                timer = window.setTimeout(function(){
                    self.loadPicSize(self.groupData[self.index].src);
                },500);
                if(self.isIE6){
                    self.popupMask.css({
                        width:$(window).width(),
                        height:$(window).height()
                    });
                }
            }
        }).keyup(function(e){       //键盘事件
            //console.log(e.which);
            var keyValue = e.which;
            if(self.clear){
                switch (keyValue){
                    case 37:
                        case 38:
                            self.prevBtn.click();
                            break;
                    case 39:
                        case 40:
                            self.nextBtn.click();
                            break;
                    case 27 :
                        self.popupMask.click();
                        self.closeBtn.click();
                        break;
                }
            }
        });

        //如果是IE6
        if(this.isIE6){
            $(window).scroll(function(){
                self.popupMask.css({
                   top:$(window).scrollTop()
                });
                self.popupWin.css({
                    top:$(window).scrollTop()
                });
            });
        }

    };
    LightBox.prototype = {
        initPopup:function(currentObj){      //初始化弹出
            var self = this,
                sourceSrc = currentObj.attr('data-source'),
                currentId = currentObj.attr('data-id');
            this.showMaskAndPopup(sourceSrc,currentId);
        },
        showMaskAndPopup:function(sourceSrc,currentId){     //显示遮罩层和弹出框
            var self = this;
            this.popupPic.hide();           //隐藏图片
            this.picCaptionArea.hide();     //隐藏图片描述信息

            var winWidth = $(window).width(),       //获取当前窗口的宽度
                winHeight = $(window).height();     //获取当前窗口的高度
            var maxW = this.settings.maxWidth < winWidth ? this.settings.maxWidth : winWidth,
                maxH = this.settings.maxHeight < winHeight ? this.settings.maxHeight : winHeight;

            var w = (maxW ||winWidth)/2,
                h = (maxH || winHeight)/2;
            this.picViewArea.css({
                                    width:w,
                                    height:h
                                    });
            if(this.isIE6){
                var scrollTop = $(window).scrollTop();
                this.popupMask.css({
                    width:winWidth,
                    height:winHeight,
                    top:scrollTop
                }).fadeIn();
            }else{
                this.popupMask.fadeIn();
            }

            this.popupWin.fadeIn();
            var viewHeight = h + 10;
            var topAnimate = (winHeight - viewHeight)/2;

            this.popupWin.css({
                                    width:w + 10,
                                    height:h + 10,
                                    marginLeft:-(w + 10)/2,
                                    top:(self.isIE6 ? (-viewHeight + scrollTop) : -viewHeight)
                                }).animate({
                                                top:self.isIE6 ? topAnimate + scrollTop : topAnimate
                                            },self.settings.speed,function(){
                                                //加载图片
                                                self.loadPicSize(sourceSrc);
                                            });

            //根据当前点击的元素ID获取在当前组别里面的索引
            this.index = this.getIndexOf(currentId);

            var groupDateLength = this.groupData.length;
            if(groupDateLength > 1){
                if(this.index === 0){
                    this.prevBtn.addClass('disabled');
                    this.nextBtn.removeClass('disabled');
                }else if(this.index ===groupDateLength - 1){
                    this.prevBtn.removeClass('disabled');
                    this.nextBtn.addClass('disabled');
                }else{
                    this.prevBtn.removeClass('disabled');
                    this.nextBtn.removeClass('disabled');
                }
            }else{
                this.prevBtn.addClass('disabled');
                this.nextBtn.addClass('disabled');
            }

        },
        loadPicSize:function(sourceSrc){
            var self = this;
            //每次加载图片时，初始化图片默认样式，并且隐藏
            this.popupPic.css({width:'auto',height:'auto'}).hide();
            this.picCaptionArea.hide();

            this.preLoadImg(sourceSrc,function(){
                self.popupPic.attr('src',sourceSrc);
                var picWidth = self.popupPic.width(),
                    picHeight = self.popupPic.height();
                self.changePic(picWidth,picHeight);
            });

        },
        changePic:function(w,h){
            var self = this,
                winWidth = $(window).width(),
                winHeight = $(window).height();

            var maxW = this.settings.maxWidth < winWidth ? this.settings.maxWidth : winWidth,
                maxH = this.settings.maxHeight < winHeight ? this.settings.maxHeight : winHeight;
            //如果图片的宽高大于浏览器市口的宽高比例，就看是否溢出
            var scale = Math.min( (maxW || winWidth)/(w+10),(maxH ||winHeight)/(h+10),1);
            w = w * scale;
            h = h * scale;

            this.picViewArea.animate({
                                        width:w - 10,
                                        height:h - 10
                                        },self.settings.speed);
            var top = (winHeight - h)/2;
            if(this.isIE6){
                top += $(window).scrollTop();
            }

            this.popupWin.animate({
                                width:w,
                                height:h,
                                marginLeft:-(w/2),
                                top:top
                            },self.settings.speed,function(){
                                self.popupPic.css({
                                                    width:w - 10,
                                                    height:h - 10
                                                    }).fadeIn();
                                self.picCaptionArea.fadeIn();
                                self.flag = true;
                                self.clear = true;
                            });

            //设置描述文字和当前索引
            this.captionText.text(this.groupData[this.index].caption);
            this.currentIndex.text('当前索引：' + (this.index + 1) + ' of ' + this.groupData.length);
        },
        preLoadImg:function(src,callback){
            var img = new Image();
            if(!!window.ActiveXObject){     //IE
                img.onreadystatechange = function(){
                    if(this.readyState == 'complete'){
                        callback();
                    }
                }
            }else{
                img.onload = function(){
                    callback();
                }
            }
            img.src = src;
        },
        getIndexOf:function(currentId){         //根据当前点击的元素ID获取在当前组别里面的索引
            var index = 0;
            $(this.groupData).each(function(i){
                index = i;
                if(this.id === currentId){
                    return false;
                }
            });
            return index;
        },
        getGroup:function(){            //根据当前组名获取同一组数据
            var self = this;
            //根据当前的组别名称获取页面中所有相同组别的对象
            var groupList = this.bodyNode.find('*[data-group='+ this.groupName +']');
            //清空数组数据
            self.groupData.length = 0;
            groupList.each(function(){
                self.groupData.push({
                    src:$(this).attr('data-source'),
                    id:$(this).attr('data-id'),
                    caption:$(this).attr('data-caption')
                });
            });
        },
        renderDOM:function(){           //渲染剩余的DOM 并且插入到 body
            var strDom =    '<div class="lightbox-pic-view">' +
                                '<span class="lightbox-btn lightbox-prev-btn"></span>' +
                                '<img class="lightbox-image" src="" alt=""/>' +
                                '<span class="lightbox-btn lightbox-next-btn"></span>'+
                            '</div>' +
                            '<div class="lightbox-pic-caption">' +
                                '<div class="lightbox-caption-area">' +
                                '<p class="lightbox-pic-desc"></p>' +
                                '<span class="lightbox-of-index"></span>' +
                                '</div>' +
                                '<span class="lightbox-close-btn"></span>' +
                            '</div>';
            //插入到 this.popupWin
            this.popupWin.html(strDom);
            //把遮罩和弹出框插入到body
            this.bodyNode.append(this.popupMask,this.popupWin);
        },
        goTo:function(dir){
            var src;
            if(dir === 'next'){
                this.index++;
                if(this.index >= this.groupData.length - 1){
                    this.nextBtn.addClass('disabled').removeClass('lightbox-next-btn-show');
                }
                if(this.index != 0){
                    this.prevBtn.removeClass('disabled');
                }
                src = this.groupData[this.index].src;
                this.loadPicSize(src);
            }else if(dir === 'prev'){
                this.index--;
                if(this.index <= 0){
                    this.prevBtn.addClass('disabled').removeClass('lightbox-prev-btn-show');
                }
                if(this.index != this.groupData.length - 1){
                    this.nextBtn.removeClass('disabled');
                }
                src = this.groupData[this.index].src;
                this.loadPicSize(src);
            }
        }
    };
    window['LightBox'] = LightBox;

})(jQuery);
















