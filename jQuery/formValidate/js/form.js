var MyValidator = function() {
    var handleSubmit = function() {
        $('.form-horizontal').validate({
            errorElement : 'span',
            errorClass : 'help-block',
            focusInvalid : false,
            rules : {
                name : {
                    required : true
                },
                password : {
                    required : true
                },
                intro : {
                    required : true
                }
            },
            messages : {
                name : {
                    required : "不能为空！"
                },
                password : {
                    required : "不能为空！"
                },
                intro : {
                    required : "不能为空！"
                }
            },

            highlight : function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },

            success : function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement : function(error, element) {
                element.parent('div').append(error);
            },

            submitHandler : function(form) {
                $(form).submit();
            }
        });

        $(document).on('keyup',function(e) {
            if (e.which === 13) {
                if ($('.form-horizontal').validate().form()) {
                    $('.form-horizontal').submit();
                }
                return false;
            }
        });
    };
    return {
        init : function() {
            handleSubmit();
        }
    };

}();
