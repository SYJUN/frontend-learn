;(function($){
    var Menubar = function(){
        this.el = document.querySelector('#sidebar ul');
        this.state = 'allClosed';   //hansOpened
        this.el.addEventListener('click',function(e){
            e.stopPropagation();
        });
        var self = this;
        self.currentOpendMenuContent = null;
        this.menuList = document.querySelectorAll('#sidebar ul > li');
        for(var i= 0;i < this.menuList.length;i++){
            this.menuList[i].addEventListener('click',function(e){
                var menuContentEl = document.getElementById(e.currentTarget.id + '-content');
                if(self.state === 'allClosed'){
                    console.log('打开' + menuContentEl.id);
                    self.state = 'hasOpened';
                    self.currentOpendMenuContent = menuContentEl;
                }else{
                    console.log('关闭' + self.currentOpendMenuContent.id);
                    self.state = 'hasOpened';
                    self.currentOpendMenuContent = menuContentEl;
                }
            });
        }
    };

    var Sidebar = function(eId,closeBarId){
        var self = this;
        this.state = 'opened';
        this.menubar = new Menubar();

        this.el = document.getElementById(eId || 'sidebar');
        this.closeBarEl = document.getElementById(closeBarId || 'closeBar');
        this.el.addEventListener('click',function(event){
            if(event.target !== self.el){
                self.triggerSwitch();
            }
        });
    };
    Sidebar.prototype = {
        close:function(){
            console.log('关闭sidebar');
            this.state = 'closed';
        },
        open:function(){
            this.state = 'opened';
            console.log('打开sidebar');
        },
        triggerSwitch:function(){
            if(this.state == 'opened'){
                this.close();
            }else{
                this.open();
            }
        }
    };
    var sidebar = new Sidebar();



})(jQuery);
