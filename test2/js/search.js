window.onload = InitData;
function InitData() {
    $('sim-select').onclick = function () {
        $('define-select').style.display = "";
    };
    var isOut = true;
    var isHiden = false;
    var dom = $("sim-select");
    document.onclick = function () {
        if (isOut || isHiden) {
            $('define-select').style.display = "none";
        }
        isOut = true;
        isHiden = false;
    }

    $('sim-select').onclick = function () {
        isOut = false;
        $('define-select').style.display = "";
    };
    for (var i in $('define-select').getElementsByTagName("li")) {

        $('define-select').getElementsByTagName("li")[i].onmouseover = function () {
            this.className += " over";
        };

        $('define-select').getElementsByTagName("li")[i].onmouseout = function () {

            this.className = this.className.replace(" over", "");
        };
        $('define-select').getElementsByTagName("li")[i].onclick = function () {

            var valName = this.innerHTML.replace("<a>", "").replace("</a>", "");
            valName = valName.replace("<A>", "").replace("</A>", "");
            valName = valName.replace(" ", "");
            $('h3Name').innerHTML = valName;
            PutValue(valName);
            isHiden = true;

            if (valName == "行情" || valName == "股吧") {
                checkStock(true);
            }
            else
                checkStock(false);
        }
    }
}
function PutValue(valName) {
    var val = "";

    switch (valName) {
        case '行情':
            val = "stock";
            break;
        case '股吧':
            val = "guba";
            break;
        case '博客':
            val = "blog";
            break;
        case '资讯':
            val = "news";
            break;
    }
    $('stypeId').value = val;
}