function setHome(url) {
    if (document.all) {
        document.body.style.behavior = 'url(#default#homepage)';
        document.body.setHomePage(url);
    } else if (window.sidebar) {
        if (window.netscape) {
            try {
                netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");}
            catch (e) {
                alert(" 该操作被浏览器拒绝，假如想启用该功能，请在地址栏内输入 about:config, 然后将项 signed.applets.codebase_principal_support 值该为 true");}
        }
        var prefs = Components.classes['@mozilla.org/preferences-service;1'].getService(Components.interfaces.nsIPrefBranch);
        prefs.setCharPref('browser.startup.homepage',url);
    } else if (navigator.userAgent.indexOf("Chrome") != -1) {
        alert("Google Chrome 浏览器暂不支持此设为首页功能!您可以直接Ctrl+D收藏!"); }
}
function addFav(url,disc) {
    if (document.all) {
        window.external.addFavorite(url,disc);
    }
    else if (window.sidebar) {
        window.sidebar.addPanel(disc,url,"");
    }
    else if (navigator.userAgent.indexOf("Chrome") != -1) {
        alert("Google Chrome 浏览器暂不支持此收藏功能!您可以直接Ctrl+D收藏!"); }
}
function showFocus() {
    var scrollFocus = new ScrollPic("listFocus","leftFocus","rightFocus",null,null);
    scrollFocus.dotListId      = "DotList";//点列表ID
    scrollFocus.dotClassName   = "dotOFF";//点className
    scrollFocus.dotOnClassName	= "dotON";//当前点className
    scrollFocus.listType		= "dot";//列表类型(number:数字，其它为空

    scrollFocus.arrVertical  = true;
    scrollFocus.frameSet     = 568;//显示框宽度
    scrollFocus.pageSet      = 568; //翻页宽度932 233
    scrollFocus.speed          = 10; //移动速度(单位毫秒，越小越快)
    scrollFocus.space          = 15; //每次移动像素(单位px，越大越快)
    scrollFocus.autoPlay       = true; //自动播放
    scrollFocus.autoPlayTime   = 6; //自动播放间隔时间(秒)
    scrollFocus.initialize(); //初始化*/
}
function slidingMore(g,c,e,j,r) {
    var d = {};
    var a = $(g).getElementsByTagName(c);
    var b = function(m, l) {
        var k = m;
        while (k.nodeName.toUpperCase() != c.toUpperCase()) {
            k = k.parentNode;
        }
        //alert(k);
        if (d.onObj != k) {
            d.onObj.className = "";
            $(j + d.onNum).style.display = "none";
            $(r + d.onNum).style.display = "none";
            k.className = e;
            $(j + l).style.display = "block";
            $(r + l).style.display = "block";
            d.onObj = k;
            d.onNum = l
            //alert(d.onNum);
            $(g).className="clear "+e+l;
        }
    };
    var h = function(l, k) {
        b(l, k + 1)
    };
    for (var f = 0; f < a.length; f++) {
        if (a[f].className == e) {
            d = {
                onObj: a[f],
                onNum: f + 1
            }
        }
        addEvent(a[f], "mouseover", h.bind(this, a[f], f), false);
    }
}
function slidingBg(g,c,e,j) {
    var d = {};
    var a = $(g).getElementsByTagName(c);
    var b = function(m, l) {
        var k = m;
        while (k.nodeName.toUpperCase() != c.toUpperCase()) {
            k = k.parentNode;
        }
        //alert(k);
        if (d.onObj != k) {
            d.onObj.className = "";
            $(j + d.onNum).style.display = "none";
            k.className = e;
            $(j + l).style.display = "block";
            d.onObj = k;
            d.onNum = l
            //alert(d.onNum);
            $(g).className="clear "+e+l;
        }
    };
    var h = function(l, k) {
        b(l, k + 1)
    };
    for (var f = 0; f < a.length; f++) {
        if (a[f].className == e) {
            d = {
                onObj: a[f],
                onNum: f + 1
            }
        }
        addEvent(a[f], "mouseover", h.bind(this, a[f], f), false);
    }
}
function showZt() {
    var scrollZt = new ScrollPic("listZt","leftZt","rightZt",null,null);
    scrollZt.arrVertical  = true;
    scrollZt.frameSet     = 480;//显示框宽度
    scrollZt.pageSet      = 160; //翻页宽度870 145
    scrollZt.speed          = 10; //移动速度(单位毫秒，越小越快)
    scrollZt.space          = 15; //每次移动像素(单位px，越大越快)
    scrollZt.autoPlay       = true; //自动播放
    scrollZt.autoPlayTime   = 3; //自动播放间隔时间(秒)
    scrollZt.initialize(); //初始化*/
}
function showZz() {
    var scrollZz = new ScrollPic("listZz","leftZz","rightZz",null,null);
    scrollZz.arrVertical  = true;
    scrollZz.frameSet     = 840;//显示框宽度
    scrollZz.pageSet      = 140; //翻页宽度870 145
    scrollZz.speed          = 10; //移动速度(单位毫秒，越小越快)
    scrollZz.space          = 15; //每次移动像素(单位px，越大越快)
    scrollZz.autoPlay       = true; //自动播放
    scrollZz.autoPlayTime   = 3; //自动播放间隔时间(秒)
    scrollZz.initialize(); //初始化*/
}

function videoPlay() {
    if ($(".roll").length > 0) {
        $(".rollimglist li").each(function () {
            $(this).addClass("rollimg");
            var thisindex = $(this).index();
            $(this).attr("nowpo", thisindex);
            if (thisindex > 2) {
                $(this).addClass("ldsn");
            }
            else {
                $(this).addClass("l" + (thisindex + 1));
            }
        });
        $(".rollimg:not(:eq(1))").css("opacity", "0.2");
        function rollimgto(i) {
            $(".rollimgtxt").remove();
            $(".rollimglist a .rollimg").unwrap();
            var rollimgpo = [{"left":10,"top":19,"zindex":9,"width":164,"height":92,"opacity": 0.2},{"left":45,"top":12,"zindex":10,"width":191,"height":107,"opacity":1},{"left":106,"top":19,"zindex":9,"width":164,"height":92,"opacity":0.2}];
            var rollimglenght = $(".rollimg").length;
            $(".rollimg").each(function () {
                var qindex = parseInt($(this).attr("nowpo")); //目前位置
                var toindex = (qindex + i + rollimglenght) % rollimglenght; //目标位置
                var thisimg = $(this).children("img"); //下面的图片
                $(this).attr("nowpo",toindex);
                if (toindex > 2) {
                    $(this).css("display","none");
                }
                else {
                    $(this).css("display","block");
                    $(this).animate({
                        left: rollimgpo[toindex].left,
                        top: rollimgpo[toindex].top,
                        opacity: rollimgpo[toindex].opacity
                    }, 300, function () {
                        $(this).css("z-index", rollimgpo[toindex].zindex);
                        if (toindex == 1) {
                            thisimg.after('<div class="rollimgtxt">' + thisimg.attr('alt') + '</div>');
                            $(this).wrap("<a href='" + thisimg.attr('ilink') + "' target='_blank'></a>");
                        }
                    });
                    thisimg.animate({
                        width: rollimgpo[toindex].width,
                        height: rollimgpo[toindex].height
                    }, 300);
                }
            });
        }
        $(".rollimg").eq(1).append('<div class="rollimgtxt">' + $(".rollimg").eq(1).children("img").attr('alt') + '</div>');
        $(".rollimg").eq(1).wrap('<a href="' + $(".rollimg").eq(1).children("img").attr('ilink') + '" target="_blank"></a>');
        $(".rollimg").click(function () {
            var qcindex = parseInt($(this).attr("nowpo"));
            if (qcindex == 0) {
                rollimgto(1);
            }
            else if (qcindex == 2) {
                rollimgto(-1);
            }
        });
        var timeroll;
        timeroll = setInterval(function () {
            rollimgto(-1);
        }, 5000);

        $(".roll").mouseenter(function () {
            clearInterval(timeroll);
        });
        $(".roll").mouseleave(function () {
            timeroll = setInterval(function () {
                rollimgto(-1);
            }, 5000);
        });
    }
}
