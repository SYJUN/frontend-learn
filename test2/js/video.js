jQuery.noConflict();
; (function ($) {
    var StockHK = {
        init: function () {
            StockHK.videoPlay();
        },
        videoPlay: function () {
            if ($(".roll").length > 0) {
                $(".rollimglist li").each(function () {
                    $(this).addClass("rollimg");
                    var thisindex = $(this).index();
                    $(this).attr("nowpo", thisindex);
                    if (thisindex > 2) {
                        $(this).addClass("ldsn");
                    }
                    else {
                        $(this).addClass("l" + (thisindex + 1));
                    }
                });
                $(".rollimg:not(:eq(1))").css("opacity", "0.2");
                function rollimgto(i) {
                    $(".rollimgtxt").remove();
                    $(".rollimglist a .rollimg").unwrap();
                    var rollimgpo = [{ "left": 10, "top": 19, "zindex": 9, "width": 164, "height": 92, "opacity": 0.2 }, { "left": 45, "top": 12, "zindex": 10, "width": 191, "height": 107, "opacity": 1 }, { "left": 106, "top": 19, "zindex": 9, "width": 164, "height": 92, "opacity": 0.2 }];
                    var rollimglenght = $(".rollimg").length;
                    $(".rollimg").each(function () {
                        var qindex = parseInt($(this).attr("nowpo")); //目前位置
                        var toindex = (qindex + i + rollimglenght) % rollimglenght; //目标位置
                        var thisimg = $(this).children("img"); //下面的图片
                        $(this).attr("nowpo", toindex);
                        if (toindex > 2) {
                            $(this).css("display", "none");
                        }
                        else {
                            $(this).css("display", "block");
                            $(this).animate({
                                left: rollimgpo[toindex].left,
                                top: rollimgpo[toindex].top,
                                opacity: rollimgpo[toindex].opacity
                            }, 300, function () {
                                $(this).css("z-index", rollimgpo[toindex].zindex);
                                if (toindex == 1) {
                                    thisimg.after('<div class="rollimgtxt">' + thisimg.attr('alt') + '</div>');
                                    $(this).wrap("<a href='" + thisimg.attr('ilink') + "' target='_blank'></a>");
                                }
                            });
                            thisimg.animate({
                                width: rollimgpo[toindex].width,
                                height: rollimgpo[toindex].height
                            }, 300);
                        }
                    });
                }
                $(".rollimg").eq(1).append('<div class="rollimgtxt">' + $(".rollimg").eq(1).children("img").attr('alt') + '</div>');
                $(".rollimg").eq(1).wrap('<a href="' + $(".rollimg").eq(1).children("img").attr('ilink') + '" target="_blank"></a>');
                $(".rollimg").click(function () {
                    var qcindex = parseInt($(this).attr("nowpo"));
                    if (qcindex == 0) {
                        rollimgto(1);
                    }
                    else if (qcindex == 2) {
                        rollimgto(-1);
                    }
                });
                var timeroll;
                timeroll = setInterval(function () {
                    rollimgto(-1);
                }, 5000);

                $(".roll").mouseenter(function () {
                    clearInterval(timeroll);
                });
                $(".roll").mouseleave(function () {
                    timeroll = setInterval(function () {
                        rollimgto(-1);
                    }, 5000);
                });
            }
        }
    }
    StockHK.init();
})(jQuery);

