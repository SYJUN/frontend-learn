/*全局数据缓存*/
var dataCache = {};

$ = function (s) {
    return (typeof (s) == "string") ? document.getElementById(s) : s;
}

$e = function (e) {
    return e.target ? e.target : event.srcElement;
}

$attr = function (elem, attr, value) {
    return (value) ? elem.setAttribute(attr, value) : elem.getAttribute(attr);

}

$addEvent = function (elem, eType, fn, useCapture) {
    if (elem.addEventListener) {
        elem.addEventListener(eType, fn, useCapture);
        return true;
    } else if (elem.attachEvent) {
        var r = elem.attachEvent('on' + eType, fn);
        return r;
    } else {
        elem['on' + eType] = fn;
    }
}

$delEvent = function (elem, eType, fn, useCapture) {
    if (elem.removeEventListener) {
        elem.removeEventListener(eType, fn, useCapture);
        return true;
    } else if (elem.detachEvent) {
        var r = elem.detachEvent('on' + eType, fn);
        return r;
    } else {
        elem['on' + eType] = null;
        return;
    }
}

$number = function (num, exponent) {
    if (exponent < 1) return num;
    var str = num.toString();
    if (str.indexOf(".") != -1) {
        if (str.split(".")[1].length >= exponent) {
            return str;
        } else {
            return $number(str + "0", exponent);
        }
    } else {
        return $number(str + ".0", exponent);
    }
}


$jsLoad = function (sUrl, sBianMa, fCallback) {
    var _js = document.createElement('script');
    _js.setAttribute('charset', sBianMa);
    _js.setAttribute('type', 'text/javascript');
    _js.setAttribute('src', sUrl);
    document.getElementsByTagName('head')[0].appendChild(_js);
    _js.onload = _js.onreadystatechange = function () {
        if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
            fCallback(_js);
        }
    }
}

$moveElement = function (elementID, final_x, final_y, interval, fn, step) {
    if (!$ || !$(elementID)) return false;
    var elem = $(elementID);
    if (elem.movement) clearTimeout(elem.movement);
    if (!elem.style.left) elem.style.left = "0px";
    if (!elem.style.top) elem.style.top = "0px";

    var xpos = parseInt(elem.style.left);
    var ypos = parseInt(elem.style.top);
    if (xpos == final_x && ypos == final_y) { setTimeout(fn, 0); return true; }

    if (step == null) {
        step = Math.abs(Math.ceil((final_x - xpos) / 10));
        if (step == 0) {
            step = Math.abs(Math.ceil((final_y - ypos) / 10));
        }
    }
    var xadd = (Math.abs(final_x - xpos) > step) ? step : Math.abs(final_x - xpos);
    var yadd = (Math.abs(final_y - ypos) > step) ? step : Math.abs(final_y - ypos);

    xpos = (xpos < final_x) ? xpos + xadd : xpos - xadd;
    ypos = (ypos < final_y) ? ypos + yadd : ypos - yadd;

    elem.style.left = xpos + "px";
    elem.style.top = ypos + "px";

    elem.movement = setTimeout(function () { $moveElement(elementID, final_x, final_y, interval, fn, step) }, interval);
}

Function.prototype.bind = function () {
    var __m = this, object = arguments[0], args = new Array();
    for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
    }
    return function () {
        return __m.apply(object, args);
    }
};

function getRadioValue(radio) {
    if (!radio.length && radio.type.toLowerCase() == 'radio') {
        return (radio.checked) ? radio.value : '';
    }
    if (radio[0].tagName.toLowerCase() != 'input' || radio[0].type.toLowerCase() != 'radio') return '';
    var len = radio.length;
    for (i = 0; i < len; i++) {
        if (radio[i].checked) {
            return radio[i].value;
        }
    }
    return '';
}

function StockImgChg(type, obj) {
    var ImgObj = null;
    var HrefNode = null;
    var stockCode = "";
    switch (type) {
        case "cn":
            ImgObj = $("cnStockImg");
            HrefNode = ImgObj.parentNode;
            ImgObj.src = "http://hqpicr.eastmoney.com/hp2/" + obj.value + ".png?Rt=" + Math.random();
            if (HrefNode.nodeName.toUpperCase() == "A") {
                switch (obj.value) {
                    case "0000011":
                        HrefNode.href = "http://quote.eastmoney.com/zs000001.html";
                        break;
                    case "3990012":
                        HrefNode.href = "http://quote.eastmoney.com/zs399001.html";
                        break;
                    case "0000031":
                        HrefNode.href = "http://quote.eastmoney.com/zs000003.html";
                        break;
                    case "3990032":
                        HrefNode.href = "http://quote.eastmoney.com/zs399003.html";
                        break;
                    case "0003001":
                        HrefNode.href = "http://quote.eastmoney.com/zs000300.html";
                        break;
                }
            }
            break;
        case "hk":
            ImgObj = $("hkStockImg");
            HrefNode = ImgObj.parentNode;
            ImgObj.src = "http://hkimg.eastmoney.com/HKImage2/" + obj.value + "_Small.png?Rt=" + Math.random();
            if (ImgObj.parentNode.nodeName.toUpperCase() == "A") {
                switch (obj.value) {
                    case "HSI":
                        HrefNode.href = "http://hk.eastmoney.com/quoteIndex_1.html";
                        break;
                    case "HSCCI":
                        HrefNode.href = "http://hk.eastmoney.com/quoteIndex_2.html";
                        break;
                    case "HSCEI":
                        HrefNode.href = "http://hk.eastmoney.com/quoteIndex_3.html";
                        break;
                    case "GEM":
                        HrefNode.href = "http://hk.eastmoney.com/quoteIndex_4.html";
                        break;
                }
            }
            break;
        case "ua":
            ImgObj = $("uaStockImg");
            HrefNode = ImgObj.parentNode;
            ImgObj.src = "http://img.eastmoney.com/AmericaImage2/" + obj.value + "_small.png?Rt=" + Math.random();
            if (ImgObj.parentNode.nodeName.toUpperCase() == "A") {
                switch (obj.value) {
                    case "DJI":
                        HrefNode.href = "http://stock.eastmoney.com/americastock/";
                        break;
                    case "IXIC":
                        HrefNode.href = "http://stock.eastmoney.com/americastock/";
                        break;
                    case "FTSE":
                        HrefNode.href = "http://stock.eastmoney.com/globalindex/FTSE.html";
                        break;
                }
            }
            break;
        case "qh":
            ImgObj = $("qhStockImg");
            HrefNode = ImgObj.parentNode;
            ImgObj.src = "http://hqpicqz.eastmoney.com/hp2/" + obj.value + "3.png?Rt=" + Math.random();
            if (ImgObj.parentNode.nodeName.toUpperCase() == "A") {
                HrefNode.href = "http://quote.eastmoney.com/gzqh/" + obj.value + ".html";
            }
            break;
        default:
            break;
    }
}

function reFreshImg() {
    var ParmObjs = $("stockSelectTab").getElementsByTagName("li");
    var atIndex = 0;
    for (var i = 0; i < ParmObjs.length; i++) {
        if (ParmObjs[i].className == "at" || ParmObjs[i].className == "h2") {
            atIndex = i;
            break;
        }
    }
    var obj = "cnStockImg";
    switch (atIndex) {
        case 0:
            obj = "cnStockImg";
            break;
        case 1:
            obj = "hkStockImg";
            break;
        case 2:
            obj = "uaStockImg";
            break;
        case 3:
            obj = "qhStockImg";
            break;
    }
    if (atIndex == 3 && !$(obj)) obj = "cybStockImg";
    var imgUrl = $(obj).src.split("Rt")[0];
    var lastStr = imgUrl.substring(imgUrl.length - 1, imgUrl.length);
    if (lastStr != "?" && lastStr != "&") {
        if (imgUrl.indexOf("?") >= 0) {
            imgUrl += "&";
        }
        else imgUrl += "?";
    }
    $(obj).src = imgUrl + "Rt=" + Math.random();
}

function slidingEvent(slidingObj, tagName, classname, showObj) {
    var sliderCache = {};
    var elements = $(slidingObj).getElementsByTagName(tagName);
    var slidingChg = function (obj, num) {
        var tempObj = obj;
        while (tempObj.nodeName.toUpperCase() != tagName.toUpperCase()) {
            tempObj = tempObj.parentNode;
        }

        if (sliderCache.onObj != tempObj) {
            sliderCache.onObj.className = "";
            $(showObj + sliderCache.onNum).style.display = "none";
            tempObj.className = classname;
            $(showObj + num).style.display = "block";

            sliderCache.onObj = tempObj;
            sliderCache.onNum = num;
        }
    };

    var fun = function (obj, num) {
        slidingChg(obj, num + 1);
    };
    for (var i = 0; i < elements.length; i++) {
        if (elements[i].className == classname) sliderCache = { onObj: elements[i], onNum: i + 1 };//当前所在焦点
        $addEvent(elements[i], "mouseover", fun.bind(this, elements[i], i), false);
    }
}

/* 刷新今日热点 */
function refreshHotToday() {
    var dataUrl = "http://cmsjs.eastmoney.com/data/hottoday.js";
    $jsLoad(dataUrl + '?rt=' + Math.random(),
        "utf-8",
        function (obj) {
            if ((typeof hotToday == "undefined") || (hotToday == null) || (hotToday == "")) {
                dataCache.hotTodayCache = dataCache.hotTodayCache || "";
            } else {
                dataCache.hotTodayCache = hotToday;
            }
            obj.parentNode.removeChild(obj);
            if (dataCache.hotTodayCache != "") {
                try {
                    var url = 'http://cmsjs.eastmoney.com/data/images/' + dataCache.hotTodayCache.trade[0] + '.png';
                    $("hotTodayImageUp").src = url + "?rt=" + Math.random();
                    $("hotTodayUpNews").href = "http://stock.eastmoney.com/hangye/" + dataCache.hotTodayCache.trade[0] + ".html";
                    $("hotTodayUpLink").href = "http://bk.eastmoney.com/BkStockList.aspx?bktype_id=" + dataCache.hotTodayCache.trade[0];

                    url = 'http://cmsjs.eastmoney.com/data/images/' + dataCache.hotTodayCache.trade[2] + '.png';
                    $("hotTodayImageDown").src = url + "?rt=" + Math.random();
                    $("hotTodayDownNews").href = "http://stock.eastmoney.com/hangye/" + dataCache.hotTodayCache.trade[2] + ".html";
                    $("hotTodayDownLink").href = "http://bk.eastmoney.com/BkStockList.aspx?bktype_id=" + dataCache.hotTodayCache.trade[2];

                    var datas_0 = dataCache.hotTodayCache.stock[0].split(',');
                    var datas_1 = dataCache.hotTodayCache.stock[1].split(',');
                    var c0 = (datas_0[3].indexOf('-') == -1) ? "r_" : "g_";
                    var c1 = (datas_1[3].indexOf('-') == -1) ? "r_" : "g_";
                    if (dataCache.ZC == true) {
                        $("hotTodayStockUp").innerHTML = '<li>'
                        + '<span class="sname">-</span> '
                        + '<span class="sprice ' + c0 + '">-</span> '
                        + '<span class="schange ' + c0 + '">-</span> '
                        + '</li><li>'
                        + '<span class="sname">-</span> '
                        + '<span class="sprice ' + c1 + '">-</span> '
                        + '<span class="schange ' + c1 + '">-</span> '
                        + '</li>';
                    } else {
                        $("hotTodayStockUp").innerHTML = '<li>'
                        + '<span class="sname"><a href="http://quote.eastmoney.com/' + datas_0[0] + '.html">' + datas_0[1].toString() + '</a></span> '
                        + '<span class="sprice ' + c0 + '">' + datas_0[2].toString() + '</span> '
                        + '<span class="schange ' + c0 + '">' + datas_0[3].toString() + '</span> '
                        + '</li><li>'
                        + '<span class="sname"><a href="http://quote.eastmoney.com/' + datas_1[0] + '.html">' + datas_1[1].toString() + '</a></span> '
                        + '<span class="sprice ' + c1 + '">' + datas_1[2].toString() + '</span> '
                        + '<span class="schange ' + c1 + '">' + datas_1[3].toString() + '</span> '
                        + '</li>';
                    }

                    datas_0 = dataCache.hotTodayCache.stock[2].split(',');
                    datas_1 = dataCache.hotTodayCache.stock[3].split(',');
                    c0 = (datas_0[3].indexOf('-') == -1) ? "r_" : "g_";
                    c1 = (datas_1[3].indexOf('-') == -1) ? "r_" : "g_";
                    if (dataCache.ZC == true) {
                        $("hotTodayStockDown").innerHTML = '<li>'
                        + '<span class="sname">-</span> '
                        + '<span class="sprice ' + c0 + '">-</span> '
                        + '<span class="schange ' + c0 + '">-</span> '
                        + '</li><li>'
                        + '<span class="sname">-</span> '
                        + '<span class="sprice ' + c1 + '">-</span> '
                        + '<span class="schange ' + c1 + '">-</span> '
                        + '</li>';
                    } else {
                        $("hotTodayStockDown").innerHTML = '<li>'
                        + '<span class="sname"><a href="http://quote.eastmoney.com/' + datas_0[0] + '.html">' + datas_0[1].toString() + '</a></span> '
                        + '<span class="sprice ' + c0 + '">' + datas_0[2].toString() + '</span> '
                        + '<span class="schange ' + c0 + '">' + datas_0[3].toString() + '</span> '
                        + '</li><li>'
                        + '<span class="sname"><a href="http://quote.eastmoney.com/' + datas_1[0] + '.html">' + datas_1[1].toString() + '</a></span> '
                        + '<span class="sprice ' + c1 + '">' + datas_1[2].toString() + '</span> '
                        + '<span class="schange ' + c1 + '">' + datas_1[3].toString() + '</span> '
                        + '</li>';
                    }
                } catch (e) { };
            }
        });
}

/* Tab切换函数 */
/* 20100402 新添 指定元素展开 - focusObj
 * 备注：对应元素序列从0开始计数
 */
function slidingEventPlus(slidingObj, tagName, classname, showObj, eventType, focusObj, callback) {
    var sliderCache = {};
    var elements = $(slidingObj).getElementsByTagName(tagName);
    eventType = (eventType == null || eventType == 0) ? "mouseover" : "click";

    var slidingChg = function (obj, num) {
        if ($(showObj + parseInt(num))) {
            var tempObj = obj;
            while (tempObj.nodeName != tagName.toUpperCase()) {
                tempObj = tempObj.parentNode;
            }
            if (sliderCache.obj != tempObj) {
                sliderCache.obj.className = "";
                $(showObj + parseInt(sliderCache.id)).style.display = "none";
                $(showObj + parseInt(num)).style.display = "block";

                tempObj.className = classname;

                sliderCache.obj = tempObj;
                sliderCache.id = num;

                if (typeof callback == "function") {
                    callback(obj, num);
                }
            }
        }
    };

    var runOnce = true;
    for (var i = 0; i < elements.length; i++) {
        var fn = function (obj, num) {
            //slidingChg(obj, parseInt(num) + 1);
            if (eventType == "mouseover") {
                sliderCache.fobj = obj;
                setTimeout(function () {
                    if (sliderCache.fobj != null && sliderCache.fobj == obj) {
                        slidingChg(obj, parseInt(num) + 1);
                    } else { return }
                }, 100);
            } else {
                slidingChg(obj, parseInt(num) + 1);
            }
        };
        var fn_out = function (obj, num) {
            sliderCache.fobj = null;
        };
        if (eventType == "mouseover") {
            $addEvent(elements[i], "mouseout", fn_out.bind(this, elements[i], i), false);
        }
        $addEvent(elements[i], eventType, fn.bind(this, elements[i], i), false);
        if (focusObj != null && !isNaN(focusObj)) {
            focusObj = (focusObj >= elements.length || focusObj < 0) ? 0 : focusObj;
            /* 指定某元素展开 */
            if (runOnce == true) {
                //初始化显示，仅一次
                sliderCache = { obj: elements[focusObj], id: parseInt(focusObj) + 1, fobj: null };
                runOnce = false;
            }
            if (i == focusObj) {
                elements[i].className = classname;
                $(showObj + (parseInt(i) + 1)).style.display = "block";
            } else {
                elements[i].className = "";
                $(showObj + (parseInt(i) + 1)).style.display = "none";
            }
        } else {
            if (elements[i].className == classname) sliderCache = { obj: elements[i], id: parseInt(i) + 1, fobj: null };
        }

    }
}

//国内外期货数据行情
function slidingHQData() {
    var show = "cn-bar"; /* 0,local;1,oversea */
    var proc = false;
    var displayHeight = $("hq-local").offsetHeight;
    var tween = function (obj0, obj1, h) {
        var step = 100;
        if ("\v" == "v") { step = 160; }
        if (h > 0) {
            var h0 = obj0.offsetHeight;
            var h1 = obj1.offsetHeight;
            if (h < step) {
                obj0.style.height = "0px";
                obj1.style.height = (h1 + h) + "px";
                proc = false;
            } else {
                h = h - step;
                obj0.style.height = (h0 - step) + "px";
                obj1.style.height = (h1 + step) + "px";
                setTimeout(function () { tween(obj0, obj1, h) }, 20);
            }
        } else {
            proc = false;
        }
    }
    $("cn-bar").onmouseover = $("gw-bar").onmouseover = function () {
        var _this = this.id;
        if (_this == show) {
            return false;
        }

        var _now = (show == "cn-bar") ? "hq-oversea" : "hq-local";
        var _tmp = (show == "cn-bar") ? "hq-local" : "hq-oversea";
        var _this = (show == "cn-bar") ? "gw-bar" : "cn-bar";



        if (proc == true) {
            return false;
        } else {
            proc = true;
            tween($(_tmp), $(_now), displayHeight);
            $(show).className = "areabar";
            $(_this).className = "areabar on";
            show = _this;
        }

    }
}
/* 添加option */
function addOption(obj, txt, val) {
    var objOption = document.createElement("OPTION");
    objOption.text = txt;
    objOption.value = val;
    obj.options.add(objOption);
}

//合约走势图
function initFutrueImage(val) {
    var obj_jys = $("fi-jys");
    var obj_pz = $("fi-pz");
    var obj_hy = $("fi-hy");
    var obj_img = $("fi-img");
    var obj_link = $("fi-link");
    var obj_btn = $("fi-sx");
    var index = [0, 0, 0];//map;
    var init = function () {
        obj_jys.options.length = 0;
        obj_pz.options.length = 0;
        obj_hy.options.length = 0;
        obj_jys.style.width = "80px";
        obj_pz.style.width = "80px";
        obj_hy.style.width = "80px";
        //添加jys
        for (var i = 0; i < _jys.length; i++) {
            if (i >= 4) { break; }
            addOption(obj_jys, _jys[i], i);
        }
        addOption(obj_pz, _pz[0][0], 0);
        addOption(obj_hy, _hy[0][0][0][0], 0);
        setit();
        refreshImg();
    }
    var setit = function () {
        //设置一个开始就选中的
        index[0] = 1;
        obj_jys.options[1].selected = true;
        loadpz();
        index[1] = 0;
        obj_pz.options[0].selected = true;
        loadhy();
        if (val == null || val == "") {
            index[2] = 5;
            obj_hy.options[5].selected = true;
        } else {
            setRB(val);
        }
    }
    var setRB = function (val) {
        //先设置默认，防止出错
        index[2] = 5;
        var totle = obj_hy.options.length;
        for (var i = 0; i < totle; i++) {
            //alert(obj_hy.options[i].value);
            if (obj_hy.options[i].value.toLowerCase() == val.toLowerCase()) {
                obj_hy.options[i].selected = true;
                index[2] = i;
            }
        }

    };
    var refreshImg = function () {
        var hyCode = _hy[index[0]][index[1]][index[2]][1];
        var markertCode = index[0];
        if (markertCode == 3) {
            markertCode = 4;
        } else if (markertCode == 2) {
            markertCode = 3;
        } else if (markertCode == 4) {
            markertCode = 0;
        } else if (markertCode == 0) {
            markertCode = 1;
        }
        if (hyCode.substr(0, 2) == "IF") {
            obj_link.href = "http://quote.eastmoney.com/gzqh/" + hyCode + ".html";
            obj_img.src = "http://hqpicqz.eastmoney.com/hp2/" + hyCode + "3.png?" + getRandom();
        }
        else {
            obj_link.href = "http://quote.eastmoney.com/qihuo/" + hyCode + ".html";
            obj_img.src = "http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?ImageType=RS&ID=" + hyCode + markertCode + "&refrence=flm&rt=" + getRandom();
        }
    }
    var demon = function () {
        refreshImg();
        setTimeout(function () { demon() }, 30000);
    }
    var loadpz = function () {
        var tmppz = _pz[index[0]];
        index[1] = 0;
        obj_pz.options.length = 0;
        for (var i = 0; i < tmppz.length; i++) {
            addOption(obj_pz, tmppz[i], i);
        }
        loadhy();
    }
    var loadhy = function () {
        var tmphy = _hy[index[0]][index[1]];
        index[2] = 0;
        obj_hy.options.length = 0;
        for (var i = 0; i < tmphy.length; i++) {
            addOption(obj_hy, tmphy[i][0], tmphy[i][1]);
        }
        refreshImg();
    }
    obj_jys.onchange = function () {
        index[0] = this.value;
        loadpz();
    }
    obj_pz.onchange = function () {
        index[1] = this.value;
        loadhy();
    }
    obj_hy.onchange = function () {
        index[2] = this.selectedIndex;
        refreshImg();
    }
    obj_btn.onclick = function () {
        refreshImg();
    }
    init();
    demon();
}

/* 分类，类似走势图
 备注:只联动分类,不做其他处理
 */
function initFutrueMenu(obj_jys, obj_pz, obj_hy, obj_member) {
    var index = [0, 0, 0]; //map;
    var memberData = []; //会员缓存
    var init = function () {
        obj_jys.options.length = 0;
        obj_pz.options.length = 0;
        obj_pz.style.width = "70px";

        //添加jys
        for (var i = 0; i < _jys.length; i++) {
            addOption(obj_jys, _jys[i], i);
        }
        addOption(obj_pz, _pz[0][0], 0);
        if (!(obj_hy == null || obj_hy == "")) {
            obj_hy.options.length = 0;
            obj_hy.style.width = "80px";
            addOption(obj_hy, _hy[0][0][0][0], 0);
        }
        if (obj_member != null) {
            obj_member.options.length = 0;
            addOption(obj_member, "会员", 0);
        }
    }
    var getMemeberData = function () {
        if (!(typeof cm == "undefined" || cm == null || cm == "")) {
            memberData = cm;
        }
    }
    var loadpz = function () {
        var tmppz = _pz[index[0]];
        index[1] = 0;
        obj_pz.options.length = 0;
        for (var i = 0; i < tmppz.length; i++) {
            addOption(obj_pz, tmppz[i], i);
        }
        if (!(obj_hy == null || obj_hy == "")) {
            loadhy();
        }
    }
    var loadhy = function () {
        var tmphy = _hy[index[0]][index[1]];
        index[2] = 0;
        obj_hy.options.length = 0;
        for (var i = 0; i < tmphy.length; i++) {
            addOption(obj_hy, tmphy[i][0], tmphy[i][1]);
        }
    }
    var loadMember = function () {
        getMemeberData();
        var tmpIndex = (index[0] - 1);
        obj_member.options.length = 0;
        if (tmpIndex < 0) {
            addOption(obj_member, "会员", 0);
        } else {
            var tmpMembers = memberData[tmpIndex].data;
            if (memberData[tmpIndex].value != index[0]) {
                //alert("Error Members!");
            }
            for (var i = 0; i < tmpMembers.length; i++) {
                addOption(obj_member, tmpMembers[i][1], tmpMembers[i][0]);
            }
        }
    }
    obj_jys.onchange = function () {
        index[0] = this.value;
        loadpz();
        if (obj_member != null) {
            loadMember();
        }
    }
    obj_pz.onchange = function () {
        index[1] = this.value;
        if (!(obj_hy == null || obj_hy == "")) {
            loadhy();
        }

    }
    if (!(obj_hy == null || obj_hy == "")) {
        obj_hy.onchange = function () {
            index[2] = this.selectedIndex;
        }
    }

    init();
}

function getRandom() {
    return (new Date()).getTime() + Math.random().toString().replace("0.", "");
}

function getQueryString(paras) {
    var url = location.href;
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {};
    for (i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[paras.toLowerCase()];
    if (typeof (returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
}

/* 全球股市行情 */
function loadGlobalQuote() {
    var initScroll = function () {
        var scrolltxt = new ScrollPic();
        scrolltxt.scrollContId = "worldData";
        scrolltxt.arrVertical = true;
        scrolltxt.frameSet = 840;
        scrolltxt.pageSet = 1;
        scrolltxt.speed = 0.02;
        scrolltxt.space = 1;
        scrolltxt.autoPlay = true;
        scrolltxt.autoPlayTime = 0.02;
        scrolltxt.initialize();
    }
    $jsLoad("http://cmsjs.eastmoney.com/data/worlddata.js?rt=" + Math.random(),
        "utf-8",
        function (obj) {
            try {
                if (!(typeof (worldData) == "undefined" || worldData == null || worldData == "")) {
                    document.getElementById("worldData").innerHTML = worldData;
                    initScroll();
                }
            } catch (e) { }
        });
}

/*持仓数据查询*/
function dataSearch(type) {
    var url = "http://data.eastmoney.com/futures/{jys}/{type}/{code},{date}.html";
    var typeArr = ["", "data", "position", "timeline", "price", "profit"];
    var jysArr = ["", "sh", "dl", "zz", "zj"];

    type = (isNaN(type) || type > 5 || type < 1) ? 1 : type;

    var jys = $("jys_" + type).value;
    var pzname = $("pz_" + type).options[$("pz_" + type).selectedIndex].text;
    var date, member, code;
    var pz_name = null;
    jys = (isNaN(jys) || jys > 4 || jys < 0) ? 0 : jys;

    if ($("date_" + type)) {
        date = $("date_" + type).value;
        if (date == null || date.replace(/^\s+/ig, "") == "") {
            alert("请输入您要查询的交易日期!");
            return false;
        }
        if (!(/^\d{4}-\d{1,2}-\d{1,2}$/).test(date)) {
            alert("请输入正确的交易日期！(正确的格式:2010-01-01)");
            return false;
        } else {
            var full_d = new Date(Date.parse(date.replace(/-/ig, "/")));
            var y = full_d.getFullYear();
            var m = full_d.getMonth() + 1;
            var d = full_d.getDate();
            m = (m < 10) ? "0" + m : m;
            d = (d < 10) ? "0" + d : d;
            date = y + "-" + m + "-" + d;
        }
        if (jys == 0) {
            alert("请先选择交易所！");
            return false;
        }
        if ($("hy_" + type)) {
            code = $("hy_" + type).value;
            if (code == 0) {
                alert("请选择合约！");
                return false;
            }
        }
        if ($("member_" + type)) {
            member = $("member_" + type).value;
            if (member == 0) {
                alert("请选择会员！");
                return false;
            }
            pz_name = pzname;
            code = member;
        }
    } else if ($("member_" + type)) {
        if (jys == 0) {
            alert("请先选择交易所！");
            return false;
        }
        if ($("hy_" + type)) {
            code = $("hy_" + type).value;
            if (code == 0) {
                alert("请选择合约！");
                return false;
            }
        }
        member = $("member_" + type).value;
        if (member == 0) {
            alert("请选择会员！");
            return false;
        }
        date = member;
    }


    jys = jysArr[jys];
    type = typeArr[type];

    url = url.replace("{jys}", jys);
    url = url.replace("{type}", type);
    url = url.replace("{code}", code);
    url = url.replace("{date}", date);
    if (pz_name != null) {
        window.open(url + "?type=" + pz_name, "");
    } else {
        window.open(url);
    }
}


// 股票搜索框 通用
var flagStock = false;
var soType = "news";
var arg = {
    text: "输代码、名称或拼音",
    autoSubmit: true,
    width: 202,
    header: ["选项", "代码", "名称", "类型"],
    body: [-1, 1, 4, -2],
    callback: function () { }
};

function checkStock(m_flagStock) {
    flagStock = m_flagStock;
    soType = getRadioValue($("sofrm").stype);
    ss.dispose();
    if (flagStock) {
        switch (soType) {
            case "stock":
                ss.text = "输代码、名称或拼音";
                ss.type = "";
                ss.autoSubmit = true;
                break;
            case "guba":
                ss.text = "输代码、股吧名";
                ss.type = "CNSTOCK";
                ss.autoSubmit = false;
                break;
        }
        ss.init();
    } else {
        $("sofrm").action = "";
        $("sofrm").onsubmit = function () { return checkso($("sofrm")); };
        $("sofrm").StockCode_bar.value = "请输入查询内容";
        $("sofrm").StockCode_bar.onclick = function () {
            if (this.value == '输代码、名称或拼音' || this.value == '输代码、股吧名' || this.value == '请输入查询内容') this.value = '';
        }
        $("sofrm").StockCode_bar.onblur = function () {
            if (this.value == '') {
                switch (soType) {
                    case "stock":
                        this.value = "输代码、名称或拼音";
                        break;
                    case "guba":
                        this.value = "输代码、股吧名";
                        break;
                    default:
                        this.value = "请输入查询内容";
                        break;
                }
            }
        }
    }
}
function checkso(obj) {
    var keywords = $("StockCode_bar").value;
    keywords = (keywords == '输代码、名称或拼音' || keywords == '输代码、股吧名' || keywords == '请输入查询内容')
        ? ""
        : keywords;

    if ((keywords.length < 2 && (soType == "news" || soType == "blog"))) {
        alert("关键字不能少于两位字符");
        return false;
    }
    if (soType == "news" || soType == "blog") {
        window.open("http://so.eastmoney.com/Search.aspx?q=" + escape(keywords) + "&searchclass=" + soType);
    } else if (soType == "guba") {
        window.open('http://quote.eastmoney.com/search.aspx?stockcode=' + escape(keywords) + '&toba=1');
    } else {
        window.open('http://quote.eastmoney.com/search.aspx?stockcode=' + escape(keywords) + '');
    }

    return false;
}

function gb_search(code) {
    var keywords = (code == "输代码、名称或拼音缩写" || code == "输代码、名称或拼音" || code == "输入代码或简称") ? "" : code;
    window.open('http://quote.eastmoney.com/search.aspx?stockcode=' + escape(keywords) + '&toba=1');
}

//
var UpdateCache = { Jys: 1, "sqs": 1, "dss": 1, "zss": 1 }
var JysArr = ["", "sqs", "dss", "zss"]
var setVal = function (obj, num) {
    eval(JysArr[UpdateCache.Jys] + "update" + UpdateCache[JysArr[UpdateCache.Jys]] + ".Sleep()");
    UpdateCache[JysArr[UpdateCache.Jys]] = num;
    eval(JysArr[UpdateCache.Jys] + "update" + UpdateCache[JysArr[UpdateCache.Jys]] + ".Awake()");
}
var setJys = function (obj, num) {
    eval(JysArr[UpdateCache.Jys] + "update" + UpdateCache[JysArr[UpdateCache.Jys]] + ".Sleep()");
    UpdateCache.Jys = num;
    eval(JysArr[UpdateCache.Jys] + "update" + UpdateCache[JysArr[UpdateCache.Jys]] + ".Awake()");

}
