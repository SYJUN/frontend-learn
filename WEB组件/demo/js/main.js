require.config({
    paths: {
        jquery: 'jquery-1.11.3',
        //jqueryUI: 'http://code.jquery.com/ui/1.10.4/jquery-ui',
        jqueryUI: 'jquery-ui'
    }
});
require(['jquery', 'window'], function ($, w) {
    $('#a').click(function () {
        var win = new w.Window();
        win.alert({
            title: '提示',
            content: 'welcome!',
            width: 300,
            height: 150,
            y: 50,
            hasCloseBtn: true,
            textForAlertBtn: 'ok',
            dragHandle: '.window_header',
            //skinClassName:'window_skin_a',
            handlerForAlertBtn: function () {
                alert("you click the alert bottom");
            },
            handlerForCloseBtn: function () {
                alert("you click the close bottom");
            }
        })
            .on('alert',function(){alert('the second alert handler');})
            .on('alert',function(){alert('the third alert handler');})
            .on('close',function(){alert('the second close handler');});
    });

    $('#b').click(function(){
        new w.Window().confirm({
            title:'系统消息',
            content:'您确定要删除这个文件吗？',
            width:300,
            height:150,
            y:50,
            textForConfirmBtn:'是',
            textForCancelBtn:'否',
            dragHandle:'.window_header'
        })
            .on('confirm',function(){
                alert('确定');
            })
            .on('cancel',function(){
                alert('取消');
            });
    });

    $('#c').click(function(){
        new w.Window().prompt({
            title:'请输入您的名字',
            content:'我们将会为您保密您输入的信息。',
            width:300,
            height:150,
            y:50,
            textForPromptBtn:'输入',
            textForCancelBtn:'取消',
            defaultValueForPromptInput:'张三',
            dragHandle: '.window_header',
            handlerForPromptBtn:function(inputValue){
                alert('您输入的内容是：' + inputValue);
            },
            handlerForCancelBtn:function(){
                alert('取消');
            }
        });
    });

    $('#d').click(function(){
        new w.Window().common({
            content:'我是一个通用弹窗',
            width:300,
            height:150,
            y:50,
            hasCloseBtn: true
        });
    });
});

