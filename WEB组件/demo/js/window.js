define([ 'widget','jquery', 'jqueryUI'], function (widget,$, $UI) {
    function Window() {
        this.cfg = {
            width: 500,
            height: 300,
            title: '系统消息',
            content: '',
            hasCloseBtn: false,
            hasMask: true,
            isDraggable: true,
            dragHandle: null,
            skinClassName: null,

            textForAlertBtn: '确定',
            handlerForAlertBtn: null,
            handlerForCloseBtn: null,

            textForConfirmBtn:'确定',
            textForCancelBtn:'取消',
            handlerForConfirmBtn:null,
            handlerForCancelBtn:null,

            textForPromptBtn:'确定',
            isPromptInputPassword:false,    //false - 普通输入框, true - 密码框
            defaultValueForPromptInput:'',
            maxlengthForPromptInput:10,
            handlerForPromptBtn:null

        };
        this.handlers = {};
    }

    Window.prototype = $.extend({}, new widget.Widget(), {
        renderUI:function(){
            var $body = $('body'),
                footerContent = '';
            switch(this.cfg.winType){
                case 'alert':
                    footerContent = '<input class="window_alertBtn" type="button" value="' + this.cfg.textForAlertBtn + '" />';
                    break;
                case 'confirm':
                    footerContent = '<input type="button" value="'+ this.cfg.textForConfirmBtn +'" class="window_confirmBtn" />' +
                        '<input type="button" value="'+ this.cfg.textForCancelBtn +'" class="window_cancelBtn" />';
                    break;
                case 'prompt':
                    this.cfg.content += '<p class="window_promptInputWrapper">' +
                                                '<input type="'+ (this.cfg.isPromptInputPassword ? "password" : "text") +'" ' +
                                                        'value="'+ this.cfg.defaultValueForPromptInput +'" ' +
                                                        'maxlength="'+ this.cfg.maxlengthForPromptInput +'" ' +
                                                        'class="window_promptInput" />' +
                                            '</p>';
                    footerContent = '<input type="button" value="'+ this.cfg.textForPromptBtn +'" class="window_promptBtn" />'+
                                    '<input type="button" value="'+ this.cfg.textForCancelBtn +'" class="window_cancelBtn" />';
                    break;
            }

            this.boundingBox = $(
                '<div class="window_boundingBox">' +
                    '<div class="window_body">' + this.cfg.content + '</div>' +
                '</div>'
            );
            if(this.cfg.winType != 'common'){
                this.boundingBox.prepend('<div class="window_header">' + this.cfg.title + '</div>');
                this.boundingBox.append('<div class="window_footer">'+ footerContent +'</div>');
            }

            if(this.cfg.hasMask){
                this._mask = $('<div class="window_mask"></div>');
                this._mask.appendTo($body);
            }
            if(this.cfg.hasCloseBtn){
                this.boundingBox.append('<span class="window_closeBtn">X</span>');
            }
            this.boundingBox.appendTo(document.body);

            this._promptInput = this.boundingBox.find('.window_promptInput');
        },
        bindUI:function(){
            var self = this;
            this.boundingBox
                .delegate('.window_alertBtn','click',function(){
                    self.fire('alert');
                    self.destroy();
                })
                .delegate('.window_closeBtn','click',function(){
                    self.fire('close');
                    self.destroy();
                })
                .delegate('.window_confirmBtn','click',function(){
                    self.fire('confirm');
                    self.destroy();
                })
                .delegate('.window_cancelBtn','click',function(){
                    self.fire('cancel');
                    self.destroy();
                })
                .delegate('.window_promptBtn','click',function(){
                    self.fire('prompt',self._promptInput.val());
                    self.destroy();
                });
            if(this.cfg.handlerForAlertBtn) this.on('alert',this.cfg.handlerForAlertBtn);
            if(this.cfg.handlerForCloseBtn) this.on('close',this.cfg.handlerForCloseBtn);

            if(this.cfg.handlerForConfirmBtn) this.on('confirm',this.cfg.handlerForConfirmBtn);
            if(this.cfg.handlerForCancelBtn) this.on('cancel',this.cfg.handlerForCancelBtn);

            if(this.cfg.handlerForPromptBtn) this.on('prompt',this.cfg.handlerForPromptBtn);
        },
        syncUI:function(){
            this.boundingBox.css({
                width: this.cfg.width + 'px',
                height: this.cfg.height + 'px',
                left: (this.cfg.x || (window.innerWidth - this.cfg.width) / 2) + 'px',
                top: (this.cfg.y || (window.innerHeight - this.cfg.height) / 2 ) + 'px'
            });
            if(this.cfg.skinClassName) this.boundingBox.addClass(this.cfg.skinClassName);

            if(this.cfg.isDraggable){
                if(this.cfg.dragHandle){
                    this.boundingBox.draggable({handle:this.cfg.dragHandle});
                }else{
                    this.boundingBox.draggable();
                }
            }
        },
        destructor:function(){
            this._mask && this._mask.remove();
        },
        alert: function (cfg) {
            $.extend(this.cfg,cfg,{winType:'alert'});
            this.render();
            return this;
        },
        confirm: function (cfg) {
            $.extend(this.cfg,cfg,{winType:'confirm'});
            this.render();
            return this;
        },
        prompt: function (cfg) {
            $.extend(this.cfg,cfg,{winType:'prompt'});
            this.render();
            this._promptInput.focus();//输入框自动获取焦点
            return this;
        },
        common:function(cfg){
            $.extend(this.cfg,cfg,{winType:'common'});
            this.render();
            return this;
        }
    });
    return {Window: Window};
});
