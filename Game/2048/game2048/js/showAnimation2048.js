function showNumberWithAnimation(i, j, randNumber) {
    var numberCell = $('#number_cell_' + i + "_" + j);

    numberCell.css('background-color', getNumberBackgroundColor(randNumber));
    numberCell.css('color', getNumberColor(randNumber));
    numberCell.text(randNumber);

    numberCell
        .animate({
            width: cellSideLength * 1.2,
            height: cellSideLength * 1.2,
            top: getPosTop(i, j) - cellSideLength * 0.1,
            left: getPosLeft(i, j) - cellSideLength * 0.1
        }, 100)
        .animate({
            width: cellSideLength,
            height: cellSideLength,
            top: getPosTop(i, j),
            left: getPosLeft(i, j)
        }, 100);
}


function showMoveAnimation(fromx, fromy, tox, toy) {

    var numberCell = $('#number_cell_' + fromx + '_' + fromy);
    numberCell.animate({
        top: getPosTop(tox, toy),
        left: getPosLeft(tox, toy)
    }, 200);
}

function updateScore(score) {
    $('#score').text(score);
}



















