/*
function route(handler,pathname,response,request){
    console.log("About to route a request for "+ pathname);
    if(typeof handler[pathname] === 'function'){
        return handler[pathname](response,request);
    }else{
        console.log("No request handler found for "+ pathname);
        response.writeHead(404,{"Content-Type":"text/plain"});
        response.write("404 Not found");
        response.end();
    }
}
exports.routeUrl = route;
*/

function route(handle,pathname){
    console.log("About to route a request for " + pathname);
    if(typeof handle[pathname] === 'function'){
        handle[pathname]();
    }else{
        console.log("No request handler found for " + pathname);
        return "404 Not found";
    }
}
exports.route = route;






