1.通过命令行访问js文件：
	进入cmd模式下，输入js文件所在盘符（例如： d: ）--> cd + js所在文件夹（防止中文出现） --> node + js文件名（例如：test.js）
		PS:
		js文件内容：  test.js
		var http = require('http');
    http.createServer(function (request, response) {
        // 发送 HTTP 头部
        // HTTP 状态值: 200 : OK
        // 内容类型: text/plain
        response.writeHead(200, {'Content-Type': 'text/plain'});

        // 发送响应数据 "Hello World"
        response.end('Hello World\n');
    }).listen(8888);
    // 终端打印如下信息
    console.log('Server running at http://127.0.0.1:8888/');


		 命令行启动： d: --> cd nodejs_study --> node test.js
				(此时已经启动该服务器)

2.安装supervisor
	安装在全局模式：npm install supervisor -g