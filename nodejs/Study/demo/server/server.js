var http = require('http'),
    fs = require('fs'),
    url = require('url'),
    path = require('path');

//创建服务器
http.createServer(function (request, response) {
    //解析请求，包括文件名
    var pathname = url.parse(request.url).pathname;
    pathname = (pathname === '/') ? '/index.html' : pathname;
    //输出请求的文件名
    console.log('Request for ' + pathname + ' received.');

    var filePath = path.join(__dirname,pathname);
    //从文件系统中读取请求的文件内容    pathname.substr(1)
    fs.readFile(filePath, function (err, data) {
        console.log(filePath);
        if (err) {
            console.log(err);
            //HTTP 状态码：404 ： NOT FOUND
            //Content Type : text/plain
            response.writeHead(404, {'Content-Type': 'text/html'});
            response.write('404：找不到页面!')
        } else {
            //HTTP 状态码：200 ： OK
            //Content Type : text/plain
            response.writeHead(200, {'Content-Type': 'text/html'});

            //响应文件内容
            response.write(data.toString());
        }
        //发送响应数据
        response.end();
    });
}).listen(8888);

//控制台会输出以下信息
console.log('Server running at http://127.0.0.1:8888');


