exports.printFoo = function(){return "foo"};
var user = new Object();
user.name = "Lee";
user.getName = function(){return this.name;};
user.setName = function(name){this.name = name;};

//用于查看一个对象中的内容并且将该对象的信息输出到控制台中
//console.dir(user);

exports.user = user;