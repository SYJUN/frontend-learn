/*
 * 新闻列表
 * time:2016/03/23
 * */
var newsListContent = {
    data: newsListData,     //读取的数据
    page: 1,                 //分页的页数初始数
    pagelistcount: 5,     //每页显示的列表数量
    pageLen:0,              //显示总的分页数量
    temvar:null,            //临时存储变量

    parentElement: document.getElementById("newsListContent"),
    pagerNoDiv:document.getElementById("pagerNoDiv"),
    init: function () { //初始化
        var self = this;
        self.temvar = self.pagelistcount;

        self.pageLen = Math.ceil(this.data.length / this.pagelistcount);
        self.newsUpdate(self.page);
        self.pagerReset();
    },
    newsUpdate:function(n){     //刷新页面数据
        var self = this;
        /*
         * 0 - 跳转链接地址  aLink
         * 1 - 图片地址 imgUrl
         * 2 - 文章标题 Title
         * 3 - 详情信息 Info
         * 4 - 发表时间  time
         * 5 - 分享者链接 shareUrl
         * */
        var dataLi = '<li>' +
            '<div class="image">' +
            '<a href="{0}" target="_blank">' +
            '<img src="{1}" alt=""/>' +
            '</a>' +
            '</div>' +
            '<div class="text">' +
            '<p class="title">' +
            '<a href="{0}" target="_blank">{2}</a>' +
            '</p>' +
            '<p class="info" title="{2}">{3}</p>' +
            '<p class="time">{4}</p>' +
            '</div>' +
            '<div class="share">' +
            '<span class="shareIco"></span>' +
            '<div class="shareContent" data="{\'url\':\'\{5\}\',text:\'\{2\}\'}">' +
            '<a href="#" class="bds_iguba" title="分享到股吧"></a>' +
            '<a href="#" class="bds_tsina" title="分享到新浪微博"></a>' +
            '<a href="#" class="bds_qzone" title="分享到QQ空间"></a>' +
            '<a href="#" class="bds_tqq" title="分享到腾讯微博"></a>' +
            '</div>' +
            '</div>' +
            '</li>';

        var lihtml = "";

 /*测试：
        console.log("第"+ n +"页");
        console.log("数据的长度:"+ self.data.length );
        console.log("总的容纳长度："+ self.pagelistcount * n );
        */

        //如果   数据总长度 - 上一页显示完的总数量 = 剩下未显示的数据 < 每页规定显示的数量
        if( self.data.length - self.pagelistcount * (n-1)  < self.pagelistcount ){
            self.pagelistcount = self.pagelistcount - self.pagelistcount * self.pageLen % self.data.length;
        }else{
            self.pagelistcount = self.temvar;
        }

        for (var i = 0; i < self.pagelistcount; i++) {
            var current = self.data[i + self.temvar * (n-1)];
            var info = current.Info.length > 70 ? current.Info.slice(0, 70) + "..." : current.Info;
            var str = dataLi.format(
                current.aLink,
                current.imgUrl,
                current.Title,
                info,
                current.time,
                current.shareUrl
            );
            lihtml += str;
        }
        self.parentElement.innerHTML = lihtml;

        self.newsListMouseover();
    },
    newsListMouseover: function () {       //新闻列表的鼠标移入事件
        var self = this;
        var lis = self.parentElement.getElementsByTagName("li");
        var len = lis.length;
        var i;
        len = lis.length;
        //每个li 的鼠标移入事件
        for (i = 0; i < len; i++) {
            lis[i].onmouseover = function () {
                this.style.backgroundColor = "#f7f7f7";
            };
            lis[i].onmouseout = function () {
                this.style.backgroundColor = "";
            };
        }
        //每个分享的鼠标移入事件
        var share = getElementsByClassName(self.parentElement, "div", "share");
        for (i = 0; i < len; i++) {
            share[i].onmouseover = function () {
                this.getElementsByTagName("div")[0].style.display = "block";
            };
            share[i].onmouseout = function () {
                this.getElementsByTagName("div")[0].style.display = "";
            };
        }
    },
    createPagerBtn:function(){      //动态生成分页按钮
        //分页按钮
        var self = this;
        //var pageLen = Math.ceil(self.data.length / self.pagelistcount);
        var pagerNoDiv = document.getElementById("pagerNoDiv");
        var j, pagerhtml = "";
        if (self.pageLen > 0 && self.pageLen <= 3) {
            for (j = 0; j < self.pageLen; j++) {
                pagerhtml += '<li>' + (j + 1) + '</li>';
            }
            pagerNoDiv.innerHTML = pagerhtml;
        } else if (self.pageLen > 3) {
            for (j = 0; j < self.pageLen; j++) {
                pagerhtml += '<li>' + (j + 1) + '</li>';
            }

            pagerNoDiv.innerHTML = '<li class="first">上一页</li>' + pagerhtml + '<li class="last">下一页</li>';
        }

    },
    pagerReset: function () {       //分页按钮初始化
        var self = this;
        self.createPagerBtn();
        var pagebtns = self.pagerNoDiv.getElementsByTagName("li");
        var i, len = pagebtns.length;
        for (i = 0; i < len; i++) {
            if (len > 3 && i > 0 && i < len - 1) {
                if (i == 1) {
                    pagebtns[i].className = "at";
                    getElementsByClassName(self.pagerNoDiv, "li", "first")[0].style.visibility = "hidden";
                }
                self.pagerClick();
            }else if(len<=3){
                if (i == 0) pagebtns[i].className = "at";
                pagebtns[i].onclick = function(){
                    self.pagerBtnMove(this.innerHTML-1);
                };
            }

            pagebtns[i].onmouseover = function () {
                this.style.borderColor = "#286eb4";
            };
            pagebtns[i].onmouseout = function () {
                this.style.borderColor = "";
            };
        }

    },
    pagerClick: function () {       //分页按钮的点击事件
        var self = this;
        var pagebtns = self.pagerNoDiv.getElementsByTagName("li");
        var i, len = pagebtns.length;
        for (i = 0; i < len; i++) {
            pagebtns[i].onclick = function(){
                self.pagerBtnMove(this.innerHTML);
            };
        }
        var prevbtn = getElementsByClassName(self.pagerNoDiv,"li","first")[0];
        prevbtn.onclick = function(){
            var atBtn = getElementsByClassName(self.pagerNoDiv,"li","at")[0];
            self.pagerBtnMove( atBtn.innerHTML - 1 );
        };

        var nextbtn = getElementsByClassName(self.pagerNoDiv,"li","last")[0];
        nextbtn.onclick = function(){
            var atBtn = getElementsByClassName(self.pagerNoDiv,"li","at")[0];
            self.pagerBtnMove( atBtn.innerHTML - 0 + 1 );
        };
    },
    pagerBtnMove:function(n){       //分页按钮改变状态的方法
        var self = this;
        var pagebtns = self.pagerNoDiv.getElementsByTagName("li");

        var f_btn = getElementsByClassName(self.pagerNoDiv, "li", "first")[0];
        var l_btn = getElementsByClassName(self.pagerNoDiv, "li", "last")[0];
        var len = pagebtns.length;

        getElementsByClassName(self.pagerNoDiv, "li", "at")[0].className = "";
        pagebtns[n].className = "at";
        if(f_btn){
            if( pagebtns[n].innerHTML != 1 ){
                f_btn.style.visibility = "visible";
            }else{
                f_btn.style.visibility = "hidden";
            }
        }
        if(l_btn){
            if( pagebtns[n].innerHTML != len - 2 ){
                l_btn.style.visibility = "visible";
            }else{
                l_btn.style.visibility  = "hidden";
            }
        }
        self.page = Number(getElementsByClassName(self.pagerNoDiv, "li", "at")[0].innerHTML);
        self.newsUpdate(self.page);
    }

};

newsListContent.init();

















