/*
 * 功能：格式化本地时间
 * 格式：2016年03月22日 11：13
 *
 * */
function formatDate(){
    var date = new Date();
    var month = (date.getMonth()+1) < 10 ? "0"+ (date.getMonth()+1) : date.getMonth()+1;
    var day = date.getDate() <10 ? "0"+date.getDate():date.getDate();
    var h = date.getHours() < 10 ? "0" +date.getHours() : date.getHours();
    var m = date.getMinutes() < 10 ? "0"+date.getMinutes() : date.getMinutes();

    return date.getFullYear() + "年" + month + "月"+ day + "日 " + h+"："+ m;
}

/*
 * 功能：字符串格式化
 * 动态替换
 * arguments[i]表示当前位置的参数
 * */

String.prototype.format = function(){
    var str = this;
    for(var i=0;i<arguments.length;i++){
        var reg = new RegExp("\\{"+ i +"\\}","gm");
        str = str.replace(reg,arguments[i]);
    }
    return str;
};

/*  自定义getElementsByClassName方法 --  兼容低版本IE
 *根据元素className得到元素集合
 *@param   fatherId 父元素的ID ，默认为document
 *@param    tagName 子元素的标签名
 *@param    className 用空格分开的className (类选择器名) 字符串
 * */
function getElementsByClassName(fatherId,tagName,cName){
    var node = fatherId && document.getElementById(fatherId) || document;
    tagName = tagName || "*";
    cName = cName.split(" ");
    var classNameLength = cName.length;
    var len = null;
    var i = null;
    for(i= 0,len=classNameLength;i<len;i++){
        //创建匹配类名的正则
        cName[i] = new RegExp( "(^|\\s)" + cName[i].replace(/\-/g,"\\-") +"(\\s|$)" );
    }
    var elements = node.getElementsByTagName(tagName);
    var result = [];
    var n = 0;
    for(i=0,len = elements.length;i<len;i++){
        var element = elements[i];
        while( cName[n++].test(element.className) ){    //优化循环
            if(n === classNameLength){
                result[result.length] = element;
                break;
            }
        }
        n = 0;
    }
    return result;
}



