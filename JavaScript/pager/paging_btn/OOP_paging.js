/**
 * 动态生成分页按钮
 * @param totalPages 分页总页数
 * @param pageNavId 分页按钮位置ID
 *
 * Created by Or_焰 on 2016/6/21.
 */
!function () {
    var Paging = function (options) {
        this.pages = options.currentPage || 1;
        this.totalPages = options.totalPages;
        this.pageNavId = options.pageNavId;
        this.pageNav = document.getElementById(this.pageNavId);
        this.pageStyleSheetId = 'PageStyleSheet';  //生成css样式表的id

        if (this.pageNavId && this.pageNav) this.init();
    };
    Paging.prototype = {
        init: function () {
            this.createCssStyleSheet();
            this.render();
            this.createPage();
        },
        update:function(callback){
            this.createPage();
            if(callback) callback();
        },
        createCssStyleSheet: function () {
            if (!document.styleSheets[this.pageStyleSheetId]) {
                var ss = document.createElement('style');
                ss.setAttribute('type', 'text/css');
                ss.id = this.pageStyleSheetId;
                var addCssStyle = 'a{color: #00349a;}' +
                    '#' + this.pageNavId + '{display:block;margin:15px 0 10px;overflow:hidden;font-size:14px;}' +
                    '#' + this.pageNavId + ' .page{float:left;font-size:12px;line-height:14px;left:50%;position:relative;}' +
                    '#' + this.pageNavId + ' .page span,' + '#' + this.pageNavId + ' .page a,' +
                    '#' + this.pageNavId + ' .page input{float:left;margin:0px 2px;padding:4px 8px 2px;position:relative;right:50%;border:1px solid #a7bacb;display: inline;}' +
                    '#' + this.pageNavId + ' .page span{font-weight:bold;color:#fff;border:1px solid #38628b;background-color:#38628b;}' +
                    '#' + this.pageNavId + ' .page a{background:#fff;color:#3669ba;text-decoration:none;vertical-align:middle;}' +
                    '#' + this.pageNavId + ' .page a:hover {border: 1px solid #286eb4;}' +
                    '#' + this.pageNavId + ' .page a.nolink{color:#777;cursor: default;border:1px solid #a7bacb;}' +
                    '#' + this.pageNavId + ' .page input{border:1px solid #999;width:30px;padding:3px 2px 2px;margin:0 3px;}' +
                    '#' + this.pageNavId + ' .page span.txt{border:1px solid #fff;color:#000;background:#fff;font-weight:normal;margin:0;padding:0;height:22px;line-height:20px;}' +
                    '#' + this.pageNavId + ' .page .btn_link{color:#000;cursor:pointer;border:1px solid #999;}';

                if( ss.styleSheet ){        //IE
                    ss.styleSheet.cssText = addCssStyle;
                }else{          //W3C
                    ss.appendChild(document.createTextNode(addCssStyle));
                }
                document.getElementsByTagName('head')[0].appendChild(ss);
            }
        },
        render: function () {
            this.pageContainer = document.createElement('div');
            this.pageNav.appendChild(this.pageContainer);
            this.pageContainer.className = 'page';
        },
        createPage: function () {
            var _t = this,
                p = this.pages || 1,
                totalPages = this.totalPages || 1,
                _pn = this.pageContainer;

            if (!_pn) return;
            _pn.innerHTML = '';
            p = isNaN(p) ? 1 : parseInt(p);
            if (p == totalPages && totalPages == 1) {
                _pn.parentNode.style.display = 'none';
                return;
            } else {
                _pn.parentNode.style.display = '';
            }
            //生成上一页按钮
            var _a = document.createElement('a');
            _a.setAttribute('href', 'javascript:void(0);');
            _a.setAttribute('target', '_self');
            _a.innerHTML = '上一页';
            _pn.appendChild(_a);
            if (p == 1) {
                _a.className = 'nolink';
                _a.onclick = function () {
                    return false;
                }
            } else {
                _a.className = '';
                _a.setAttribute('href', 'javascript:void(0);');
                _a.setAttribute('target', '_self');
                _a.setAttribute('title', '转到第' + (p - 1) + '页');
                _a.onclick = function () {
                    _t.go(p - 1);
                }
            }
            var start = (p > 3) ? p - 2 : 1;
            start = (p > totalPages - 3 && totalPages > 4) ? totalPages - 4 : start;
            var end = (start == 1) ? 5 : start + 4;
            end = (end > totalPages) ? totalPages : end;

            if (start > 1) {
                var prev = ((start - 3) < 1) ? 1 : (start - 3);
                //跳转到第一页
                _a = document.createElement('a');
                _pn.appendChild(_a);
                _a.setAttribute('href', 'javascript:void(0);');
                _a.setAttribute('target', '_self');
                _a.setAttribute('title', '转到第一页');
                _a.onclick = function () {
                    _t.go(1);
                };
                _a.innerHTML = 1;
                //生成上一组按钮
                if (prev > 1) {
                    _a = document.createElement('a');
                    _pn.appendChild(_a);
                    _a.setAttribute('href', 'javascript:void(0);');
                    _a.setAttribute('target', '_self');
                    _a.setAttribute('title', '转到上一组');
                    _a.onclick = function () {
                        _t.go(prev);
                    };
                    _a.className = 'prev';
                    _a.innerHTML = '...';
                }
            }

            //生成每一组所有按钮
            for (var i = start; i <= end; i++) {
                if (p == i) {
                    _a = document.createElement('span');
                    _pn.appendChild(_a);
                    _a.className = 'at';
                    _a.innerHTML = i;
                } else {
                    _a = document.createElement('a');
                    _pn.appendChild(_a);
                    _a.setAttribute("href", "javascript:void(0);");
                    _a.setAttribute("target", "_self");
                    _a.setAttribute("title", "转到第" + i + "页");
                    _a.onclick = (function (n) {
                        return function () {
                            _t.go(n);
                        }
                    })(i);
                    _a.innerHTML = i;
                }
            }
            if (totalPages > end) {
                var next = ((end + 3) > totalPages) ? totalPages : (end + 3);
                //生成下一组按钮
                _a = document.createElement('a');
                _pn.appendChild(_a);
                _a.setAttribute('href', 'javascript:void(0);');
                _a.setAttribute('target', '_self');
                _a.setAttribute('title', '转到下一组');
                _a.onclick = function () {
                    _t.go(next);
                };
                _a.className = 'next';
                _a.innerHTML = '...';
                //跳转到最后一页
                if (next < totalPages) {
                    _a = document.createElement('a');
                    _pn.appendChild(_a);
                    _a.setAttribute("href", "javascript:void(0);");
                    _a.setAttribute("target", "_self");
                    _a.setAttribute("title", "转到最后一页");
                    _a.onclick = function () {
                        _t.go(totalPages);
                    };
                    _a.innerHTML = totalPages;
                }
            }

            //生成下一页按钮
            _a = document.createElement('a');
            _pn.appendChild(_a);
            _a.setAttribute("href", "javascript:void(0);");
            _a.setAttribute("target", "_self");
            _a.innerHTML = '下一页';
            if (p == totalPages) {
                _a.className = 'nolink';
                _a.onclick = function () {
                    return false;
                };
            } else {
                _a.onclick = function () {
                    _t.go(p + 1);
                };
                _a.setAttribute('title', '转到' + (p + 1) + '页');
            }

            _a = document.createElement('span');
            _pn.appendChild(_a);
            _a.className = 'txt';
            _a.innerHTML = '&nbsp;&nbsp;转到';
            _a = document.createElement('input');
            _pn.appendChild(_a);
            _a.className = 'txt';
            _a.id = 'gopage';
            _a.value = p;
            _a = document.createElement('a');
            _pn.appendChild(_a);
            _a.className = 'btn_link';
            _a.onclick = function () {
                if (document.getElementById('gopage')) {
                    var gopage = document.getElementById('gopage').value;
                    if (isNaN(gopage) || parseInt(gopage) < 0) gopage = 1;
                    _t.go(gopage);
                }
            };
            _a.innerHTML = 'Go';
        },
        go: function (p) {
            var _t = this;
            p = (p > this.totalPages) ? this.totalPages : p;
            p = (p < 1) ? 1 : p;
            this.pages = p;
            setTimeout(function () {
                _t.update();
            }, 0);
        }
    };
    window['Paging'] = Paging;
}();





















