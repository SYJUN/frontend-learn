if (!Function.prototype.bind) {
    Function.prototype.bind = function () {
        var _t = this,
            obj = arguments[0],
            args = new Array();
        for (var i = 1; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return function () {
            return _t.apply(obj, args);
        }
    };
}
var pageit = {
    pages: 1,            //当前页数
    dataPages: 15,      //总页数
    pagenav: "pagenav",  //分页控件的ID
    init: function () {
        var self = this;
        self.update();
    },
    update: function () {
        var self = this;
        self.pageFn();
    },
    pageFn: function () {
        var self = this,
            p = self.pages || 1,
            pages = self.dataPages || 1;
        var _pn = self.pagenav;
        if (_pn == null) {
            return;
        }
        p = isNaN(p) ? 1 : parseInt(p);
        _pn = document.getElementById(_pn);
        if (!_pn) return;
        _pn.innerHTML = "";
        if (p == pages && pages == 1) {
            _pn.parentNode.style.display = "none";
            return;
        } else {
            _pn.parentNode.style.display = "";
        }
        var _a = document.createElement("a");
        _a.setAttribute("href", "javascript:void(0);");
        _a.setAttribute("target", "_self");
        _a.innerHTML = "上一页";
        _pn.appendChild(_a);
        if (p == 1) {
            _a.className = "nolink";
            _a.onclick = function () {
                return false;
            }
        } else {
            _a.className = "";
            _a.setAttribute("href", "javascript:void(0);");
            _a.setAttribute("target", "_self");
            _a.setAttribute("title", "转到第" + (p - 1) + "页");
            _a.onclick = function () {
                self.go(p - 1);
            }
        }
        var start = (p > 3) ? p - 2 : 1;
        start = ( p > pages - 3 && pages > 4 ) ? pages - 4 : start;
        var end = (start == 1) ? 5 : start + 4;
        end = (end > pages) ? pages : end;
        if (start > 1) {
            var prev = ((start - 3) < 1) ? 1 : (start - 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href", "javascript:void(0);");
            _a.setAttribute("target", "_self");
            _a.setAttribute("title", "转到第一页");
            _a.onclick = function () {
                self.go(1);
            };
            _a.innerHTML = 1;
            if (prev > 1) {
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href", "javascript:void(0);");
                _a.setAttribute("target", "_self");
                _a.setAttribute("title", "转到上一组");
                _a.onclick = function () {
                    self.go(prev);
                };
                _a.className = "prev";
                _a.innerHTML = "...";
            }
        }
        for (var i = start; i <= end; i++) {
            if (p == i) {
                _a = document.createElement("span");
                _pn.appendChild(_a);
                _a.className = "at";
                _a.innerHTML = i;
            } else {
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href", "javascript:void(0);");
                _a.setAttribute("target", "_self");
                _a.setAttribute("title", "转到第" + i + "页");
                _a.onclick = function (n) {
                    self.go(n);
                }.bind(this, i);
                _a.innerHTML = i;
            }
        }
        if (pages > end) {
            var next = ((end + 3) > pages) ? pages : (end + 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href", "javascript:void(0);");
            _a.setAttribute("target", "_self");
            _a.setAttribute("title", "转到下一组");
            _a.onclick = function () {
                self.go(next);
            };
            _a.className = "next";
            _a.innerHTML = "...";
            if (next < pages) {
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href", "javascript:void(0);");
                _a.setAttribute("target", "_self");
                _a.setAttribute("title", "转到最后一页");
                _a.onclick = function () {
                    self.go(pages);
                };
                _a.innerHTML = pages;
            }
        }
        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.setAttribute("href", "javascript:void(0);");
        _a.setAttribute("target", "_self");
        _a.innerHTML = "下一页";
        if (p == pages) {
            _a.className = "nolink";
            _a.onclick = function () {
                return false;
            }
        } else {
            _a.onclick = function () {
                self.go(p + 1);
            };
            _a.setAttribute("title", "转到" + (p + 1) + "页");
        }
        _a = document.createElement("span");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.innerHTML = "&nbsp;&nbsp;转到";
        _a = document.createElement("input");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.id = "gopage";
        _a.value = p;
        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.className = "btn_link";
        _a.onclick = function () {
            if (document.getElementById("gopage")) {
                var gopage = document.getElementById("gopage").value;
                if (isNaN(gopage) || parseInt(gopage) < 0) {
                    gopage = 1;
                }
                self.go(gopage);
            }
        };
        _a.innerHTML = "Go";

    },
    go: function (p) {
        var self = this;
        p = (p > self.dataPages) ? self.dataPages : p;
        p = (p < 1) ? 1 : p;
        self.pages = p;
        setTimeout(function () {
            self.update();
        }, 0);
    }
};

pageit.init();



