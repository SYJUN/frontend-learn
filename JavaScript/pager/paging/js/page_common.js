/*
 * 新闻列表
 * time:2016/03/23
 * */
var newsListContent = {
    data: newsListData,     //读取的数据
    page: 1,                 //分页的页数初始数
    pagelistcount: 5,     //每页显示的列表数量
    pageLen:1,              //显示总的分页数量
    temvar:null,            //临时存储变量

    parentElement: document.getElementById("newsListContent"),
    pagerNoDiv:document.getElementById("pagerNoDiv"),
    init: function () { //初始化
        var self = this;
        self.temvar = self.pagelistcount;
        self.pageLen = Math.ceil(this.data.length / this.pagelistcount);
        self.update();
    },
    update:function(){
        var self = this;
        self.newsList(self.page);
        self.pageFn();
    },
    newsList:function(n){     //刷新页面数据
        var self = this;
        /*
         * 0 - 跳转链接地址  aLink
         * 1 - 图片地址 imgUrl
         * 2 - 文章标题 Title
         * 3 - 详情信息 Info
         * 4 - 发表时间  time
         * 5 - 分享者链接 shareUrl
         * */
        var dataLi = '<li>' +
            '<div class="image">' +
            '<a href="{0}" target="_blank">' +
            '<img src="{1}" alt=""/>' +
            '</a>' +
            '</div>' +
            '<div class="text">' +
            '<p class="title">' +
            '<a href="{0}" target="_blank">{2}</a>' +
            '</p>' +
            '<p class="info" title="{2}">{3}</p>' +
            '<p class="time">{4}</p>' +
            '</div>' +
            '<div class="share">' +
            '<span class="shareIco"></span>' +
            '<div class="shareContent" data="{\'url\':\'\{5\}\',text:\'\{2\}\'}">' +
            '<a href="#" class="bds_iguba" title="分享到股吧"></a>' +
            '<a href="#" class="bds_tsina" title="分享到新浪微博"></a>' +
            '<a href="#" class="bds_qzone" title="分享到QQ空间"></a>' +
            '<a href="#" class="bds_tqq" title="分享到腾讯微博"></a>' +
            '</div>' +
            '</div>' +
            '</li>';

        var lihtml = "";
        //如果   数据总长度 - 上一页显示完的总数量 = 剩下未显示的数据 < 每页规定显示的数量
        if( self.data.length - self.pagelistcount * (n-1)  < self.pagelistcount ){
            self.pagelistcount = self.pagelistcount - self.pagelistcount * self.pageLen % self.data.length;
        }else{
            self.pagelistcount = self.temvar;
        }

        for (var i = 0; i < self.pagelistcount; i++) {
            var current = self.data[i + self.temvar * (n - 1)];
            var info = current.Info.length > 70 ? current.Info.slice(0, 70) + "..." : current.Info;
            var str = dataLi.format(
                current.aLink,
                current.imgUrl,
                current.Title,
                info,
                current.time,
                current.shareUrl
            );
            lihtml += str;
        }
        self.parentElement.innerHTML = lihtml;

        self.newsListMouseover();
    },
    newsListMouseover: function () {       //新闻列表的鼠标移入事件
        var self = this;
        var lis = self.parentElement.getElementsByTagName("li");
        var len = lis.length;
        var i;
        len = lis.length;
        //每个li 的鼠标移入事件
        for (i = 0; i < len; i++) {
            lis[i].onmouseover = function () {
                this.style.backgroundColor = "#f7f7f7";
            };
            lis[i].onmouseout = function () {
                this.style.backgroundColor = "";
            };
        }
        //每个分享的鼠标移入事件
        var share = getElementsByClassName(self.parentElement, "div", "share");
        for (i = 0; i < len; i++) {
            share[i].onmouseover = function () {
                this.getElementsByTagName("div")[0].style.display = "block";
            };
            share[i].onmouseout = function () {
                this.getElementsByTagName("div")[0].style.display = "";
            };
        }
    },
    pageFn:function(){
        var self = this,
            p = self.page || 1,
            pages = self.pageLen || 1;
        var _pn = self.pagerNoDiv;
        if(_pn == null){
            return;
        }
        p = isNaN(p) ? 1 : parseInt(p);
        /*_pn = document.getElementById(_pn);*/
        if(!_pn) return;
        _pn.innerHTML = ""; //每次渲染页面的时候，都初始化一次
        if( p == pages && pages == 1 ){
            _pn.parentNode.style.display = "none";
            return;
        }else{
            _pn.parentNode.style.display = "";
        }
        var _a = document.createElement("a");
        _a.setAttribute("href","javascript:void(0);");
        _a.setAttribute("target","_self");
        _a.innerHTML = "上一页";
        _pn.appendChild(_a);
        if(p == 1){
            _a.className = "nolink";
            _a.onclick = function(){
                return false;
            }
        }else{
            _a.className = "";
            _a.setAttribute("title","转到第"+ (p - 1) +"页");
            _a.onclick = function(){
                self.go( p - 1);
            }
        }

        //按钮开始变化的位置
        var start = (p > 3) ? p - 2 : 1;                                //刚开始的初始位置变化
        start = ( p > pages - 3 && pages > 4 ) ? pages - 4 : start;     //终点的初始位置变化
        console.log("start:" + start);
        //按钮结束变化的位置
        var end = (start == 1) ? 5 : start + 4;     //刚开始的结束位置变化
        end = (end > pages) ? pages : end;          //终点的结束位置变化
        console.log("end:"+end);
        if(start > 1){
            var prev = ((start - 3) < 1) ? 1 : (start - 3);
            console.log("prev:"+ prev);
            //var _prev = ((start - 3) > pages) ? pages : (next + 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href","javascript:void(0);");
            _a.setAttribute("target","_self");
            _a.setAttribute("title","转到第一页");
            _a.onclick = function() {
                self.go(1);
            };
            _a.innerHTML = 1;
            if(prev > 1){       //当 p = 7，即start = 5时，执行
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到上一组");
                _a.onclick = function(){
                    self.go(prev);
                };
                _a.className = "next";
                _a.innerHTML = "...";
            }
        }
        for(var i = start;i <= end;i++){
            if(p == i){
                //处于当前页时创建 span 标签
                _a = document.createElement("span");
                _pn.appendChild(_a);
                _a.className = "at";
                _a.innerHTML = i;
            }else{
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到第"+ i +"页");
                _a.onclick = function(n){
                    self.go(n);
                }.bind(this,i);
                _a.innerHTML = i;
            }
        }
        if(pages > end){
            var next = ((end + 3) > pages) ? pages : (end + 3);
            console.log("next:"+next);
            //var _next = ((next + 3) > pages) ? pages : (next + 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href","javascript:void(0);");
            _a.setAttribute("target","_self");
            _a.setAttribute("title","转到下一组");
            _a.onclick = function(){
                self.go(next);
            };
            _a.className = "next";
            _a.innerHTML = "...";
            if(next < pages){
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到最后一页");
                _a.onclick = function(){
                    self.go(pages);
                };
                _a.innerHTML = pages;
            }
        }
        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.setAttribute("href","javascript:void(0);");
        _a.setAttribute("target","_self");
        _a.innerHTML = "下一页";
        if(p == pages){
            _a.className = "nolink";
            _a.onclick = function(){
                return false;
            }
        }else{
            _a.onclick = function(){
                self.go(p + 1);
            };
            _a.setAttribute("title","转到" + (p + 1) + "页");
        }
        _a = document.createElement("span");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.innerHTML = "&nbsp;&nbsp;转到";
        _a = document.createElement("input");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.id = "gopage";
        _a.value = p;

        _a = document.createElement("span");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.innerHTML = "页";

        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.className = "btn_link";
        _a.onclick = function(){
            if(document.getElementById("gopage")){
                var gopage = document.getElementById("gopage").value;
                if(isNaN(gopage) || parseInt(gopage) < 0){
                    gopage = 1;
                }
                self.go(gopage);
            }
        };
        _a.innerHTML = "Go";

    },
    go:function(p){
        var self = this;
        p = (p > self.pageLen) ? self.pageLen : p;
        p = (p < 1) ? 1 : p;
        self.page = p;
        setTimeout(function(){
            self.update();
        },0);
    }
};


















