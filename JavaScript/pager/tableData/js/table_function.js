Function.prototype.bind = function(){
  var _t = this,
      obj = arguments[0],
      args = [];
  for(var i = 1;i<arguments.length;i++){
      args.push(arguments[i]);
  }
  return function(){
      return _t.apply(obj,args);
  }
};
String.prototype.format = function(){
    var str = this;
    for(var i= 0;i<arguments.length;i++){
        var reg = new RegExp("\\{" + i + "\\}","gm");
        str = str.replace(reg,arguments[i]);
    }
    return str;
};

var Class = {
    create:function(){
        return function(){
            this.initialize.apply(this,arguments);
        }
    }
};
//传入两个参数对象
/*
Object.extend = function(destination, source){
    for(prototype in source){
        destination[prototype] = source[prototype];
    }
    return destination;
};
*/

Object.extend = function(target,source){
    for(var k in source){
        target[k] = source[k];
    }
    return target;
};

var LoadTable = Class.create();
/*
* 功能：动态加载表格数据
* 参数说明
* {
*   id：展示数据table 的 id
*   data: 传入的数据
*   page: 当前的页数，初始化为 1
*   pages: 总的页数，初始化为 1
*   pagesize: 每页显示的数据行数
*   cells: 展示字段{ "n": "表头名称"} 可多个
*   cellcount: 默认可不填，在多行表头时使用，传入实际数据的列数
*   pagenav: 分页导航条id
* }
* */

Object.extend(LoadTable.prototype,{
    initialize:function(opt){
        var self = this;
        var options = {
            id:"tableData",
            table:null,
            thead:null,
            tbody:null,
            data:[],
            page:1,
            pages:1,
            pagesize:5,
            cells:10,
            pagenav:"PageCont"
        };
        Object.extend(options,opt || {});
        self.options = options;
        var _table = document.getElementById(self.options.id);
        if( !_table ){
            return;
        }else{
            self.options.table = _table;
            self.loadThead();
        }
        if(self.options.data.length < 1){
            return alert("没有数据！");
        }
        self.options.pages = Math.ceil(self.options.data.length/self.options.pagesize);
        self.update();

    },
    update:function(){
        var self = this;
        self.loadData( self.options.page );
        self.hoverTable();
        self.pager();
    },
    loadThead:function(){
        var self = this,
            _cells = self.options.cells,
            _table = self.options.table,
            _thead = document.createElement("thead");
        _thead.className = "h01";
        var rowTh1 = _thead.insertRow(0);
        var rowTh2 = _thead.insertRow(1);
        var len = _cells.length;
        var th;
        for(var i = 0;i < len;i++){
            if ( _cells[i].cells ){
                th = document.createElement("th");
                th.colSpan = "2";
                th.innerHTML = _cells[i].n;
                rowTh1.appendChild(th);

                th = document.createElement("th");
                th.innerHTML = _cells[i].cells[0].n;
                rowTh2.appendChild(th);

                th = document.createElement("th");
                th.innerHTML = _cells[i].cells[1].n;
                rowTh2.appendChild(th);

            }else{
                th = document.createElement("th");
                th.rowSpan = "2";
                th.innerHTML = _cells[i].n;
                rowTh1.appendChild(th);
            }
        }
        _table.appendChild(_thead);
        self.options.thead = _table.getElementsByTagName("thead")[0];
    },
    loadData:function(p){
        var self = this,
            _pagesize = self.options.pagesize,
            _data = self.options.data,
            _table = self.options.table,
            _tbody = document.createElement("tbody");
        _table.appendChild(_tbody);
        self.options.tbody = _table.getElementsByTagName("tbody")[0];
        /*
        * 0 - 日期
        * 1 - 总票数
        * 2 - 票数（上涨）
        * 3 - 比例（上涨）
        * 4 - 票数（盘整）
        * 5 - 比例（盘整）
        * 6 - 票数（下跌）
        * 7 - 比例（下跌）
        * 8 - 票数（仓位大于70%）
        * 9 - 比例（仓位大于70%）
        * 10 - 票数（仓位30%～70%）
        * 11 - 比例（仓位30%～70%）
        * 12 - 票数（仓位小于30%）
        * 13 - 比例（仓位小于30%）
        * 14 - 加权仓位
        * 15 - 上证指数当日涨跌幅
        * */
        var tbodyData = '<tr>' +
                            '<td>{0}</td>' +
                            '<td>{1}</td>' +
                            '<td>{2}</td>' +
                            '<td>{3}</td>' +
                            '<td>{4}</td>' +
                            '<td>{5}</td>' +
                            '<td>{6}</td>' +
                            '<td>{7}</td>' +
                            '<td>{8}</td>' +
                            '<td>{9}</td>' +
                            '<td>{10}</td>' +
                            '<td>{11}</td>' +
                            '<td>{12}</td>' +
                            '<td>{13}</td>' +
                            '<td>{14}</td>' +
                            '<td class="red">{15}</td>' +
                        '</tr>';
        var trHtml = "";

        function dataVote(obj,total){
            return (parseInt(obj)/total).toFixed(4) * 100 +"%";
        }
        var i,len;
        if(!p){
            return
        }else{
            if( p == 1){
                i = 0;
                len = _pagesize;
            }else{
                i = (p - 1) * _pagesize;
                len = (p * _pagesize) > _data.length ? _data.length : p * _pagesize;
            }
            self.options.tbody.innerHTML = "";
        }
         for( ;i < len;i++){
             var total = parseInt(_data[i].grail["up"]) + parseInt(_data[i].grail["balance"]) + parseInt(_data[i].grail["down"]);
             var trs = tbodyData.format(
                 (_data[i].dataId + 1) +"&nbsp;&nbsp;&nbsp;" + _data[i].time,
                total,
                _data[i].grail["up"],
                 dataVote(_data[i].grail["up"],total),
                _data[i].grail["balance"],
                 dataVote(_data[i].grail["balance"],total),
                _data[i].grail["down"],
                 dataVote(_data[i].grail["down"],total),
                _data[i].positions["up"],
                 dataVote(_data[i].positions["up"],total),
                _data[i].positions["balance"],
                 dataVote(_data[i].positions["balance"],total),
                _data[i].positions["down"],
                 dataVote(_data[i].positions["down"],total),
                 _data[i].weighting,
                 _data[i].change
            );
             trHtml += trs;
        }
        self.options.tbody.innerHTML = trHtml;

    },
    hoverTable:function(){
        var self = this,
            _tbody = self.options.table.getElementsByTagName("tbody")[0],
            trs = _tbody.getElementsByTagName("tr"),
            len = trs.length;
        for(var i = 0;i < len;i++){
            trs[i].onmouseover = function(){
                this.className = "over";
            };
            trs[i].onmouseout = function(){
                this.className = "";
            };
        }
    },
    pager:function(){
        var self = this,
            p = self.options.page || 1,
            pages = self.options.pages || 1;
        var _pn = self.options.pagenav;
        if( _pn == null ) return;
        p = isNaN(p) ? 1 : parseInt(p);
        _pn = document.getElementById(_pn);
        if(!_pn) return;
        _pn.innerHTML = "";
        if(p == 1 && pages == 1){
            _pn.parentNode.style.display = "none";
            return ;
        }else{
            _pn.parentNode.style.display = "";
        }
        var _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.setAttribute("href","javascript:void(0);");
        _a.setAttribute("target","_self");
        _a.innerHTML = "上一页";
        if(p == 1){
            _a.className = "nolink";
            _a.onclick = function(){ return false; }
        }else {
            _a.className = "";
            _a.setAttribute("title", "转到第" + (p - 1) + "页");
            _a.onclick = function () { self.go(p - 1); }
        }
        var start = (p > 3) ? p - 2 : 1;
        start = (p > pages - 3 && pages > 4) ? pages - 4 : start;
        var end = (start == 1) ? 5 : start + 4;
        end = (end > pages) ? pages : end;
        if(start > 1){
            var prev = (start - 3) < 1 ? 1 : (start - 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href","javascript:void(0);");
            _a.setAttribute("target","_self");
            _a.setAttribute("title","转到第一页");
            _a.innerHTML = 1;
            _a.onclick = function(){ self.go(1);  };

            if(prev > 1){
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到上一组");
                _a.onclick = function(){ self.go(prev); };
                _a.className = "next";
                _a.innerHTML = "...";
            }
        }

        for(var i = start;i <= end;i++){
            if( p == i ){
                _a = document.createElement("span");
                _pn.appendChild(_a);
                _a.innerHTML = i;
                _a.className = "at";
            }else{
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到第"+ i + "页");
                _a.innerHTML = i;
                _a.onclick = function(n){
                    self.go(n);
                }.bind(this,i);
            }
        }

        if(pages > end){
            var next = (end + 3) > pages ? pages : (end + 3);
            _a = document.createElement("a");
            _pn.appendChild(_a);
            _a.setAttribute("href","javascript:void(0);");
            _a.setAttribute("target","_self");
            _a.setAttribute("title","转到下一组");
            _a.innerHTML = "...";
            _a.className = "next";
            _a.onclick = function(){ self.go(next); };
            if( next < pages ){
                _a = document.createElement("a");
                _pn.appendChild(_a);
                _a.setAttribute("href","javascript:void(0);");
                _a.setAttribute("target","_self");
                _a.setAttribute("title","转到最后一页");
                _a.innerHTML = pages;
                _a.onclick = function(){ self.go(pages); };
            }
        }
        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.setAttribute("href","javascript:void(0);");
        _a.setAttribute("target","_self");
        _a.innerHTML = "下一页";
        if(p == pages){
            _a.className = "nolink";
            _a.onclick = function(){ return false; };
        }else{
            _a.setAttribute("title","转到"+ (p + 1) + "页");
            _a.className = "";
            _a.onclick = function(){ self.go(p + 1); };
        }
        _a = document.createElement("span");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.innerHTML = "&nbsp;&nbsp;转到第";
        _a = document.createElement("input");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.id = "gopage";
        _a.value = p;
        _a = document.createElement("span");
        _pn.appendChild(_a);
        _a.className = "txt";
        _a.innerHTML = "&nbsp;页";
        _a = document.createElement("a");
        _pn.appendChild(_a);
        _a.className = "btn_link";
        _a.innerHTML = "Go";
        _a.onclick = function(){
            if(document.getElementById("gopage")){
                var gopage = document.getElementById("gopage").value;
                if(isNaN(gopage) || parseInt(gopage) < 0){
                    gopage = 1;
                }
                self.go(gopage);
            }
        };

    },
    go:function(p){
        var self = this;
        p = (p > self.options.pages) ? self.options.pages : p;
        p = (p < 1) ? 1 : p;
        self.options.page = p;
        setTimeout(function(){
            self.update();
        },0);

    }
});

var runTable = new LoadTable({
    data:tableData,
    pagesize:6,
    cells:[
        {n:"日期"},
        {n:"总票数"},
        {n:"上涨",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"盘整",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"下跌",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"仓位大于70%",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"仓位30%～70%",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"仓位小于30%",cells:[
            {n:"票数"},{n:"比例"}
        ]},
        {n:"加权仓位"},
        {n:"上证指数"+ '<br>'+"当日涨跌幅"}
    ]
});



























