
//前台调用
var $ = function(_this){
    return new Base(_this);
};
//基础库
function Base(_this){
    //创建一个数组，来保存获取的节点和节点数组
    this.elements = [];
    if(_this != undefined){     //_this是一个对象，undefined也是一个对象，区别与typeof返回的带单引号的'undefined
        this.elements[0] = _this;
    }
}
Base.prototype = {
    getId:function(id){             //获取 ID节点
        this.elements.push(document.getElementById(id));
        return this;
    },
    getTagName:function(tag){       //获取元素节点
        var tags = document.getElementsByTagName(tag);
        for(var i = 0;i< tags.length;i++){
            this.elements.push(tags[i]);
        }
        return this;
    },
    getClass:function(className,parentName){            //获取class 节点数组
        var node = null;
        if(arguments.length == 2){
            if(!this.getId(parentName)) return;
            node = this.getId(parentName).elements[0];
        }else{
            node = document;
        }
        var all = node.getElementsByTagName('*');
        for(var i = 0;i < all.length;i++){
            if(all[i].className == className){
                this.elements.push(all[i]);
            }
        }
        return this;
    },
    addClass:function(className){        //添加Class
        for(var i = 0;i<this.elements.length;i++){
            if( !this.elements[i].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)')) ){
                this.elements[i].className += ' '+ className;
            }
        }
        return this;
    },
    removeClass:function(className){             //移除class
        for(var i = 0;i<this.elements.length;i++){
            if( this.elements[i].className.match(new RegExp('(\\s|^)' + className + '(\\s|$)')) ){
                this.elements[i].className = this.elements[i].className.replace( new RegExp('(\\s|^)' + className + '(\\s|$)'),'' );
            }
        }
        return this;
    },
    getElement:function(num){           //获取某一个元素
        var element = this.elements[num];
        this.elements = [];
        this.elements[0] = element;
        return this;
    },
    addRule:function(num,selectorText,cssText,position){             //添加Link 或 style 的css规则
        var sheet = document.styleSheets[num];
        if(typeof sheet.insertRule != 'undefined'){         //W3C
            sheet.insertRule(selectorText + '{' + cssText+ '}',position)
        }else if(typeof sheet.addRule != 'undefined'){      //IE
            sheet.addRule(selectorText,cssText,position)
        }
        return this;
    },
    removeRule:function(num,index){              // 移除link  或  style 的 css规则
        var sheet = document.styleSheets[num];
        if(typeof sheet.deleteRule != 'undefined'){         //W3C
            sheet.deleteRule(index)
        }else if(typeof sheet.removeRule != 'undefined'){      //IE
            sheet.removeRule(index)
        }
        return this;
    },
    css:function(attr,value){       //设置css
        for(var i = 0;i < this.elements.length;i++){
            if(arguments.length == 1){
                if(typeof window.getComputedStyle != 'undefined'){  //W3C
                    return window.getComputedStyle(this.elements[i],null)[attr];
                }else if(typeof this.elements[i].currentStyle != 'undefined'){
                    return this.elements[i].currentStyle[attr];
                }
            }
            this.elements[i].style[attr] = value;
        }
        return this;
    },
    html:function(str){             //设置html
        for(var i = 0;i < this.elements.length;i++){
            if(arguments.length == 0){
                return this.elements[i].innerHTML;
            }
            this.elements[i].innerHTML = str;
        }
        return this;
    },
    center:function(width,height){          //居中显示
        var top = (document.documentElement.clientWidth - width)/ 2,
            left = (document.documentElement.clientHeight - height)/2;
        for(var i = 0;i < this.elements.length;i++){
            this.elements[i].style.top = top + 'px';
            this.elements[i].style.left = left + 'px';
        }
        return this;
    },
    resize:function(fn){
        window.onresize = fn;
        return this;
    },
    click:function(fn){             //触发点击事件
        for(var i = 0;i < this.elements.length;i++){
            this.elements[i].onclick = fn;
        }
        return this;
    },
    hover:function(over,out){       //设置鼠标移入移出方法
        for(var i = 0;i < this.elements.length;i++){
            this.elements[i].onmouseover = over;
            this.elements[i].onmouseout = out;
        }
        return this;
    },
    show:function(){            //设置显示
        for(var i = 0;i < this.elements.length;i++){
            this.elements[i].style.display = 'block';
        }
        return this;
    },
    hide:function(){            //设置隐藏
        for(var i = 0;i < this.elements.length;i++){
            this.elements[i].style.display = 'none';
        }
        return this;
    }
};









