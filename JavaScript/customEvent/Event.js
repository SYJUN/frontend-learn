/**
 * JavaScript实现自定义对象的自定义事件
 * @type {{_listeners: {}, addEvent: Function, fireEvent: Function, removeEvent: Function}}
 */
var EventTarget = function () {
    /**
     * 用来保存事件类型以及相同类型的所用事件
     */
    this._listeners = {};
};

EventTarget.prototype = {
    constructor: this,
    /**
     * 添加自定义事件 -- 单事件
     * @param type
     * @param fn
     */
    addEvent: function (type, fn) {
        if (typeof type === 'string' || typeof fn === 'function') {
            if (typeof this._listeners[type] === 'undefined') {
                this._listeners[type] = [fn];
            } else {
                this._listeners[type].push(fn);
            }
        }
        return this;
    },
    /**
     * 添加自定义事件 -- 多事件
     * @param obj
     * @returns {EventTarget}
     */
    addEvents: function (obj) {
        obj = typeof obj === 'object' ? obj : {};
        for (var type in obj) {
            if (type && typeof obj[type] === 'function') {
                this.addEvent(type, obj[type]);
            }
        }
        return this;
    },

    /**
     * 触发自定义事件 -- 单事件
     * @param type
     */
    fireEvent: function (type) {
        if (type && this._listeners[type]) {
            var events = {
                type: type,
                target: this
            };
            for (var i = 0, len = this._listeners[type].length; i < len; i++) {
                this._listeners[type][i].call(this, events);
            }
        }
        return this;
    },
    /**
     * 触发自定义事件 -- 多事件
     * @param array
     * @returns {EventTarget}
     */
    fireEvents: function (array) {
        if (array instanceof Array) {
            for (var i = 0, len = array.length; i < len; i++) {
                this.fireEvent(array[i]);
            }
        }
        return this;
    },

    /**
     * 删除自定义事件 -- 单事件
     * @param type
     * @param fn
     */
    removeEvent: function (type, fn) {
        var EventArray = this._listeners[type];
        if (typeof type === 'string' && EventArray instanceof Array) {
            if (typeof fn === 'function') {
                //清除当前type类型事件下对应的fn方法
                for (var i = 0, len = EventArray.length; i < len; i++) {
                    if (EventArray[i] === fn) {
                        this._listeners[type].splice(i, 1);
                        break;
                    }
                }
            } else if (fn instanceof Array) {
                for (var j = 0, fnlen = fn.length; j < fnlen; j++) {
                    this.removeEvent(type,fn[j]);
                }
            } else {
                //如果仅仅参数type，或参数fn，则所有type类型事件清除
                delete this._listeners[type];
            }
        }
        return this;
    },
    /**
     * 删除自定义事件 -- 多事件
     * @param params
     */
    remveEvents:function(params){
        //params为类型数组时
        if(params instanceof Array){
            for(var i = 0,len = params.length;i<len;i++){
                this.removeEvent(params[i]);
            }
        }else if(typeof params === 'object'){
            //params为对象{ type:fn }键值对时
            for(var type in params){
                this.removeEvent(type,params[type]);
            }
        }
        return this;
    }
};






