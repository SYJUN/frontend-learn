/*
* IE9以下 中的XML
*
* 创建XML DOM
* var xmlDom = new ActiveXObject('MSXML2.DOMDocument');
* alert(xmlDom);
* */
//IE浏览器创建 XML DOM
function createXMLDOM(){
    var version = [
        'MSXML2.DOMDocument6.0',
        'MSXML2.DOMDocument3.0',
        'MSXML2.DOMDocument'
    ];
    for(var i = 0;i< version.length;i++){
        try{
            var xmlDom = new ActiveXObject(version[i]);
            return xmlDom;
        }catch(e){
            //跳过
        }
    }
    throw new Error('您的系统或浏览器不支持MSXML库！');
}
/*载入XML文件，两种方式：
    1.加载XML字符串loadXML();
    2.加载XML外部文件load()
var xmlDom =createXMLDOM();
xmlDom.loadXML('<root>\n<user>Lee</user>\n</root>');
alert(xmlDom.xml);
var user = xmlDom.getElementsByTagName('user')[0];
alert(user.nodeType);
alert(user.tagName);
alert(user.firstChild.nodeValue);
*/

//加载外部文件（服务器端加载外部文件为异步加载）
//在服务器端，使用的是异步加载，load() 还没有加载完毕，就去打印xmlDom.xml序列化的字符串


/*
* 服务器端--同步加载
*
* var xmlDom = createXMLDOM();
 xmlDom.async = false;       //改成同步加载，默认true 为异步加载
 xmlDom.load('demo.xml');
 alert(xmlDom.xml);
* */

/*
异步加载
*/
var xmlDom = createXMLDOM();
xmlDom.async = true;
/*
* readystateChange事件就绪状态：
*   readyState 获取就绪状态值
*   1 -- DOM 正在加载
*   2 -- DOM 已经加载完数据
*   3 -- DOM 已经可以使用，但某些部分还无法访问
*   4 -- DOM 已经完全可以
*
* */
//onreadystatechange 比较特殊，里面的this表示window 而不是执行的object

/*
 * 解析错误
 * parseError属性对象
 *   errorCode   --  发生的错误类型的数字代号
 *   filepos     --  发生错误文件中的位置
 *   line        --  错误行号
 *   linepos     --  遇到错误行号那一行上的字符的位置
 *   reason      --  错误的解释信息
 *
 * */

xmlDom.onreadystatechange = function(){
    if(xmlDom.readyState == 4){
        if(xmlDom.parseError.errorCode == 0){
            alert(xmlDom.xml);
        }else{
            throw new Error('错误行号：' + xmlDom.parseError.line +
                    '\n错误代号：'+ xmlDom.parseError.errorCode +
                    '\n错误解释：'+ xmlDom.parseError.reason
            );
        }
    }
};
xmlDom.load('demo.xml');    //放在后面重点体现异步的作用

















