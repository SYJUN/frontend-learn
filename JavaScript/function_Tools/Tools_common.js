/*
 * 功能：实现类式继承
 *       创建一个空对像，并且原型__proto__指向传递的参数
 * 注意：Object.create只传第一个参数时，是创建原型指向参数的空对象。但是其第二个参数是可以设置新对象的属性及权限的。
 * */
if (!Object.create) {
    Object.create = function (proto) {
        function F() {}
        F.prototype = proto;
        return new F;
    };
}

/*
 * 功能：格式化本地时间
 * 格式：2016年03月22日 11：13
 *
 * */
function formatDate() {
    var date = new Date();
    var month = (date.getMonth() + 1) < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
    var h = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var m = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

    return date.getFullYear() + "年" + month + "月" + day + "日 " + h + "：" + m;
}


/*
 * 功能：动态加载js文件
 * time:2016/03/25
 *
 * */
function loadScript(url, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Other
        script.onload = function () {
            callback();
        };
    }
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

/*
 * 功能：扩展function  原型上面的方法
 * bind() 为该事件对象传参
 * PS : _a.onclick = function(n){
 self.go(n);
 }.Bind(this,i);
 *
 * */
/*
 Function.prototype.Bind = function(){
 var _t = this,
 obj = arguments[0],
 args = new Array();
 for(var i = 1;i < arguments.length;i++){
 args.push(arguments[i]);
 }
 return function(){
 return _t.apply(obj,args);
 }
 };
 */
if (!Function.prototype.bind) {
    Function.prototype.bind = function (obj) {
        var _t = this,
            arr = [],
            args = arr.call(arguments, 1),
            nop = function () {
            },
            bound = function () {
                return _t.apply(this instanceof nop ? this : (obj || {}), args.concat(arr.call(arguments)));
            };
        nop.prototype = _t.prototype;
        bound.prototype = new nop();
        return bound;
    }
}


/*
 * 功能：判断是否是 IE 以及IE的版本
 *
 * PS: isIE() === false
 * */

function isIE() {
    var navUA = navigator.userAgent.toLocaleLowerCase();
    return (navUA.indexOf('msie') != -1) ? parseInt(navUA.split('msie')[1]) : false;
}

/*
 * 功能：跨浏览器添加事件
 * 参数：   obj：事件对象
 *          type：时间类型
 *          fn:事件的回调函数
 * */
function addEvent(obj, type, fn) {
    //检测浏览器类型，并且重写 addEvent 方法
    if (typeof obj.addEventListener != 'undefined') {   //非IE浏览器
        addEvent = function (obj, type, fn) {
            obj.addEventListener(type, fn, false);
        };
    } else if (typeof obj.attachEvent != 'undefined') { //IE浏览器
        addEvent = function (obj, type, fn) {
            obj.attachEvent("on" + type, function () {
                fn.call(obj, window.event);   //IE 下 this指向 window,对象冒充来传递obj对象
            });
        };
    }
    //调用新的函数
    addEvent(obj, type, fn);
}

/*
 * 功能：跨浏览器移除事件
 * 参数：   obj：事件对象
 *          type：时间类型
 *          fn:事件的回调函数
 * */
function removeEvent(obj, type, fn) {
    if (typeof obj.removeEventListener != 'undefined') {   //非IE浏览器
        removeEvent = function (obj, type, fn) {
            obj.removeEventListener(type, fn, false);
        };
    } else if (typeof obj.detachEvent != 'undefined') {   //IE浏览器
        removeEvent = function (obj, type, fn) {
            obj.detachEvent("on" + type, fn);
        };
    }
    //调用新的函数
    removeEvent(obj, type, fn);
}

/*
 * 功能：跨浏览器阻止默认行为
 * */
function preDef(evt) {
    var e = evt || window.event;
    if (e.preventDefault) {
        e.preventDefault();
    } else {
        e.returnValue = false;
    }
}

/*
 *功能：夸浏览器获取字符编码
 */

function getCharCode(evt) {
    var e = evt || window.event;
    if (typeof e.charCode == 'number') {
        return e.charCode;
    } else {
        return e.keyCode;
    }
}

/*
 * 功能：继承对象
 * @param {Object} target 目标对象。
 * @param {Object} source 源对象。
 * @param {boolean} deep 是否复制(继承)对象中的对象。
 *
 * @returns {Object} 返回继承了source对象属性的新对象。
 */
if(!Object.extend){
    Object.extend = function (target, /*optional*/source, /*optional*/deep) {
        target = target || {};
        var sType = typeof source, i = 1, options;
        if (sType === 'undefined' || sType === 'boolean') {
            deep = sType === 'boolean' ? source : false;
            source = target;
            target = this;
        }
        if (typeof source !== 'object' && Object.prototype.toString.call(source) !== '[object Function]')
            source = {};
        while (i <= 2) {
            options = i === 1 ? target : source;
            if (options != null) {
                for (var name in options) {
                    var src = target[name], copy = options[name];
                    if (target === copy)
                        continue;
                    if (deep && copy && typeof copy === 'object' && !copy.nodeType)
                        target[name] = this.extend(src ||
                        (copy.length != null ? [] : {}), copy, deep);
                    else if (copy !== undefined)
                        target[name] = copy;
                }
            }
            i++;
        }
        return target;
    };
}


/********************
 * 取窗口滚动条高度
 ******************/
function getScrollTop() {
    var scrollTop = 0;
    if (document.documentElement && document.documentElement.scrollTop) {
        scrollTop = document.documentElement.scrollTop;
    }
    else if (document.body) {
        scrollTop = document.body.scrollTop;
    }
    return scrollTop;
}

/********************
 * 取窗口可视范围的高度
 *******************/
function getClientHeight() {
    var clientHeight = 0;
    if (document.body.clientHeight && document.documentElement.clientHeight) {
        clientHeight = (document.body.clientHeight < document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
    }
    else {
        clientHeight = (document.body.clientHeight > document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
    }
    return clientHeight;
}

/********************
 * 取文档内容实际高度
 *******************/
function getScrollHeight() {
    return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
}

/*
 * 扩展forEach 方法
 * 参数：
 *   callback : 函数测试数组的每个元素调用该回调函数，该回调函数可传三个参数：
 *           value : 数组元素的值
 *           index : 数组元素的数字索引
 *           array1 : 包含该元素的数组对象
 *   thisArg ： 可选。 可在 callback 函数中为其引用 this 关键字的对象。 如果省略 thisArg，则 undefined 将用作 this 值。
 * */
if (!Array.prototype.forEach) {
    Array.prototype.forEach = function (callback, thisArg) {
        var T, k;
        if (this == null) {
            throw new TypeError(" this is null or not defined");
        }
        var O = Object(this);       //相当于 O = new Object(this)
        var len = O.length >>> 0; // Hack to convert O.length to a UInt32
        if ({}.toString.call(callback) != "[object Function]") {
            throw new TypeError(callback + " is not a function");
        }
        if (thisArg) {
            T = thisArg;
        }
        k = 0;
        while (k < len) {
            var kValue;
            if (k in O) {
                kValue = O[k];
                callback.call(T, kValue, k, O);
            }
            k++;
        }
    };
}

/*
 * 原生js动态生成css样式表
 * @param styleId 给生成的样式表定义一个id
 * @param cssTxt css属性集合
 * */
function createCssStyleSheet(styleId, cssTxt) {
    if (!document.styleSheets[styleId]) {
        var ss = document.createElement('style');
        ss.setAttribute('type', 'text/css');
        ss.id = styleId;
        if (ss.styleSheet) {
            ss.styleSheet.cssText = cssTxt;
        } else {
            ss.appendChild(document.createTextNode(cssTxt));
        }
        document.getElementsByTagName('head')[0].appendChild(ss);
    }
}

/**
 * [uniqueSort 对数组和字符串去重]
 * @param  {[string or array]} result [传入字符串或者数组]
 * @return {[array]}        [返回一个已经去重并且序列化的数组]
 */
function uniqueSort(result){
    if (!result || result && (typeof result !== 'string' && !(result instanceof Array))) {
        console.info('请传入一个字符串或数组');
        return result;
    }
    var arr;
    if (typeof result === 'string') {
        arr = result.split('');
    } else {
        arr = result;
    }
    var una = [],
            i = 0;
    arr = arr.sort();
    una.push(arr[i]);
    for(var j = 1;j < arr.length;j++){
        if(una[i] !== arr[j]){
            una.push(arr[j]);
            i++;
        }
    }
    return una;
}

