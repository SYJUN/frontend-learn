var eventUtil = {
    /**
     * 跨浏览器绑定事件
     * @param eleObj    元素对象
     * @param type      事件类型
     * @param callback  回调函数
     */
    addEvent:function(eleObj,type,callback){
        if(typeof eleObj.addEventListener != 'undefined'){
            this.addEvent = function(eleObj,type,callback){
                eleObj.addEventListener(type,callback,false);
            };
        }else if(typeof eleObj.attachEvent != 'undefined'){
            this.addEvent = function(eleObj,type,callback){
                eleObj.attachEvent('on' + type,function(){
                    callback.call(eleObj,window.event);
                });
            };
        }
        this.addEvent(eleObj,type,callback);
    },
    /**
     * 跨浏览器删除事件
     * @param eleObj    元素对象
     * @param type      事件类型
     * @param callback  回调函数
     */
    removeEvent:function(eleObj,type,callback){
        if(typeof eleObj.removeEventListener != 'undefined'){
            this.removeEvent = function(eleObj,type,callback){
                eleObj.removeEventListener(type,callback,false);
            };
        }else if(typeof eleObj.detachEvent != 'undefined'){
            this.removeEvent = function(eleObj,type,callback){
                eleObj.detachEvent('on' + type,function(){
                    callback.call(eleObj,window.event);
                });
            };
        }
        this.removeEvent(eleObj,type,callback);
    },
    /**
     * 跨浏览器获取事件对象
     * @param evt
     * @returns {*|Event}
     */
    getEvent:function(evt){
        return evt = evt || window.event;
    },
    /**
     * 跨浏览器获取事件元素
     * @param evt
     * @returns {*|Object}
     */
    getEventElem:function(evt){
        evt = this.getEvent(evt);
        return evt.target || evt.srcElement;
    },
    /**
     * 跨浏览器阻止事件默认行为
     * @param evt
     */
    preDef:function(evt){
        var e = this.getEvent(evt);
        if(typeof e.preventDefault != 'undefined'){
            e.preventDefault();
        }else{
            e.returnValue = false;
        }
    },
    /**
     * 跨浏览器阻止事件冒泡
     * @param evt
     */
    stopBubble:function(evt){
        var e = this.getEvent(evt);
        if(typeof e.stopPropagation != 'undefined'){
            e.stopPropagation();
        }else{
            e.cancelBubble = true;
        }
    }
};