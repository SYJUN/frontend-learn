
//检查class是否存在
function hasClass(element,cName){
    return !!element.className.match(new RegExp('(\\s|^)'+cName+'(\\s|$)') );   //转换成布尔值
}
//添加一个class，如果不存在的话
function addClass(element,cName){
    if(!hasClass(element,cName)){
        element.className += ' '+cName;
    }
}
//删除一个class，如果存在的话
function removeClass(element,cName){
    if (hasClass(element,cName)) {
        element.className = element.className.replace( new RegExp('(\\s|^)'+ cName +'(\\s|$)'),' ' );
    }
}

/*  自定义getElementsByClassName方法 --  兼容低版本IE
 *根据元素className得到元素集合
 *@param   fatherId 父元素的ID ，默认为document
 *@param    tagName 子元素的标签名
 *@param    className 用空格分开的className (类选择器名) 字符串
 * */
function getElementsByClassName(fatherId,tagName,cName){
    var node = fatherId && document.getElementById(fatherId) || document;
    tagName = tagName || "*";
    cName = cName.split(" ");
    var classNameLength = cName.length;
    var len = null;
    var i = null;
    for(i= 0,len=classNameLength;i<len;i++){
        //创建匹配类名的正则
        cName[i] = new RegExp( "(^|\\s)" + cName[i].replace(/\-/g,"\\-") +"(\\s|$)" );
    }
    var elements = node.getElementsByTagName(tagName);
    var result = [];
    var n = 0;
    for(i=0,len = elements.length;i<len;i++){
        var element = elements[i];
        while( cName[n++].test(element.className) ){    //优化循环
            if(n === classNameLength){
                result[result.length] = element;
                break;
            }
        }
        n = 0;
    }
    return result;
}

/**
 * 功能：获取元素计算后的属性值
 *参数：
 *  elemObj:元素对象
 *  attr:属性名
 */
function getStyle(elemObj,attr){
    var style = null;
    if(window.getComputedStyle){
        style = window.getComputedStyle(elemObj,false);
    }else{
        style = elemObj.currentStyle;
    }
    return style[attr];
}

/*
 * 功能：对于IE6-IE9里如果要设置tbody的innerHTML
 * */
function setTbodyInnerHTML(tbody,html){
    var div = document.createElement('div');
    div.innerHTML = '<table>' + html + '</table>';
    var parent = tbody.parentNode;
    parent.removeChild(tbody);
    parent.appendChild(div.firstChild.firstChild);
}


/*
 * 功能：清空元素内容
 *
 * */
function clearData(element){
    for(var i = element.childNodes.length - 1; i >= 0 ;i--) {
        element.removeChild(element.childNodes[i]);
    }
}

/*
* 功能：获取选中的文本
* 参数：text 需要选中文本的元素(input , textarea)
*       start 选取文本的开始位置
*       end 选取文本的结束位置
* */
function getSelectText(text,start,end){
    if(text.setSelectionRange){
        text.setSelectionRange(start,end);
        text.focus();
    }else if(text.createTextRange){
        var range = text.createTextRange();
        range.collapse(true);
        range.moveStart('character',start);
        range.moveEnd('character',end - start); //用最后的位置 - 开始的位置 = 个数
        range.select();
    }
}





















