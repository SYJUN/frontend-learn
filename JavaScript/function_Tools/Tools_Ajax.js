/*
* 功能：跨浏览器的CORS
* PS：检测XHR 是否支持CORS 的最简单方式，就是检查是否存在 withCredentials 属性。再结合检测XDomainRequest 对象是否存在，就可以兼顾所有浏览器了
* */

function createCORSRequest(method,url){
    var xhr = new XMLHttpRequest();
    if("withCredentials" in xhr){
        xhr.open(method,url,true);
    }else if(typeof XDomainRequest != "undefined"){
        xhr = new XDomainRequest();
        xhr.open(method,url);
    }else{
        xhr = null;
    }
    return xhr;
}


/*
* 功能：封装Ajax
* 参数：{
*   method:'get',
*   url:'demo3.php',
*   data:{
*       "name":"Lee",
*       "age":100
*   },
*   success:function(data){
*       alert(data);
*   },
*   async:true
*
* }
* */
function ajax(obj){
    var xhr = createXHR();
    obj.url = obj.url + '?rand=' + Math.random();

    obj.data = params(obj.data);
    if(!obj.method) obj.method = 'get';

    if(obj.method.toLowerCase() === 'get'){
        obj.url += obj.url.indexOf('?') == -1 ? '?' + obj.data : '&'  + obj.data;
    }
    if(obj.async === true){
        xhr.onreadystatechange = function(){
            callback();
        };
    }

    xhr.open(obj.method,obj.url,obj.async);
    if(obj.method.toLowerCase() === 'post'){
        xhr.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
        xhr.send(obj.data);
    }else{
        xhr.send(null);
    }
    if(obj.async === false){
        callback();
    }

    //创建XHR 核心对象
    function createXHR(){
        if(typeof XMLHttpRequest != 'undefined'){
            return new XMLHttpRequest();
        }else if(typeof ActiveXObject != 'undefined'){
            var version = [
                'MSXML2.XMLHttp.6.0',
                'MSXML2.XMLHttp.3.0',
                'MSXML2.XMLHttp'
            ];
            for(var i = 0;i<version.length;i++){
                try{
                    return new ActiveXObject(version[i]);
                }catch(e){
                    //跳过
                }
            }
        }else{
            throw new Error('您的系统或浏览器不支持MSXML对象');
        }
    }
    //名值对转换为字符串
    function params(data){
        var arr = [];
        for(var k in data){
            arr.push( encodeURIComponent(k) + '=' + encodeURIComponent(data[k]));
        }
        return arr.join('&');
    }

    function callback(){
        if(xhr.readyState == 4){
            if( xhr.status == 200){
                obj.success(xhr.responseText);		//回调传递参数
            }else{
                alert('获取数据错误！错误代号：' + xhr.status + ',错误信息：' + xhr.statusText);
            }
        }
    }
}














