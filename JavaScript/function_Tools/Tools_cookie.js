/*
 * 完成形式：
 *   document.cookie = 'user=值；[expires=失效时间;path=路劲访问;domain=域名访问;secure=安全的https限制访问]'
 *   path 路劲限制，可以限制访问cookie的目录
 *   domain 限制域名访问
 *   secure 指定必须通过https来通信访问
 * */
//设置Cookie
function setCookie(name,value,expires,path,domain,secure){
    var cookieName = encodeURIComponent(name) + '=' + encodeURIComponent(value);
    if(expires instanceof Date) cookieName += ';expires=' + expires;
    if(path) cookieName += ';path=' + path;
    if(domain) cookieName += ';domain' + domain;
    if(secure) cookieName += ';secure';
    document.cookie = cookieName;
}

//过期时间
function setCookieDate(day){
    //传递一个天数，比如传递7，就7天后失效
    var date = null;
    if(typeof day == 'number' && day > 0){
        date = new Date();
        date.setDate(date.getDate() + day);
    }else{
        throw new Error('传递的天数不合法！必须是数字且大于0');
    }
    return date;
}
//读取 cookie
function getCookie(name){
    var cookieName = encodeURIComponent(name) + '=';
    var cookieStart = document.cookie.indexOf(cookieName);
    var cookieValue = null;
    if(cookieStart > -1){
        var cookieEnd = document.cookie.indexOf(';',cookieStart);
        if(cookieEnd == -1) cookieEnd = document.cookie.length;
        cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length,cookieEnd));
    }
    return cookieValue;
}

//删除 cookie
function delCookie(name){
    var exp = new Date(new Date().getTime() - 1),
        s = getCookie(name);
    if(s != null){
        document.cookie = name + '=' + s + ';expires=' + exp.toGMTString() + ';path=/';
    }
}