/**
 * Created by Administrator on 2016/8/23.
 */
(function (win) {
    var CommonJS = (function () {
        function CommonJS() {}

        CommonJS.prototype = {
            Tools: {
                //夸浏览器获取字符编码
                getCharCode: function (evt) {
                    var e = evt || window.event;
                    if (typeof e.charCode == 'number') {
                        return e.charCode;
                    } else {
                        return e.keyCode;
                    }
                },
                //动态加载js文件
                loadScript: function (url, callback) {
                    var script = document.createElement("script");
                    script.type = "text/javascript";
                    if (script.readyState) {  //IE
                        script.onreadystatechange = function () {
                            if (script.readyState == "loaded" || script.readyState == "complete") {
                                script.onreadystatechange = null;
                                callback();
                            }
                        };
                    } else { //Other
                        script.onload = function () {
                            callback();
                        };
                    }
                    script.src = url;
                    document.getElementsByTagName("head")[0].appendChild(script);
                },
                /*
                 * 功能：判断是否是 IE 以及IE的版本
                 *
                 * PS: isIE() === false
                 * */
                isIE: function () {
                    var navUA = navigator.userAgent.toLocaleLowerCase();
                    return (navUA.indexOf('msie') != -1) ? parseInt(navUA.split('msie')[1]) : false;
                },
                /*
                 * 功能：继承对象
                 * @param {Object} target 目标对象。
                 * @param {Object} source 源对象。
                 * @param {boolean} deep 是否复制(继承)对象中的对象。
                 *
                 * @returns {Object} 返回继承了source对象属性的新对象。
                 */
                extend: function (target, /*optional*/source, /*optional*/deep) {
                    target = target || {};
                            var sType = typeof source, i = 1, options;
                            if (sType === 'undefined' || sType === 'boolean') {
                                deep = sType === 'boolean' ? source : false;
                                source = target;
                                target = this;
                            }
                            if (typeof source !== 'object' && Object.prototype.toString.call(source) !== '[object Function]')
                                source = {};
                            while (i <= 2) {
                                options = i === 1 ? target : source;
                                if (options != null) {
                                    for (var name in options) {
                                        var src = target[name], copy = options[name];
                                        if (target === copy)
                                            continue;
                                        if (deep && copy && typeof copy === 'object' && !copy.nodeType)
                                            target[name] = this.extend(src ||
                                            (copy.length != null ? [] : {}), copy, deep);
                                        else if (copy !== undefined)
                                            target[name] = copy;
                                    }
                                }
                                i++;
                            }
                            return target;
                }
            },
            Events: {
                /**
                 * 跨浏览器绑定事件
                 * @param eleObj    元素对象
                 * @param type      事件类型
                 * @param callback  回调函数
                 */
                addEvent: function (eleObj, type, callback) {
                    if (typeof eleObj.addEventListener != 'undefined') {
                        this.Events.addEvent = function (eleObj, type, callback) {
                            eleObj.addEventListener(type, callback, false);
                        };
                    } else if (typeof eleObj.attachEvent != 'undefined') {
                        this.Events.addEvent = function (eleObj, type, callback) {
                            eleObj.attachEvent('on' + type, function () {
                                callback.call(eleObj, window.event);
                            });
                        };
                    }
                    this.Events.addEvent(eleObj, type, callback);
                },
                /**
                 * 跨浏览器删除事件
                 * @param eleObj    元素对象
                 * @param type      事件类型
                 * @param callback  回调函数
                 */
                removeEvent: function (eleObj, type, callback) {
                    if (typeof eleObj.removeEventListener != 'undefined') {
                        this.Events.removeEvent = function (eleObj, type, callback) {
                            eleObj.removeEventListener(type, callback, false);
                        };
                    } else if (typeof eleObj.detachEvent != 'undefined') {
                        this.Events.removeEvent = function (eleObj, type, callback) {
                            eleObj.detachEvent('on' + type, function () {
                                callback.call(eleObj, window.event);
                            });
                        };
                    }
                    this.Events.removeEvent(eleObj, type, callback);
                },
                /**
                 * 跨浏览器获取事件对象
                 * @param evt
                 * @returns {*|Event}
                 */
                getEvent: function (evt) {
                    return evt = evt || window.event;
                },
                /**
                 * 跨浏览器获取事件元素
                 * @param evt
                 * @returns {*|Object}
                 */
                getEventElem: function (evt) {
                    evt = this.Events.getEvent(evt);
                    return evt.target || evt.srcElement;
                },
                /**
                 * 跨浏览器阻止事件默认行为
                 * @param evt
                 */
                preDef: function (evt) {
                    var e = this.Events.getEvent(evt);
                    if (typeof e.preventDefault != 'undefined') {
                        e.preventDefault();
                    } else {
                        e.returnValue = false;
                    }
                },
                /**
                 * 跨浏览器阻止事件冒泡
                 * @param evt
                 */
                stopBubble: function (evt) {
                    var e = this.Events.getEvent(evt);
                    if (typeof e.stopPropagation != 'undefined') {
                        e.stopPropagation();
                    } else {
                        e.cancelBubble = true;
                    }
                }
            },
            Doms: {
                //检查class是否存在
                hasClass: function (element, cName) {
                    //转换成布尔值
                    return !!element.className.match(new RegExp('(\\s|^)' + cName + '(\\s|$)'));
                },
                //如果不存在改class，则添加
                addClass: function (element, cName) {
                    if (!this.Doms.hasClass(element, cName)) {
                        element.className += ' ' + cName;
                    }
                },
                //如果存在该class，则删除
                removeClass: function (element, cName) {
                    if (this.Doms.hasClass(element, cName)) {
                        element.className = element.className.replace(new RegExp('(\\s|^)' + cName + '(\\s|$)'), ' ');
                    }
                },
                /*  自定义 getClassName 方法 --  兼容低版本IE
                 *根据元素className得到元素集合
                 *@param   fatherId 父元素的ID ，默认为document
                 *@param    tagName 子元素的标签名
                 *@param    className 用空格分开的className (类选择器名) 字符串
                 * */
                getClassName: function (fatherId, tagName, cName) {
                    var node = fatherId && document.getElementById(fatherId) || document;
                    tagName = tagName || "*";
                    cName = cName.split(" ");
                    var classNameLength = cName.length;
                    var len = null;
                    var i = null;
                    for (i = 0, len = classNameLength; i < len; i++) {
                        //创建匹配类名的正则
                        cName[i] = new RegExp("(^|\\s)" + cName[i].replace(/\-/g, "\\-") + "(\\s|$)");
                    }
                    var elements = node.getElementsByTagName(tagName);
                    var result = [];
                    var n = 0;
                    for (i = 0, len = elements.length; i < len; i++) {
                        var element = elements[i];
                        while (cName[n++].test(element.className)) {    //优化循环
                            if (n === classNameLength) {
                                result[result.length] = element;
                                break;
                            }
                        }
                        n = 0;
                    }
                    return result;
                },
                /**
                 * [computerStyle 获取DOM元素对象的属性值]
                 * @param  {[type]} element [DOM元素对象]
                 * @param  {[type]} attr    [DOM元素属性名]
                 * @return {[type]}         [DOM元素属性值，带有单位]
                 */
                computerStyle: function (element,attr){
                    var style = null;
                    if (window.getComputedStyle) {
                        style = window.getComputedStyle(element);
                    } else {
                        style = element.currentStyle;
                    }
                    return style[attr];
                },
                /*
                 * 功能：对于IE6-IE9里如果要设置tbody的innerHTML
                 * */
                setTbodyInnerHTML: function (tbody, html) {
                    var div = document.createElement('div');
                    div.innerHTML = '<table>' + html + '</table>';
                    var parent = tbody.parentNode;
                    parent.removeChild(tbody);
                    parent.appendChild(div.firstChild.firstChild);
                },
                //清空元素所有子元素
                clearData: function (element) {
                    for (var i = element.childNodes.length - 1; i >= 0; i--) {
                        element.removeChild(element.childNodes[i]);
                    }
                },
                /*
                 * 功能：获取选中的文本
                 * 参数：text 需要选中文本的元素(input , textarea)
                 *       start 选取文本的开始位置
                 *       end 选取文本的结束位置
                 * */
                getSelectText: function (text, start, end) {
                    if (text.setSelectionRange) {
                        text.setSelectionRange(start, end);
                        text.focus();
                    } else if (text.createTextRange) {
                        var range = text.createTextRange();
                        range.collapse(true);
                        range.moveStart('character', start);
                        range.moveEnd('character', end - start); //用最后的位置 - 开始的位置 = 个数
                        range.select();
                    }
                },
                /*
                 * 原生js动态生成css样式表
                 * @param styleId 给生成的样式表定义一个id
                 * @param cssTxt css属性集合
                 * */
                createCssStyleSheet: function (styleId, cssTxt) {
                    if (!document.styleSheets[styleId]) {
                        var ss = document.createElement('style');
                        ss.setAttribute('type', 'text/css');
                        ss.id = styleId;
                        if (ss.styleSheet) {
                            ss.styleSheet.cssText = cssTxt;
                        } else {
                            ss.appendChild(document.createTextNode(cssTxt));
                        }
                        document.getElementsByTagName('head')[0].appendChild(ss);
                    }
                },
                //取窗口滚动条高度
                getScrollTop: function () {
                    var scrollTop = 0;
                    if (document.documentElement && document.documentElement.scrollTop) {
                        scrollTop = document.documentElement.scrollTop;
                    }
                    else if (document.body) {
                        scrollTop = document.body.scrollTop;
                    }
                    return scrollTop;
                },
                //取窗口可视范围的高度
                getClientHeight: function () {
                    var clientHeight = 0;
                    if (document.body.clientHeight && document.documentElement.clientHeight) {
                        clientHeight = (document.body.clientHeight < document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
                    }
                    else {
                        clientHeight = (document.body.clientHeight > document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
                    }
                    return clientHeight;
                },
                //取文档内容实际高度
                getScrollHeight: function () {
                    return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
                },
                /**
                 * [animate description]
                 * @param  {[type]}   obj    [DOM元素对象]
                 * @param  {[type]}   attr   [DOM元素属性名]
                 * @param  {[type]}   target [DOM元素属性进行动画的目标值]
                 * @param  {Function} fn     [链式调用函数]
                 * @return {[type]}          [description]
                 */
                animate: function (obj,attr,target,fn) {
                    clearInterval(obj.timer);
                    obj.timer = setInterval(function(){
                        var cur = 0;
                        if(attr == "opacity"){
                            cur = Math.round(parseFloat(computerStyle(obj,attr))*100);
                        }else{
                            cur = parseInt(computerStyle(obj,attr));
                        }
                        var speed = (target-cur)/8;
                        speed = speed>0?Math.ceil(speed):Math.floor(speed);
                        if(cur == target){
                            clearInterval(obj.timer);
                            if(fn){
                                fn();
                            }
                        }else{
                            if(attr == "opacity"){
                                obj.style.filter = "alpha(opacity="+(cur+speed)+")";
                                obj.style.opacity = (cur+speed)/100;
                            }else{
                                obj.style[attr] = cur + speed + "px";
                            }
                        }
                    },30);
                }
            },
            Cookies: {
                /*
                 * 设置Cookie
                 * 完成形式：
                 *   document.cookie = 'user=值；[expires=失效时间;path=路劲访问;domain=域名访问;secure=安全的https限制访问]'
                 *   path 路劲限制，可以限制访问cookie的目录
                 *   domain 限制域名访问
                 *   secure 指定必须通过https来通信访问
                 * */
                setCookie: function (name, value, expires, path, domain, secure) {
                    var cookieName = encodeURIComponent(name) + '=' + encodeURIComponent(value);
                    if (expires instanceof Date) cookieName += ';expires=' + expires;
                    if (path) cookieName += ';path=' + path;
                    if (domain) cookieName += ';domain' + domain;
                    if (secure) cookieName += ';secure';
                    document.cookie = cookieName;
                },
                //读取 cookie
                getCookie: function (name) {
                    var cookieName = encodeURIComponent(name) + '=';
                    var cookieStart = document.cookie.indexOf(cookieName);
                    var cookieValue = null;
                    if (cookieStart > -1) {
                        var cookieEnd = document.cookie.indexOf(';', cookieStart);
                        if (cookieEnd == -1) cookieEnd = document.cookie.length;
                        cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length, cookieEnd));
                    }
                    return cookieValue;
                },
                //设置 cookie 过期时间
                setCookieDate: function (day) {
                    //传递一个天数，比如传递7，就7天后失效
                    var date = null;
                    if (typeof day == 'number' && day > 0) {
                        date = new Date();
                        date.setDate(date.getDate() + day);
                    } else {
                        throw new Error('传递的天数不合法！必须是数字且大于0');
                    }
                    return date;
                },
                //删除 cookie
                delCookie: function (name) {
                    var exp = new Date(new Date().getTime() - 1),
                        s = this.Cookies.getCookie(name);
                    if (s != null) {
                        document.cookie = name + '=' + s + ';expires=' + exp.toGMTString() + ';path=/';
                    }
                }
            },
            Strings: {
                /*
                 * 功能：字符串格式化
                 * 动态替换
                 * arguments[i]表示当前位置的参数
                 * */
                format: function (str) {
                    for (var i = 0; i < arguments.length; i++) {
                        var reg = new RegExp("\\{" + i + "\\}", "gm");
                        str = str.replace(reg, arguments[i]);
                    }
                    return str;
                },
                /*
                 * 功能：RGB颜色转换为16进制
                 * */
                colorHex: function (colorStr) {
                    var reg = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/,
                        i;
                    if (/^(rgb|RGB)/.test(colorStr)) {
                        var aColor = colorStr.replace(/(?:||rgb|RGB)*/g, "").split(",");
                        var strHex = "#";
                        for (i = 0; i < aColor.length; i++) {
                            var hex = Number(aColor[i]).toString(16);
                            if (hex === "0") {
                                hex += hex;
                            }
                            strHex += hex;
                        }
                        if (strHex.length !== 7) {
                            strHex = colorStr;
                        }
                        return strHex;
                    } else if (reg.test(colorStr)) {
                        var aNum = colorStr.replace(/#/, "").split("");
                        if (aNum.length === 6) {
                            return colorStr;
                        } else if (aNum.length === 3) {
                            var numHex = "#";
                            for (i = 0; i < aNum.length; i += 1) {
                                numHex += (aNum[i] + aNum[i]);
                            }
                            return numHex;
                        }
                    } else {
                        return colorStr;
                    }
                },
                /*
                 * 功能：16进制颜色转为RGB格式
                 * */
                colorRgb: function (colorStr) {
                    var sColor = colorStr.toLowerCase(),
                        reg = /^#([0-9a-fA-F]{3}|[0-9a-fA-F]{6})$/,
                        i;
                    if (sColor && reg.test(sColor)) {
                        if (sColor.length === 4) {
                            var sColorNew = "#";
                            for (i = 1; i < 4; i += 1) {
                                sColorNew += sColor.slice(i, i + 1).concat(sColor.slice(i, i + 1));
                            }
                            sColor = sColorNew;
                        }
                        //处理六位的颜色值
                        var sColorChange = [];
                        for (i = 1; i < 7; i += 2) {
                            sColorChange.push(parseInt("0x" + sColor.slice(i, i + 2)));
                        }
                        return "RGB(" + sColorChange.join(",") + ")";
                    } else {
                        return sColor;
                    }
                },
                addQueryStringArg: function (url, name, value) {
                    if (url.indexOf("?") == -1) {
                        url += "?";
                    } else {
                        url += "&";
                    }
                    url += encodeURIComponent(name) + "=" + encodeURIComponent(value);
                    return url;
                }
            }
        };
        return CommonJS;
    })();

    win['CommonJS'] = new CommonJS();
})(window);

