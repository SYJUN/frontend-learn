var Class = function (parent) {
    var kclass = function () {
        this.init.apply(this, arguments);
    };
    if (parent) {
        var Subclass = function () {
        };
        Subclass.prototype = parent.prototype;
        kclass.prototype = new Subclass;
    }
    kclass.prototype.init = function () {
    };
    //定义 prototype 别名
    kclass.fn = kclass.prototype;
    //添加一个proxy函数
    kclass.proxy = function (fun) {
        var _t = this;
        return (function () {
            return fun.apply(_t, arguments);
        });
    };

    //定义类的别名
    kclass.fn.parent = kclass;
    kclass.fn.proxy = kclass.proxy;
    kclass._super = kclass.__proto__;
    //给类添加属性--私有
    kclass.extend = function (obj) {
        var extended = obj.extended;
        for (var k in obj) {
            kclass[k] = obj[k];
        }
        if (extended) extended(kclass);
    };
    //给实例添加属性--原型
    kclass.include = function (obj) {
        var included = obj.included;
        for (var k in obj) {
            kclass.fn[k] = obj[k];
        }
        if (included) included(kclass);
    };
    return kclass;
};
