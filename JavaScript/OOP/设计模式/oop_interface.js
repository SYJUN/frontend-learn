(function () {
    var Interface = (function () {
        /**
         *  创建接口对象
         * @param name      String 接口的名字
         * @param method    Array 接口里面定义的方法
         * @constructor     Interface
         */
        function Interface(name,methods){
            //如果构造函数的参数不等于2个，则抛出错误
            if (arguments.length != 2) {
                throw new Error("the interface length is bigger than 2");
            }
            this.name = name;
            this.method = [];
            //方法数组，保证传进来的methods数组中，每一个元素都是字符串类型
            for (var i = 0, len = methods.length; i < len; i++) {
                if (typeof methods[i] !== 'string') {
                    throw new Error("接口构造函数中的方法类型错误，字符串类型！");
                }
                this.method.push(methods[i]);
            }
        }
        /*给Interface扩展静态验证方法*/
        Interface.ensureImplements = function (obj) {
            //如果参数个数小于2，则抛出异常，obj是待判断实现接口的对象,第一个参数
            if (arguments.length < 2) {
                throw new Error("there is not Interface or the instance");
            }
            for (var i = 1, len = arguments.length; i < len; i++) {
                //inter_face为接口，一定要实现Interface类
                var inter_face = arguments[i];
                if (inter_face.constructor !== Interface) {
                    throw new Error("the argument is not interface");
                }
                for (var j = 0, methodsLen = inter_face.method.length; j < methodsLen; j++) {
                    //对象中是否含有接口中定义的方法
                    var method = inter_face.method[j];
                    if (!obj[method] || typeof obj[method] !== 'function') {
                        throw new Error("you instance doesn't implement the interface");
                    }
                }
            }
        };
        return Interface;
    })();
    function extend(target,source){
        function F(){}
        F.prototype = source.prototype;
        target.prototype = new F();
        target.prototype.constructor = target;

        target.source = source.prototype;
        if(source.prototype.constructor == Object.prototype.constructor){
            source.prototype.constructor = source;
        }
    }
    function clone(obj){
        function F(){}
        F.prototype = obj;
        return new F();
    }
    function addEvent(eleObj,type,callback){
        if(typeof eleObj.addEventListener != 'undefined'){
            this.addEvent = function(eleObj,type,callback){
                eleObj.addEventListener(type,callback,false);
            };
        }else if(typeof eleObj.attachEvent != 'undefined'){
            this.addEvent = function(eleObj,type,callback){
                eleObj.attachEvent('on' + type,function(){
                    callback.call(eleObj,window.event);
                });
            };
        }
        this.addEvent(eleObj,type,callback);
    }
    function setCookie(name,value,expires,path,domain,secure){
        var cookieName = encodeURIComponent(name) + '=' + encodeURIComponent(value);
        if(expires instanceof Date) cookieName += ';expires=' + expires;
        if(path) cookieName += ';path=' + path;
        if(domain) cookieName += ';domain' + domain;
        if(secure) cookieName += ';secure';
        document.cookie = cookieName;
    }
    function getCookie(name){
        var cookieName = encodeURIComponent(name) + '=';
        var cookieStart = document.cookie.indexOf(cookieName);
        var cookieValue = null;
        if(cookieStart > -1){
            var cookieEnd = document.cookie.indexOf(';',cookieStart);
            if(cookieEnd == -1) cookieEnd = document.cookie.length;
            cookieValue = decodeURIComponent(document.cookie.substring(cookieStart + cookieName.length,cookieEnd));
        }
        return cookieValue;
    }
    var common = {
        'Interface':Interface,
        'extend':extend,
        'clone':clone,
        'addEvent':addEvent,
        'setCookie':setCookie,
        'getCookie':getCookie
    };
    window['common'] = common;
})();














