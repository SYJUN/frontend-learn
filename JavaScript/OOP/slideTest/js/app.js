var arrImgs = [
    {"imgId":0,"imgUrl":"images/1.jpg","imgTitle":"图片1","imgLinkUrl":"#"},
    {"imgId":1,"imgUrl":"images/2.jpg","imgTitle":"图片2","imgLinkUrl":"#"},
    {"imgId":2,"imgUrl":"images/3.jpg","imgTitle":"图片3","imgLinkUrl":"#"},
    {"imgId":3,"imgUrl":"images/4.jpg","imgTitle":"图片4","imgLinkUrl":"#"},
    {"imgId":4,"imgUrl":"images/5.jpg","imgTitle":"图片5","imgLinkUrl":"#"},
    {"imgId":5,"imgUrl":"images/6.jpg","imgTitle":"图片6","imgLinkUrl":"#"}
];
//数据初始化
function resetdata(element){
    var init = {
        duration:500,   //每次滚动的总时长
        interval:20,    //滚动动画每一步的时间间隔
        wait:3000       //自动轮播时，每个广告轮播的时间间隔
    };
    return {
        width:parseInt(computerStyle(element).width),
        imgList:getElementsByClassName(element,"ul","imgList")[0],
        circleList:getElementsByClassName(element,"ul","circleList")[0],
        btn_prev:getElementsByClassName(element,"span","btn-prev")[0],
        btn_next:getElementsByClassName(element,"span","btn-next")[0],

        duration:init.duration,
        interval:init.interval,
        wait:init.wait
    }
}
window.onload = function(){
    var banner = document.getElementById("banner");
    var slider = new Slider( resetdata(banner) );
    slider.init(banner);
};






























