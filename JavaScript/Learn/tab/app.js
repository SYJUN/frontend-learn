var tabMenus = document.getElementById('tab-menuWrapper').getElementsByTagName('li'),
    tabContents = document.getElementById('tab-contentWrapper').getElementsByTagName('div');

for(var i = 0;i < tabMenus.length;i++){
    (function(_i){
        tabMenus[_i].onclick = function(){
            for(var j = 0;j < tabContents.length;j++){
                tabContents[j].style.display = 'none';
            }
            tabContents[_i].style.display = 'block';
            var currentMenu = getElementsByClassName('tab-menuWrapper','li','tab-currentMenu')[0];
            if(currentMenu){
                currentMenu.className = '';
            }
            this.className = 'tab-currentMenu';
        }
    })(i);
}



function getElementsByClassName(fatherId,tagName,cName){
    var node = fatherId && document.getElementById(fatherId) || document;
    tagName = tagName || "*";
    cName = cName.split(" ");
    var classNameLength = cName.length,
        len = null,
        i = null;
    for(i= 0,len=classNameLength;i<len;i++){
        //创建匹配类名的正则
        cName[i] = new RegExp( "(^|\\s)" + cName[i].replace(/\-/g,"\\-") +"(\\s|$)" );
    }
    var elements = node.getElementsByTagName(tagName),
        result = [],
        n = 0;
    for(i=0,len = elements.length;i<len;i++){
        var element = elements[i];
        while( cName[n++].test(element.className) ){    //优化循环
            if(n === classNameLength){
                result[result.length] = element;
                break;
            }
        }
        n = 0;
    }
    return result;
}


























