//定义电话本1
var phonebook = [
    {name:"adang",tel:"111111"},
    {name:"zhangxia",tel:"222222"},
    {name:"yangfuchuan",tel:"333333"},
    {name:"qiaojiwu",tel:"444444"},
    {name:"zhouyubo",tel:"555555"}
];

//定义电话本2
var phonebook2 = [
    {name:"niaoren",tel:"111111"},
    {name:"j2ee",tel:"222222"},
    {name:"baobao",tel:"333333"}
];

//查询电话
function getTel(oPhonebook,oName){
    var tel = '';
    for(var i = 0;i<oPhonebook.length;i++){
        if(oPhonebook[i].name == oName){
            tel = oPhonebook[i].tel;
            break;
        }
    }
    return tel;
}

//添加记录
function addItem(oPhonebook,oName,oTel){
    oPhonebook.push({name:oName,tel:oTel});
}

//删除记录
function removeItem(oPhonebook,oName){
    var n;
    for(var i = 0;i < oPhonebook.length;i++){
        if(oPhonebook[i].name == oName){
            n = i;
            break;
        }
    }
    if(n != undefined){
        oPhonebook.splice(n,1);
    }
}




