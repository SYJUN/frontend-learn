$(document).ready(function () {
    new Tab_content("clh1", "cg_ul1", "li_div1");
    new Tab_content("clh2", "cg_ul2", "li_div2");
});

function Tab_content(boxId, tabNavId, ContentId) {
    var _t = this;
    this.box = $('#' + boxId);
    this.$tabUl = $('#' + tabNavId);
    this.tabLi = this.$tabUl.find('li');
    this.tabDiv = this.box.find('.sj_div');
    this.contentDiv = $('#' + ContentId);
    this.leftBtn = this.box.find('.lft');
    this.rightBtn = this.box.find('.rht');

    this.len = this.tabLi.length;
    this.state = true;
    this.offsetL = parseFloat(this.tabDiv.css('left'));
    this.nli = 0;
    //移动的距离
    this.moveDistance = this.tabLi.eq(0).width() + parseFloat(this.tabLi.eq(0).css('marginLeft'));
    this.tabLi.on('click', 'a', function () {
        _t.nli = $(this).parent('li').index();
        var n = _t.nli-_t.count;
        _t.tab_div(n);
        _t.state = false;
    });
    this.count = 0;
    if (this.len <= 3) {
        this.leftBtn.addClass('lft0').removeClass('lft');
        this.rightBtn.addClass('rht0').removeClass('rht');
    } else {
        if (this.count == 0) {
            this.leftBtn.addClass('lft0').removeClass('lft');
            this.rightBtn.removeClass('rht0').addClass('rht');
        } else if (this.count == this.len - 1) {
            this.leftBtn.removeClass('lft0').addClass('lft');
            this.rightBtn.addClass('rht0').removeClass('rht');
        }
    }
    this.leftBtn.click(function (e) {
        e.preventDefault();
        if (!$(this).hasClass('lft0') && _t.state) {
            _t.rightBtn.removeClass('rht0').addClass('rht');
            _t.clickBtn('lft');
            _t.state = false;
        }
    });
    this.rightBtn.click(function (e) {
        e.preventDefault();
        if (!$(this).hasClass('rht0') && _t.state) {
            _t.leftBtn.removeClass('lft0').addClass('lft');
            _t.clickBtn('rht');
            _t.state = false;
        }
    });
}
Tab_content.prototype = {
    tab_div: function (n) {
        var _t = this;
        if (this.state) {
            //console.log('count:'+ this.count);
            //console.log('nli:'+this.nli);
            this.content_div();
            this.tabDiv.animate({
                left: _t.offsetL + _t.moveDistance * n + 'px'
            }, 500, function () {
                _t.state = true;
            });
        }
    },
    content_div: function () {
        this.tabLi.eq(this.nli).find('a').addClass('active').parent('li').siblings().find('a').removeClass('active');
        this.contentDiv.find('.li_cont').eq(this.nli).addClass('blk').siblings().removeClass('blk');
    },
    clickBtn: function (btn) {
        var _t = this;
        if (btn == 'rht') {
            this.count++;
            this.$tabUl.animate({
                left: -(_t.moveDistance * _t.count) + 'px'
            }, 500,function(){
                _t.state = true;
                _t.nli++;
                _t.content_div();
            });
            if (this.count >= this.len - 3) {
                this.rightBtn.addClass('rht0').removeClass('rht');
            }
        }else if(btn == 'lft'){
            this.count--;
            this.$tabUl.animate({
                left: -(_t.moveDistance * _t.count) + 'px'
            }, 500,function(){
                _t.state = true;
                _t.nli--;
                _t.content_div();
            });
            if (this.count <= 0) {
                this.leftBtn.addClass('lft0').removeClass('lft');
            }
        }
    }
};
