/*
 *
 *
 * */

;
(function () {
    var intStartTime;
    var objIMG = new Image();

    objIMG.onload =
        objIMG.onerror =
            function () {
                /*
                 * 有回应,取消超时计时
                 */
                clearTimeout(intTimerID);

                if (!bolIsRunning || bolIsTimeout)
                    return;

                var delay = new Date() - intStartTime;

                println("Reply from " + strURL + " time" + ( (delay < 1) ? ("<1") : ( "=" + delay ) ) + "ms");
                arrDelays.push(delay);

                /*
                 * 每次请求间隔限制在1秒以上
                 */
                setTimeout(ping, delay < 1000 ? (1000 - delay) : 1000);
            };

    function ping() {
        /*
         * 发送请求
         */
        intStartTime = +new Date();
        intSent++;

        objIMG.src = strURL + "/" + intStartTime;
        bolIsTimeout = false;

        /*
         * 超时计时
         */
        intTimerID = setTimeout(timeout, intTimeout);
    }

    function timeout() {
        if (!bolIsRunning)
            return;

        bolIsTimeout = true;
        objIMG.src = "X:\\";

        println("Request timed out.");
        ping();
    }

    var $ = function (v) {
        return document.getElementById(v)
    };

    var arrDelays = [];
    var intSent;

    var bolIsRunning = false;
    var bolIsTimeout;
    var strURL;
    var intTimeout;
    var intTimerID;

    var objBtn = $("btnSwitch");
    var objContent = $("divContent");
    var objTxtURL = $("txtURL");

    objTxtURL.value = window.location.host;

    function handleBtnClick() {
        if (bolIsRunning) {
            /*
             * 停止
             */
            var intRecv = arrDelays.length;
            var intLost = intSent - intRecv;
            var sum = 0;

            for (var i = 0; i < intRecv; i++)
                sum += arrDelays[i];

            objBtn.value = "Start";
            bolIsRunning = false;

            /*
             * 统计结果
             */
            println("　");
            println("Ping statistics for " + strURL + ":");
            println("　　Packets: Sent = " +
            intSent +
            ", Received = " +
            intRecv +
            ", Lost = " +
            intLost +
            " (" +
            Math.floor(intLost / intSent * 100) +
            "% loss),");

            if (intRecv == 0)
                return;

            println("Approximate round trip times in milli-seconds:");
            println("　　Minimum = " +
            Math.min.apply(this, arrDelays) +
            "ms, Maximum = " +
            Math.max.apply(this, arrDelays) +
            "ms, Average = " +
            Math.floor(sum / intRecv) +
            "ms");
        }
        else {
            /*
             * 开始
             */
            strURL = objTxtURL.value;

            if (strURL.length == 0)
                return;

            if (strURL.substring(0, 7).toLowerCase() != "http://")
                strURL = "http://" + strURL;

            intTimeout = parseInt($("txtTimeout").value, 10);
            if (isNaN(intTimeout))
                intTimeout = 2000;
            if (intTimeout < 1000)
                intTimeout = 1000;

            objBtn.value = "Stop ";
            bolIsRunning = true;

            arrDelays = [];
            intSent = 0;

            cls();
            println("Pinging " + strURL + ":");
            println("　");
            ping();
        }
    }

    function println(str) {
        var objDIV = document.createElement("div");

        if (objDIV.innerText != null)
            objDIV.innerText = str;
        else
            objDIV.textContent = str;

        objContent.appendChild(objDIV);
        objContent.scrollTop = objContent.scrollHeight;
    }

    function cls() {
        objContent.innerHTML = "";
    }

    window['handleBtnClick'] = handleBtnClick;
})();




