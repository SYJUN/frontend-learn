function $(id){
    return typeof id === 'string' ? document.getElementById(id) : id;
}
window.onload = function(){
    //当前高亮小时的页签的索引
    var notice = $('notice'),
        titles = $('notice-title').getElementsByTagName('li'),
        divs = $('notice-con').getElementsByTagName('div');
    var len = titles.length;
    if(titles.length != divs.length) return;
    var index = 0,
        timer = null;
    for(var i = 0;i < len;i++){
        titles[i].id = i;
        titles[i].onmouseover = function(){
            index = this.id;
            clearInterval(timer);
            changeOption(this.id);
        };
        titles[i].onmouseout = function(){
            timer = window.setInterval(autoPlay,2000);
        }
    }
    if(timer){
        clearInterval(timer);
        timer = null;
    }
    timer = window.setInterval(autoPlay,2000);
    function autoPlay(){
        index++;
        if(index >= titles.length){
            index = 0;
        }
        changeOption(index);
    }
    function changeOption(curIndex){
        var len = titles.length;
        for(var i = 0;i<len;i++){
            titles[i].className = "";
            divs[i].style.display = 'none';
        }
        titles[curIndex].className = 'select';
        divs[curIndex].style.display = 'block';
    }
};

//延迟切换
function tab(){
    var timer = null;
    var titles = $('notice-title').getElementsByTagName('li'),
        divs = $('notice-con').getElementsByTagName('div');
    //遍历所有的页签
    var len = titles.length;
    for(var i = 0;i < len;i++){
        titles[i].id = i;
        titles[i].onmouseover = function(){
            var _t = this;
            //如果存在准备执行的定时器，立刻清除，只有当前停留时间大于500ms时才执行
            if(timer){
                clearTimeout(timer);
                timer = null;
            }
            //延迟半秒执行
            timer = window.setTimeout(function(){
                for(var j = 0; j < len;j++){
                    titles[j].className = '';
                    divs[j].style.display = 'none';
                }
                _t.className = 'select';
                divs[_t.id].style.display = 'block';
            },500);
        };
    }
}

