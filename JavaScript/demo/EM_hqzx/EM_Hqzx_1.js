﻿(function () {      //EM_Hqzx_1

    $ = function (s) {
        return (typeof (s) == "string") ? document.getElementById(s) : s;
    }
    if (typeof $ == "undefined") { $ = function (a) { return document.getElementById(a) } }
    if (typeof $C == "undefined") { $C = function (a) { return document.createElement(a) } }
    Function.prototype.Bind = function () {
        var d = this,
            b = arguments[0],
            a = new Array();
        for (var c = 1; c < arguments.length; c++) {
            a.push(arguments[c]);
        }
        return function () {
            return d.apply(b, a);
        }
    };

    window.amineWidth = function (obj, num) {
        obj.style.width = num + "px";
    };
    var tabsItemId,
        tabsState = false;
    var MutualTab = function () {
        this.init.apply(this, arguments);
    };
    MutualTab.prototype = {
        init: function (tabs, targets, type, callbackObjArr, subTargets) {
            this.tabs = [];

            var tempTabs = tabs.childNodes;
            for (var i = 0; i < tempTabs.length; i++) {
                if (tempTabs[i].nodeType == 1 && tempTabs[i].nodeName.toLowerCase() != 'dd') {
                    this.tabs.push(tempTabs[i]);
                }
            }
            if (targets) {
                this.targets = [];
                var tempTargets = targets.childNodes;
                for (var j = 0; j < tempTargets.length; j++) {
                    if (tempTargets[j].nodeType == 1) {
                        this.targets.push(tempTargets[j]);
                    }
                }
                this.activeTab = this.tabs[0];
                this.activeTarget = this.targets[0];
            }
            if (!targets && !subTargets) this.activeTab = this.tabs[0];
            if (subTargets) this.activeTab = null;//this.tabs[0];
            if (type != "mouseover") {
                for (var k = 0; k < this.tabs.length; k++) {
                    this.tabs[k].onclick = this.showTab.Bind(this, k, callbackObjArr ? callbackObjArr[k] : null);
                }
            } else {
                tabsItemId = 0;
                for (var k = 0; k < this.tabs.length; k++) {
                    this.tabs[k].onmouseout = this._hide.Bind(this, k);
                    this.tabs[k].onmouseover = this._over.Bind(this, k, callbackObjArr ? callbackObjArr[k] : null);
                }
            }
            
        },
        showTab: function (_index, callback) {
            this.tabIndex = _index;
            if (this.tabs[_index] == this.activeTab) return;

            if (this.activeTab) this.activeTab.className = "";
            
            this.activeTab = this.tabs[_index];
            this.activeTab.className = "at";
            
            if (this.targets) {
                if (this.activeTarget) {
                    this.activeTarget.style.display = "none";
                }
                this.activeTarget = this.targets[_index];
                this.activeTarget.style.display = "";
                
            }
            if (callback) {
                this.activeCallback = callback;
                callback();
            }

        },
        _over: function (obj, _index, callback) {
            tabsState = true;
            this._timeout = window.setTimeout(this.showTab.Bind(this, obj, _index, callback), 150);
        },
        _hide: function () {
            if (this._timeout) {
                window.clearTimeout(this._timeout);
            }
        }
    };
    //window.MutualTab = MutualTab;
    var hqData = {
        CSI: {//沪深
            id: "CSI",
            set: [
                ['0000011', '沪A', 'http://hqpiczs.dfcfw.com/em_quote2010pictureproducter/picture/0000011rsindex.png', 'http://quote.eastmoney.com/zs000001.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#10">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#10?sortType=H&amp;sortRule=-1">量比</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#10?sortType=J&amp;sortRule=-1">换手</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#10?sortType=I&amp;sortRule=-1">市盈</a></li>'],
                ['3990012', '深A', 'http://hqpiczs.dfcfw.com/em_quote2010pictureproducter/picture/3990012rsindex.png', 'http://quote.eastmoney.com/zs399001.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#20">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#20?sortType=H&amp;sortRule=-1">量比</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#20?sortType=J&amp;sortRule=-1">换手</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#20?sortType=I&amp;sortRule=-1">市盈</a></li>'],
                ['3990062', '创业板', 'http://hqpiczs.dfcfw.com/em_quote2010pictureproducter/picture/3990062rsindex.png', 'http://quote.eastmoney.com/zs399006.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#27">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#27?sortType=H&amp;sortRule=-1">量比</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#27?sortType=J&amp;sortRule=-1">换手</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#27?sortType=I&amp;sortRule=-1">市盈</a></li>'],
                ['0003001', '300', 'http://hqpiczs.dfcfw.com/em_quote2010pictureproducter/picture/0003001rsindex.png', 'http://quote.eastmoney.com/zs000300.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28003500_0_2">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=H&amp;sortRule=-1">量比</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=J&amp;sortRule=-1">换手</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=I&amp;sortRule=-1">市盈</a></li>'],
                ['3000592', '东方财富', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/3000592RSINDEX.png', 'http://quote.eastmoney.com/sz300059.html', '<li><a target="_blank" href="http://f10.eastmoney.com/f10_v2/OperationsRequired.aspx?code=sz300059">F10</a></li><li><a target="_blank" href="http://data.eastmoney.com/notice/300059.html">公告</a></li><li><a target="_blank" href="http://data.eastmoney.com/report/300059.html">研报</a></li><li><a target="_blank" href="http://guba.eastmoney.com/list,300059.html">股吧</a></li>']
            ]
        },
        HK: {//香港
            id: "HK",
            set: [
                ['1100005', '恒生', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RSIndex&ID=1100005', 'http://quote.eastmoney.com/hk/zs110000.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#50_1">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#50_1?sortType=F&sortRule=-1">成交</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
                ['1100305', '红筹', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RSIndex&ID=1100305', 'http://quote.eastmoney.com/hk/zs110030.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28HSCCI_1">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28HSCCI_1?sortType=F&sortRule=-1">成交</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
                ['1100105', '国企', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RSIndex&ID=1100105', 'http://quote.eastmoney.com/hk/zs110010.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28HSCEI_1">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28HSCEI_1?sortType=F&sortRule=-1">成交</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
                ['1100505', '创业板', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RSIndex&ID=1100505', 'http://quote.eastmoney.com/hk/zs110050.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28GEM_1">涨幅</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28GEM_1?sortType=F&sortRule=-1">成交</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>']
            ]
        },
        OCCIDENT: {//欧美
            id: "OCCIDENT",
            set: [
                ['INDU7', '道琼斯', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/INDU7RSINDEX.png', 'http://quote.eastmoney.com/gb/zsindu.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>'],
                ['CCMP7', '纳斯达克', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/CCMP7RSINDEX.png', 'http://quote.eastmoney.com/gb/zsCCMP.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>'],
                ['UKX7', '英国FT', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/UKX7RSINDEX.png', 'http://quote.eastmoney.com/gb/zsUKX.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>']
            ]
        },
        FUND: {//基金
            id: "FUND",
            set: [
                ['0000111', '沪基', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/0000111rsindex.png', 'http://quote.eastmoney.com/zs000011.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#285002_4">封基</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/fundlist.html#3,_4">开基</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#2850013_4">ETF</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#2850014_4">LOF</a></li>'],
                ['3993052', '深基', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/3993052rsindex.png', 'http://quote.eastmoney.com/zs399305.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#285002_4">封基</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/fundlist.html#3,_4">开基</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#2850013_4">ETF</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#2850014_4">LOF</a></li>']
            ]
        },
        FUTURES: {//期货
            id: "FUTURES",
            set: [
                ['IFDYLX1', '股指期货', 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/IFDYLX1RSIndex.png', 'http://quote.eastmoney.com/gzqh/IFdylx.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
                ['TFGZDJ1', '国债期货', 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSIndex&ID=TFGZDJ1', 'http://quote.eastmoney.com/gzqh/TFGZDJ.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
                ['CONC0', '美原油', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/conc0RSIndex.png', 'http://quote.eastmoney.com/qihuo/CONC.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
                ['GLNC0', '美黄金', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/glnc0RSIndex.png', 'http://quote.eastmoney.com/qihuo/GLNC.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a target="_blank" href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>']
            ]
        },
        FOREX: {//外汇
            id: "FOREX",
            set: [
                ['DINI0', '美元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/dini0RSIndex.png', 'http://quote.eastmoney.com/qihuo/DINI.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a target="_blank" href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a target="_blank" href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a target="_blank" href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
                ['EURUSD0', '欧元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSindex&id=EURUSD0', 'http://quote.eastmoney.com/forex/EURUSD.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a target="_blank" href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a target="_blank" href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a target="_blank" href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
                ['GBPUSD0', '英镑', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSindex&id=GBPUSD0', 'http://quote.eastmoney.com/forex/GBPUSD.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a target="_blank" href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a target="_blank" href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a target="_blank" href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
                ['USDJPY0', '日元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSindex&id=USDJPY0', 'http://quote.eastmoney.com/forex/USDJPY.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a target="_blank" href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a target="_blank" href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a target="_blank" href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
                ['USDCNY0', '人民币', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSindex&id=USDCNY0', 'http://quote.eastmoney.com/forex/USDCNY.html', '<li><a target="_blank" href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a target="_blank" href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a target="_blank" href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a target="_blank" href="http://forex.eastmoney.com/FC.html">日历</a></li>']
            ]
        }
    };

    var StockIndex = {
        tabObjs: [],

        tabsItemId:0,           //二级标签页导航索引值
        tabsIndex: 0,           //一级标签页导航索引值
        tabshqDataId: "",       //hqData数据中对应的属性id

        imgFqc: 120000,
        activeImg: 0,
        apply: function (tabId, contentId, refBtn) {
            if (window["_IFCode"]) {
                hqData["FUTURES"].set[0][0] = window["_IFCode"];
                hqData["FUTURES"].set[0][2] = 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/' + _IFCode + '1RSIndex.png';
                hqData["FUTURES"].set[0][3] = 'http://quote.eastmoney.com/gzqh/' + _IFCode + '.html';
            }
            if (window["_TFCode"]) {
                hqData["FUTURES"].set[1][0] = window["_TFCode"];
                hqData["FUTURES"].set[1][2] = 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RSIndex&ID=' + _TFCode + '1';
                hqData["FUTURES"].set[1][3] = 'http://quote.eastmoney.com/gzqh/' + _TFCode + '.html';
            }
            this._tabsElement = $(tabId);
            this._contentElement = $(contentId);
            if (refBtn) this._refreshElement = $(refBtn);
            this._initContent();
            this._tabCtrl = new MutualTab(this._tabsElement, this._contentElement, "mouseover", this.tabObjs, true);
            this.tabsSelect();
            this._start(this.tabsIndex,this.tabshqDataId);
        },
        _initContent: function () {//初始化主体结构
            var tabs = this._tabsElement;
            var tabContent = this._contentElement;
            var tempTabs = tabs.childNodes;
            var setArr = null, showStyle = '', innerContent = '', _active = '';
            for (var i = 0; i < tempTabs.length; i++) {
                if (tempTabs[i].nodeType == 1) {
                    this.tabObjs.push(this.show.Bind(this, hqData[tempTabs[i].id]));
                    innerContent += '<div id="hq_' + tempTabs[i].id + '"' + showStyle + '>';
                    if (showStyle == '') showStyle = ' style="display:none;"';

                    setArr = hqData[tempTabs[i].id].set;
                    innerContent += '<dl class="sc-cont-tabs">';
                    for (var j in setArr) {
                        _active = '';
                        if (j == 0) _active = ' class="at"';
                        innerContent += '<dt' + _active + '><span><a target="_blank" href="' + setArr[j][3] + '">' + setArr[j][1] + '</a></span></dt>';
                        if (j < setArr.length - 1) innerContent += '<dd>|</dd>';
                    }

                    innerContent += '</dl>';
                    innerContent += '<div class="sc-cont-img"></div>';
                    innerContent += '<ul class="sc-cont-imglink"></ul>';
                    innerContent += '</div>';
                }
            }
            tabContent.innerHTML = innerContent;
        },
        tabsSelect:function(){
            var input = $("hqzx_tabId"),        //  获取页面中隐藏标签的元素
                tabs = this._tabsElement.childNodes,
                tabsArr = [],
                tabsDicArr = [],
                i,
                tabId = input.getAttribute("data-hqzx-id1"),
                tabItemIdName = input.getAttribute("data-hqzx-id2");
            var tabsDictionary = [          //标签页各级导航对应字典
                {   //沪深CSI
                    id:"hqzx_CSI",
                    itemNavId:["CSI_hA", "CSI_sA", "CSI_300", "CSI_cyb", "CSI_em"]
                },                            
                {   //香港HK
                    id: "hqzx_HK",
                    itemNavId: ["HK_hs", "HK_hc", "HK_gj", "HK_cyb"]
                },                                            
                {   //欧美OCCIDENT
                    id: "hqzx_OCCIDENT",
                    itemNavId: ["OCCIDENT_dqs", "OCCIDENT_nsdk", "OCCIDENT_UAFT"]
                },                         
                {   //基金FUND
                    id:"hqzx_FUND",
                    itemNavId: ["FUND_hj", "FUND_sj"]
                },                                                        
                {   //期货FUTURES
                    id:"hqzx_FUTURES",
                    itemNavId: ["FUTURES_guzqh", "FUTURES_guozqh", "FUTURES_myy", "FUTURES_mhj"]
                },           
                {   //外汇FOREX
                    id: "hqzx_FOREX",
                    itemNavId: ["FOREX_dollar", "FOREX_Euro", "FOREX_pound", "FOREX_yen", "FOREX_RMB"]
                }        
            ];
            if (!tabId) {
                this.tabsIndex = 0;
                this.tabsItemId = 0;
                this.tabshqDataId = hqData.CSI;
            } else {
                for (i = 0; i < tabsDictionary.length; i++) {
                    if (tabId == tabsDictionary[i].id) {
                        this.tabsIndex = i;
                        this.tabshqDataId = hqData[tabsDictionary[i].id.slice(5)];
                        tabsDicArr = tabsDictionary[i].itemNavId;
                        break;
                    } else {
                        this.tabsIndex = 0;
                        this.tabsItemId = 0;
                        this.tabshqDataId = hqData.CSI;
                    }
                }
            }

            if (!tabItemIdName) {
                this.tabsItemId = 0;
            } else if (tabsDicArr) {
                for (i = 0; i < tabsDicArr.length; i++) {
                    if (tabItemIdName == tabsDicArr[i]) {
                        this.tabsItemId = i;
                        break;
                    } else {
                        this.tabsItemId = 0;
                    }
                }
            }
            
            for (i = 0; i < tabs.length; i++) {
                if (tabs[i].nodeType == 1) {
                    tabsArr.push(hqData[tabs[i].id]);
                }
            }
            var tabsItemNav = $("hq_" + tabsArr[this.tabsIndex].id).getElementsByTagName("dt"),
                len = tabsItemNav.length;
            for (i = 0; i < len; i++) {
                tabsItemNav[i].className = "";
            }
            tabsItemNav[this.tabsItemId].className = "at";

        },
        _start: function (i, hqDataId) {
            this._tabCtrl.showTab(i, this.show.Bind(this, hqDataId));
        },
        show: function (aData) {
            this._sMarket = aData.id;
            this._oSetNow = aData.set;
            var _imgCallback = [];
            var _hqcodeNow = [];

            for (var i = 0; i < this._oSetNow.length; i++) {
                _imgCallback.push(this._loadImg.Bind(this, i));
                _hqcodeNow.push(this._oSetNow[i][0]);
            }
            if (!hqData[this._sMarket]._containersNow) {
                hqData[this._sMarket]._containersNow = [];
                var _tmpchilds = $("hq_" + aData.id).firstChild.childNodes;
                var _tmpFirst = null;
                for (var i = 0; i < _tmpchilds.length; i++) {
                    if (_tmpchilds[i].nodeType == 1) {
                        _tmpFirst = _tmpchilds[i].firstChild;
                        hqData[this._sMarket]._containersNow.push(_tmpFirst.nodeType == 1 ? _tmpFirst : _tmpFirst.nextSibling);
                    }
                }
            }
            if (!aData.hasscroll) {
                aData.hasscroll = new MutualTab($("hq_" + aData.id).firstChild, null, "mouseover", _imgCallback, true);
                //console.log(aData.hasscroll.getIndex());
                if (tabsState) {
                    this.tabsItemId = tabsItemId;
                }
                aData.hasscroll.showTab(this.tabsItemId, this._loadImg.Bind(this));
            }
        },
        _loadImg: function (sID) {//加载并开启自刷新图片
            sID = !sID ? 0 : sID;
            //console.log(sID);
            if (!hqData[this._sMarket].activeImg) {
                hqData[this._sMarket].activeImg = 0;
            }
            //滑动时回调，传sID进来，若图已初始化，则不强行读取，由更新线程更新
            if (sID != undefined) {
                hqData[this._sMarket].activeImg = sID;
                //if(this._oSetNow[hqData[this._sMarket].activeImg].imgLoaded) return;
            }
            if (this._refreshElement) {
                //if(sID != undefined)
                this._refreshElement.onclick = this._loadImg.Bind(this, sID);
                //else
                //	this._refreshElement.onclick = this._loadImg.Bind(this, 0);
            }

            if (this._imgtimeout) {
                window.clearTimeout(this._imgtimeout);
            }
            this._imgtimeout = window.setTimeout(this._loadImg.Bind(this, sID), this.imgFqc);

            $("hq_" + this._sMarket).lastChild.innerHTML = this._oSetNow[!sID ? 0 : sID][4];

            var _index = hqData[this._sMarket].activeImg;

            if (this._oSetNow[_index][2] != "") {
                this._fillImg(this._oSetNow[_index], _index)
            }

            //表明已初始化
            if (!this._oSetNow[_index].imgLoaded) {
                this._oSetNow[_index].imgLoaded = true;
            }
        },
        _fillImg: function (aSet, index) {//加载图片
            var _container = $("hq_" + hqData[this._sMarket].id).firstChild.nextSibling;
            var _img = new Image();
            _img.src = [aSet[2], aSet[2].indexOf('?') >= 0 ? '&' : "?", 'r=' + (new Date()).getTime()].join("");
            if (_img.onreadystatechange) {
                _img.onreadystatechange = this.insertBc.Bind(this, _container, _img, aSet[3]);
            } else {
                _img.onload = this.insertBc.Bind(this, _container, _img, aSet[3]);
            }
        },
        insertBc: function (_container, _img, _haslink) {//像目标容器填充图片
            if (_img.onreadystatechange) {
                if (_img.readyState != 'loaded' && _img.readyState != 'complete') return;
            }
            _container.innerHTML = "";
            if (_haslink != "") {
                var _link = $C("A");
                _link.target = "_blank";
                _link.className = "piclink";
                _link.href = _haslink;
                _link.setAttribute("hideFocus", true);
                _link.appendChild(_img);
                _container.appendChild(_link);
            } else {
                _container.appendChild(_img);
            }
            _img.height = 123;
            _img.width = 220;
        }
    };
    //window.StockIndex = StockIndex;
    
/*
    var zjlxData = {
        CF_CSI: {//沪深
            id: "CF_CSI",
            set: ['http://data.eastmoney.com/zjlx/dpzjlx.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_ls_min.png']
        },
        CF_SHA: {//沪市
            id: "CF_SHA",
            set: ['http://data.eastmoney.com/zjlx/zs000001.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_sha_min.png']
        },
        CF_SZA: {//深市
            id: "CF_SZA",
            set: ['http://data.eastmoney.com/zjlx/zs399001.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_sza_min.png']
        },
        CF_CYB: {//创业板
            id: "CF_CYB",
            set: ['http://data.eastmoney.com/zjlx/zs399006.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_cyb_min.png']
        },
        CF_SHB: {//沪B
            id: "CF_SHB",
            set: ['http://data.eastmoney.com/zjlx/zs000003.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_shb_min.png']
        },
        CF_SZB: {//深B
            id: "CF_SZB",
            set: ['http://data.eastmoney.com/zjlx/zs399003.html', 'http://cmsjs.eastmoney.com/data/zjlximg/zjlx_szb_min.png']
        }
    };
    var CapitalFlows = {//资金流向
        tabObjs: [],
        imgFqc: 180000,
        activeImg: 0,
        apply: function (tabId, contentId, refBtn) {
            this._tabsElement = $(tabId);
            this._contentElement = $(contentId);
            if (refBtn) this._refreshElement = $(refBtn);
            this._initContent();
            this._tabCtrl = new MutualTab(this._tabsElement, this._contentElement, "mouseover", this.tabObjs, true);
            this._start();
        },
        _initContent: function () {//初始化主体结构
            var tabs = this._tabsElement;
            var tabContent = this._contentElement;
            var tempTabs = tabs.childNodes;

            var showStyle = '', innerContent = '';
            for (var i = 0; i < tempTabs.length; i++) {
                if (tempTabs[i].nodeType == 1) {
                    this.tabObjs.push(this.show.Bind(this, zjlxData[tempTabs[i].id]));

                    innerContent += '<div class="sc-cont-img p1020" id="zjlx_' + tempTabs[i].id + '"' + showStyle + '>';
                    if (showStyle == '') showStyle = ' style="display:none;"';
                    innerContent += '<a hidefocus="true"></a>';
                    innerContent += '<ul class="sc-cont-imglink"><li><a target="_blank" href="http://data.eastmoney.com/zjlx/detail.html">排行</a></li><li><a target="_blank" href="http://data.eastmoney.com/zjlx/list.html">主力</a></li><li><a target="_blank" href="http://data.eastmoney.com/bkzj/jlr.html">监测</a></li></ul>';
                    innerContent += '</div>';
                }
            }
            tabContent.innerHTML = innerContent;
        },
        _start: function () {
            this._tabCtrl.showTab(0, this.show.Bind(this, zjlxData.CF_CSI));
        },
        show: function (aData) {
            this._sMarket = aData.id;
            this._oSetNow = aData.set;
            var _imgCallback = [];

            this._loadImg();
            //$("zjlx_" + aData.id).lastChild.innerHTML = this._oSetNow[2];
        },
        _loadImg: function (sID) {//加载并开启自刷新图片
            if (this._refreshElement) {
                if (sID != undefined)
                    this._refreshElement.onclick = this._loadImg.Bind(this, sID);
                else
                    this._refreshElement.onclick = this._loadImg.Bind(this, 0);
            }

            if (this._imgtimeout) {
                window.clearTimeout(this._imgtimeout);
            }
            this._imgtimeout = window.setTimeout(this._loadImg.Bind(this), this.imgFqc);

            if (this._oSetNow[1] != "") {
                this._fillImg(this._oSetNow, sID)
            }
        },
        _fillImg: function (aSet, index) {
            var _container = $("zjlx_" + this._sMarket).firstChild;
            var _img = new Image();
            _img.src = [aSet[1], aSet[1].indexOf('?') >= 0 ? '&' : "?", 'r=' + (new Date()).getTime()].join("");
            if (_img.onreadystatechange) {
                _img.onreadystatechange = this.insertBc.Bind(this, _container, _img, aSet[0]);
            } else {
                _img.onload = this.insertBc.Bind(this, _container, _img, aSet[0]);
            }
        },
        insertBc: function (_container, _img, _haslink) {
            if (_img.onreadystatechange) {
                if (_img.readyState != 'loaded' && _img.readyState != 'complete') return;
            }
            _container.innerHTML = "";
            if (_haslink != "") {
                var _link = _container;
                _link.target = "_blank";
                _link.className = "piclink";
                _link.href = _haslink;
                _link.appendChild(_img);
                //_container.appendChild(_link);
            } else {
                _container.appendChild(_img);
            }

            //if(_haslink != "") {
            // var _link = $C("A");
            // _link.target = "_blank";
            // _link.href = _haslink;
            // _link.setAttribute("hideFocus",true);
            // _link.appendChild(_img);
            // _container.appendChild(_link);
            // } else {
            // _container.appendChild(_img);
            // }

            _img.height = 123;
            _img.width = 220;
        }
    };
    window.CapitalFlows = CapitalFlows;
*/

    if (document.getElementById("hqzx")) {
        StockIndex.apply('stockTabs', 'stockContent', 'refbtn_stock');
    }
})();



