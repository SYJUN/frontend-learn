
(function () {      //EM_Search_sm

    $ = function (s) {
        return (typeof (s) == "string") ? document.getElementById(s) : s;
    }
    if (typeof $ == "undefined") { $ = function (a) { return document.getElementById(a) } }
    if (typeof $C == "undefined") { $C = function (a) { return document.createElement(a) } }

    var soOpts = {
        // 股吧
        guba: ["http://guba.eastmoney.com/", "http://guba.eastmoney.com/{#KEY#},guba.html", "http://quote.eastmoney.com/search.html?stockcode={#EKEY#}&toba=1"],
        // 资金流向
        zjlx: ["http://data.eastmoney.com/zjlx/", "http://data.eastmoney.com/zjlx/{#KEY#}.html", "http://data.eastmoney.com/zjlx/search.aspx?key={#EKEY#}"],
        // 搜微博话题
        weibo: ["http://t.eastmoney.com/", "http://t.eastmoney.com/search.aspx?kw={#EURIKEY#}", "http://t.eastmoney.com/search.aspx?kw={#EURIKEY#}"],
        // 进微博
        wbcode: ["http://t.eastmoney.com/", "http://t.eastmoney.com/stock.aspx?code={#KEY#}", "http://t.eastmoney.com/stock.aspx?code={#EURIKEY#}"],
        // 基金
        fund: ["http://fund.eastmoney.com/fund{#TYPE#}.html", "http://fund.eastmoney.com/{#KEY#}.html", "http://fund.eastmoney.com/data/fundsearch.aspx?q={#EURIKEY#}&t={#TYPE#}"],
        // 公告
        notice: ["http://data.eastmoney.com/notice/", "http://data.eastmoney.com/notice/Notice_list.aspx?code={#KEY#}", "http://data.eastmoney.com/notice/Notice_list.aspx?code={#EKEY#}"],
        // 博客
        blog: ["http://blog.eastmoney.com/", "http://so.eastmoney.com/Search.htm?t=3&q={#EKEY#}", "http://blog.eastmoney.com/search.aspx?k={#EURIKEY#}"],
        // 资讯
        news: ["http://so.eastmoney.com/", "http://so.eastmoney.com/Search.aspx?q={#EKEY#}&searchclass=body", "http://so.eastmoney.com/Search.aspx?q={#EKEY#}&searchclass=body"],
        // 研报
        report: ["http://data.eastmoney.com/report/", "http://data.eastmoney.com/report/{#EKEY#}.html", "http://data.eastmoney.com/report/{#EKEY#}.html"]
    };

    function getRadioValue(radio) {
        if (!radio.length && radio.type.toLowerCase() == 'radio') {
            return (radio.checked) ? radio.value : '';
        }
        if (radio[0].tagName.toLowerCase() != 'input' || radio[0].type.toLowerCase() != 'radio') return '';
        var len = radio.length;
        for (i = 0; i < len; i++) {
            if (radio[i].checked) {
                return radio[i].value;
            }
        }
        return '';
    }

    function soSome(type, id, param) {
        var key = $(id) ? $(id).value : "";
        if (key == "输代码、名称或拼音" ||
            key == "输代码、名称或简拼" ||
            key == "输入关键字" ||
            key == "输入代码或名称" ||
            key == "输入代码或名称" ||
            key == "输代码或名称" ||
            key == "输基金代码或简称") key = "";

        var _urlSet = soOpts[type], _url;

        if (key == "") {
            _url = _urlSet[0];
            if (type == "fund") {
                _url = _url.replace("{#TYPE#}", (param == "gz" ? "guzhi" : ""));
            }
        } else if (/^\d{6}$/.test(key)) {
            _url = _urlSet[1];
            if (type == "blog") {
                _url = (getRadioValue(param) == "body" ? _urlSet[1] : _urlSet[2]);
            }
            _url = _url.replace("{#KEY#}", key);
            _url = _url.replace("{#EKEY#}", escape(key));
            _url = _url.replace("{#EURIKEY#}", encodeURI(key));
        } else {
            _url = _urlSet[2];
            if (type == "blog") {
                _url = (getRadioValue(param) == "body" ? _urlSet[1] : _urlSet[2]);
            }
            _url = _url.replace("{#KEY#}", key);
            _url = _url.replace("{#EKEY#}", escape(key));
            _url = _url.replace("{#EURIKEY#}", encodeURI(key));
            if (type == "fund") _url = _url.replace("{#TYPE#}", param);
        }
        //alert(_url);
        window.open(_url);
        //openUrl(_url);
        return false;
    }

    window.hqzxSs = new StockSuggest("hqzx_txt", { text: "输入代码、名称或简拼", autoSubmit: true, width: 200, header: ["选项", "代码", "名称", "类型"], body: [-1, 1, 4, -2], callback: null });
    $("hqzx_btn1").onclick = function () { hqzxSs.Submit() };
    $("hqzx_btn2").onclick = function () { soSome('guba', 'hqzx_txt') };
    

}());