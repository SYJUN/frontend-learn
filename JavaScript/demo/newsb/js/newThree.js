/***
** 功能：  选项卡切换


***/
jQuery.fn.Tabs = function (options) {
    var defaults = {
        tabSelector: ".tabs li",
        conSelector: ".tabcontent",
        focusClass: "c",
        moreTrigger: ".tabTitle .more .link",
        events: "mouseover",
        selected: 0,
        delay: 0.2
    };
    var events = ["mouseover", "click"];
    var settings = jQuery.extend({}, defaults, options);
    var that = this;
    var _tabs = jQuery(settings.tabSelector, that);
    var _cons = jQuery(settings.conSelector, that);
    var _more = jQuery(settings.moreTrigger, that);
    var _isDelay = settings.events == events[0] ? !0 : !1;

    void function () {
        var tab = _tabs.eq(settings.selected);
        if (tab && tab.length == 0) {
            tab = _tabs.eq(0);
        }
        tab.addClass(settings.focusClass);
        tab.siblings(settings.tabSelector).removeClass(settings.focusClass);

        var cons = _cons.eq(settings.selected);
        if (cons && cons.length == 0) {
            cons = _cons.eq(0);
        }
        cons.show();
        cons.siblings(settings.conSelector).hide();

        var more = _more.eq(settings.selected);
        if (more && more.length == 0) {
            more = _more.eq(0);
        }
        more.show();
        more.siblings().hide();
    }();

    _tabs.each(function (i, v) {
        jQuery(v).on(settings.events, function () {
            var _this = this;
            delay.apply(this, [settings.delay, function () {
                jQuery(_this).addClass(settings.focusClass);
                jQuery(_this).siblings(settings.tabSelector).removeClass(settings.focusClass);
                jQuery(_cons[i]).fadeIn();
                jQuery(_cons[i]).siblings(settings.conSelector).hide();
                jQuery(_more[i]).show();
                jQuery(_more[i]).siblings().hide();
            }, _isDelay])
        });
    });
    //接收两个参数 t延迟时间秒为单位，fn要执行的函数,m是否执行延迟取决于事件的类型
    var delay = function (t, fn, m) {
        if (m) {
            var _this = this,
                d = setInterval(function () {
                    fn.apply(_this);
                }, t * 1000);
            _this.onmouseout = function () {
                clearInterval(d);
            };
        }
        else fn.apply(this);
    }
}

$("#tabList").Tabs({ tabSelector: ".vTabs li", conSelector: ".vContent  .item", focusClass: "active" });
$("#tabList2").Tabs({ tabSelector: ".vTabs li", conSelector: ".vContent  .item", focusClass: "active" });
$(".inbox1").Tabs({tabSelector:".clearfix li",conSelector:".box1content .item",focusClass:"current1"});
$("#hq1").Tabs({tabSelector:".panel-head span",conSelector:".panel-body .item",focusClass:"panel-title"});
$("#hq2").Tabs({tabSelector:".panel-head span",conSelector:".panel-body .item",focusClass:"panel-title"});
$("#hq3").Tabs({tabSelector:".panel-head span",conSelector:".panel-body .item",focusClass:"panel-title"});
$("#hq4").Tabs({tabSelector:".panel-head span",conSelector:".panel-body .item",focusClass:"panel-title"});
