//封装DOM、Event接口
var GLOBAL = {};
GLOBAL.namespace = function(str){
  var arr = str.split('.'),o = GLOBAL,i;
    for(i = (arr[0] == 'GLOBAL') ? 1 : 0; i < arr.length; i++){
        o[arr[i]] = o[arr[i]] || {};
        o = o[arr[i]];
    }
};

GLOBAL.namespace('Dom');

GLOBAL.Dom.getElementsByClassName = function(str,root,tag){
    if(root){
        root = typeof root == 'string' ? document.getElementById(root) : root;
    }else{
        root = document.body;
    }
    tag = tag || '*';
    var els = root.getElementsByTagName(tag),
        arr = [];
    for(var i = 0,n = els.length;i < n ; i++){
        for(var j = 0,k = els[i].className.split(' '),l = k.length; j < l; j++){
            if(k[j] == str){
                arr.push(els[i]);
                break;
            }
        }
    }
    return arr;
};

GLOBAL.namespace('Event');
GLOBAL.Event.on = function(node,eventType,handler,scope){
    node = typeof node == 'string' ? document.getElementById(node) : node;
    scope = scope || node;
    if(document.all){
        node.attachEvent('on'+eventType,function(){
            handler.apply(scope,arguments);
        });
    }else{
        node.addEventListener(eventType,function(){
            handler.apply(scope,arguments);
        },false);
    }
};

//定义 Rate 类
/*function Rate(rateRoot){
    var root = typeof rateRoot == 'string' ? document.getElementById(rateRoot) : rateRoot,
        imgs = ['images/start1.png','images/start2.png'],
        rateFlag;
    var len = root.length;
    for(var i = 0;i < len;i++){
        var test = fun(i);
        test();
    }
    function fun(x){
        return function(){
            var  items = root[x].getElementsByTagName('img');
            for(var j = 0,n = items.length;j < n ;j++){
                items[j].index = j;
            }
            GLOBAL.Event.on(root[x],'mouseover',function(e){
                if(rateFlag) return;
                var target = e.target || e.srcElement;
                if(target.tagName.toLocaleLowerCase() != 'img') return;
                for(var i = 0;i < n;i++){
                    if (i <= target.index){
                        items[i].src = imgs[1];
                    }else{
                        items[i].src = imgs[0];
                    }
                }
            });
            GLOBAL.Event.on(root[x],'mouseout',function(e){
                if(rateFlag) return;
                var target = e.target || e.srcElement;
                for(var i = 0,n = items.length; i < n;i++){
                    items[i].src = imgs[0];
                }
            });

            GLOBAL.Event.on(root[x],'click',function(e){
                if(rateFlag) return;
                rateFlag = true;
                var target = e.target || e.srcElement;
                alert('您打了' + (target.index + 1) + '分');
            });
        }
    }

}*/
function Rate(rateRoot){
    var root = typeof rateRoot == 'string' ? document.getElementById(rateRoot) : rateRoot,
        imgs = ['images/start1.png','images/start2.png'],
        rateFlag;
    var  items = root.getElementsByTagName('img');
    for(var j = 0,n = items.length;j < n ;j++){
        items[j].index = j;
    }
    GLOBAL.Event.on(root,'mouseover',function(e){
        if(rateFlag) return;
        var target = e.target || e.srcElement;
        if(target.tagName.toLocaleLowerCase() != 'img') return;
        for(var i = 0;i < n;i++){
            if (i <= target.index){
                items[i].src = imgs[1];
            }else{
                items[i].src = imgs[0];
            }
        }
    });
    GLOBAL.Event.on(root,'mouseout',function(e){
        if(rateFlag) return;
        var target = e.target || e.srcElement;
        for(var i = 0,n = items.length; i < n;i++){
            items[i].src = imgs[0];
        }
    });

    GLOBAL.Event.on(root,'click',function(e){
        if(rateFlag) return;
        rateFlag = true;
        var target = e.target || e.srcElement;
        alert('您打了' + (target.index + 1) + '分');
    });
}
Rate( GLOBAL.Dom.getElementsByClassName('J_rate','','p')[0] );
Rate( GLOBAL.Dom.getElementsByClassName('J_rate','','p')[1] );
Rate( GLOBAL.Dom.getElementsByClassName('J_rate','','p')[2] );













