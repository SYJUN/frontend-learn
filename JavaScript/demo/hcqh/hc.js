var hqData = {
    CSI: {//沪深
        id: "CSI",
        set: [
            ['0000011', '上证综指', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/0000011RS.png', 'http://quote.eastmoney.com/zs000001.html', 'cn', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#10">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#10?sortType=H&amp;sortRule=-1">量比</a></li><li><a href="http://quote.eastmoney.com/center/list.html#10?sortType=J&amp;sortRule=-1">换手</a></li><li><a href="http://quote.eastmoney.com/center/list.html#10?sortType=I&amp;sortRule=-1">市盈</a></li>'],
            ['3990012', '深证成指', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/3990012RS.png', 'http://quote.eastmoney.com/zs399001.html', 'cn', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#20">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#20?sortType=H&amp;sortRule=-1">量比</a></li><li><a href="http://quote.eastmoney.com/center/list.html#20?sortType=J&amp;sortRule=-1">换手</a></li><li><a href="http://quote.eastmoney.com/center/list.html#20?sortType=I&amp;sortRule=-1">市盈</a></li>'],
            ['0003001', '沪深300', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/0003001RS.png', 'http://quote.eastmoney.com/zs000300.html', 'cn', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#28003500_0_2">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=H&amp;sortRule=-1">量比</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=J&amp;sortRule=-1">换手</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28003500_0_2?sortType=I&amp;sortRule=-1">市盈</a></li>'],
            ['3990062', '创业板', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/3990062RS.png', 'http://quote.eastmoney.com/zs399006.html', 'cn', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#27">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#27?sortType=H&amp;sortRule=-1">量比</a></li><li><a href="http://quote.eastmoney.com/center/list.html#27?sortType=J&amp;sortRule=-1">换手</a></li><li><a href="http://quote.eastmoney.com/center/list.html#27?sortType=I&amp;sortRule=-1">市盈</a></li>']
        ],
        api: 'http://hqdigi2.eastmoney.com/EM_Quote2010NumericApplication/Index.aspx?Type=Z&IDs={#IDS#}&jsname=js_CSI&r={#RN#}'
    },
    HK: {//香港
        id: "HK",
        set: [
            ['1100005', '恒生指数', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RS&ID=1100005', 'http://quote.eastmoney.com/hk/zs110000.html', 'hk', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#50_1">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#50_1?sortType=F&sortRule=-1">成交</a></li><li><a href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
            ['1100305', '红筹指数', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RS&ID=1100305', 'http://quote.eastmoney.com/hk/zs110030.html', 'hk', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#28HSCCI_1">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28HSCCI_1?sortType=F&sortRule=-1">成交</a></li><li><a href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
            ['1100105', '国企指数', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RS&ID=1100105', 'http://quote.eastmoney.com/hk/zs110010.html', 'hk', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#28HSCEI_1">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28HSCEI_1?sortType=F&sortRule=-1">成交</a></li><li><a href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>'],
            ['1100505', '创业板', 'http://hqhkpic.eastmoney.com/EM_Quote2010PictureProducter/Index.aspx?ImageType=RS&ID=1100505', 'http://quote.eastmoney.com/hk/zs110050.html', 'hk', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#28GEM_1">涨幅</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28GEM_1?sortType=F&sortRule=-1">成交</a></li><li><a href="http://quote.eastmoney.com/center/list.html#ah_1">比价</a></li>']
        ],
        api: 'http://hq2hk.eastmoney.com/EM_Quote2010NumericApplication/Index.aspx?Type=Z&IDs={#IDS#}&jsname=js_HK&r={#RN#}'
    },
    OCCIDENT: {//欧美
        id: "OCCIDENT",
        set: [
            ['INDU7', '道琼斯', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/INDU7RS.png', 'http://quote.eastmoney.com/gb/zsindu.html', 'eu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>'],
            ['CCMP7', '纳斯达克', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/CCMP7RS.png', 'http://quote.eastmoney.com/gb/zsCCMP.html', 'eu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>'],
            ['SPX7', '标普500', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/SPX7RS.png', 'http://quote.eastmoney.com/gb/zsSPX.html', 'eu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a href="http://quote.eastmoney.com/center/list.html#itchina_2">概念</a></li><li ><a href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>'],
            ['UKX7', '英国FT', 'http://hqgjgppic.eastmoney.com/EM_Quote2010PictureProductor/Picture/UKX7RS.png', 'http://quote.eastmoney.com/gb/zsUKX.html', 'eu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#70_2">排行</a></li><li><a href="http://quote.eastmoney.com/center/list.html#28CHINA_2">概念</a></li><li><a href="http://quote.eastmoney.com/center/list.html#286_2">科技</a></li>']
        ],
        api: 'http://hq2gjgp.eastmoney.com/EM_Quote2010NumericApplication/Index.aspx?Type=Z&IDs={#IDS#}&jsname=js_OCCIDENT&r={#RN#}'
    },
    FUND: {//基金
        id: "FUND",
        set: [
            ['0000111', '沪基指数', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/0000111rs.png', 'http://quote.eastmoney.com/zs000011.html', 'fu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#285002_4">封基</a></li><li><a href="http://quote.eastmoney.com/center/fundlist.html#3,_4">开基</a></li><li><a href="http://quote.eastmoney.com/center/list.html#2850013_4">ETF</a></li><li><a href="http://quote.eastmoney.com/center/list.html#2850014_4">LOF</a></li>'],
            ['3993052', '深基指数', 'http://hqpiczs.dfcfw.com/EM_Quote2010PictureProducter/Picture/3993052rs.png', 'http://quote.eastmoney.com/zs399305.html', 'fu', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#285002_4">封基</a></li><li><a href="http://quote.eastmoney.com/center/fundlist.html#3,_4">开基</a></li><li><a href="http://quote.eastmoney.com/center/list.html#2850013_4">ETF</a></li><li><a href="http://quote.eastmoney.com/center/list.html#2850014_4">LOF</a></li>']
        ],
        api: 'http://hqdigi2.eastmoney.com/EM_Quote2010NumericApplication/Index.aspx?Type=Z&IDs={#IDS#}&jsname=js_FUND&r={#RN#}'
    },
    FUTURES: {//期货
        id: "FUTURES",
        set: [
            ['IFDYLX1', '股指期货', 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/IFDYLX1RS.png', 'http://quote.eastmoney.com/gzqh/IFDYLX.html', 'qh', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
            ['TFGZDJ1', '国债期货', 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/TFGZDJ1RS.png', 'http://quote.eastmoney.com/gzqh/TFGZDJ.html', 'qh', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
            ['CONC0', '美原油', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/conc0RS.png', 'http://quote.eastmoney.com/qihuo/CONC.html', 'qh', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>'],
            ['GLNC0', '美黄金', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/glnc0RS.png', 'http://quote.eastmoney.com/qihuo/GLNC.html', 'qh', 'redup', '<li><a href="http://quote.eastmoney.com/center/list.html#12_5_3">期指</a></li><li><a href="http://quote.eastmoney.com/center/futurelist.html#11_5_0">内盘</a></li><li><a href="http://quote.eastmoney.com/center/list.html#gjqh_5">外盘</a></li>']
        ],
        api: ['http://hq2gnqh.eastmoney.com/EM_Futures2010NumericApplication/Index.aspx?Type=Z&IDs=IFDYLX1,TFGZDJ1&jsname=js_FUTURES_GNQH&r={#RN#}', 'http://hq2gjqh.eastmoney.com/EM_Futures2010NumericApplication/Index.aspx?Type=Z&IDs=CONC0,GLNC0&jsname=js_FUTURES_GJQH&r={#RN#}']
    },
    FOREX: {//外汇
        id: "FOREX",
        set: [
            ['DINI0', '美元指数', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/dini0RS.png', 'http://quote.eastmoney.com/qihuo/DINI.html', 'wh', 'redup', '<li><a href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
            ['EURUSD0', '欧元美元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RS&id=EURUSD0', 'http://quote.eastmoney.com/forex/EURUSD.html', 'wh', 'redup', '<li><a href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
            ['GBPUSD0', '英镑美元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RS&id=GBPUSD0', 'http://quote.eastmoney.com/forex/GBPUSD.html', 'wh', 'redup', '<li><a href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a href="http://forex.eastmoney.com/FC.html">日历</a></li>'],
            ['USDJPY0', '美元日元', 'http://hqgjqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RS&id=USDJPY0', 'http://quote.eastmoney.com/forex/USDJPY.html', 'wh', 'redup', '<li><a href="http://quote.eastmoney.com/center/forexlist.html#3_6">牌价</a></li><li><a href="http://forex.eastmoney.com/BankRate.html">利率</a></li><li><a href="http://data.eastmoney.com/money/calc/ForexExchange.html">兑换</a></li><li><a href="http://forex.eastmoney.com/FC.html">日历</a></li>']
        ],
        api: 'http://hq2gjqh.eastmoney.com/EM_Futures2010NumericApplication/Index.aspx?Type=Z&IDs={#IDS#}&jsname=js_FOREX&r={#RN#}'
    }
};

(function () {
    if (typeof $ == 'undefined') $ = function (t) { return document.getElementById(t) };
    if (typeof $C == 'undefined') $C = function (t) { return document.createElement(t) };
    Function.prototype.Bind = function () {
        var __m = this, object = arguments[0], args = new Array();
        for (var i = 1; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        return function () {
            return __m.apply(object, args);
        }
    };
    var RollTab = function () {
        this.init.apply(this, arguments);
    };

    RollTab.prototype = {
        nTimeout: 40,
        nRepeat: 8,
        nContentHeight: 140,
        nMoveTime: 0,
        init: function (aData, callbacks) {
            this.oTitles = [];
            this.oContents = [];
            this.oImgs = [];

            this._callbacks = callbacks;

            var tempDls = $("hq_" + aData.id).getElementsByTagName("DL");
            for (var i = 0; i < tempDls.length; i++) {
                if (tempDls[i].nodeType == 1 && aData.set[i][2] != "") {
                    // aData.set[i][2]不为空时，即配置项中没写图片地址时，不添加滚动效果
                    for (var j = 0; j < tempDls[i].childNodes.length; j++) {
                        switch (tempDls[i].childNodes[j].tagName) {
                            case "DT":
                                this.oTitles.push(tempDls[i].childNodes[j]);
                                break;
                            case "DD":
                                this.oContents.push(tempDls[i].childNodes[j]);
                                continue;
                                break;
                        }
                    };
                } else {
                    var _arr = tempDls[i].className.split(' ');
                    _arr.push('noscroll');
                    tempDls[i].className = _arr.join(' ');
                    // 不能滚动的列给出特别的样式提醒用户
                }
            };
            // 这个地方这样的写法有问题，如果一共有五个tab，中间两个没有图片，则接下来的tab会沿用本应跳过的序号
            // 导致接下来的回调加载图片无法获取对应的图片地址

            this.activeIndex = 0;

            for (var i = 0; i < this.oTitles.length; i++) {
                // this.oTitles[i].onclick = this.start.Bind(this, i);
                this.oTitles[i].onmouseover = this.overCheck.Bind(this, i);
                this.oTitles[i].onmouseout = this.outCheck.Bind(this, i);
            };

            // this.aColors = ColorCal.Init("ff0000", "ffd9d9", this.nRepeat);

            // 匀速滚动。如果要改回来，只需修改move方法中的两处
            // this.nPace = this.nContentHeight / this.nRepeat;

            // 加速度滚动
            // obj_x += (cursor_x - obj_x) * k + (end_x - cursor_x)
            this.aPace = [];
            var _v = 0, k = 0.3, maxSteps = 20;
            for (var i = 0; i < maxSteps; i++) {
                var temp = _v;
                _v += (this.nContentHeight - _v) * k;
                this.aPace.push(Math.round(_v - temp));

                if (this.nContentHeight - _v < 3 || i == maxSteps - 1) {
                    this.steps = this.aPace.length;
                    break;
                }
            };
        },
        overCheck: function (i) {
            this.overTt = window.setTimeout(this.start.Bind(this, i), 300);
        },
        outCheck: function () {
            if (this.overTt) {
                window.clearTimeout(this.overTt);
            }
        },
        start: function (_index) {
            if (_index == this.activeIndex || this.bDisabled) return;

            if (this.oTitles[this.activeIndex]) {
                this.oTitles[this.activeIndex].className = "";
                //this.oImgs[this.activeIndex].style.display = "none";
            }

            this.move(this.activeIndex, _index);

            this.activeIndex = _index;
            this.oTitles[this.activeIndex].className = "active";
            //this.oImgs[this.activeIndex].style.display = "";
        },
        move: function (_activeIndex, _index) {
            if (this.nMoveTime == this.steps - 1) {
                //this.oTitlePre.style.background = this.aColors[7];
                //this.oTitleNow.style.background = this.aColors[0];
                this.oPre.style.height = "0px";
                this.oNow.style.height = this.nContentHeight + "px";
                //window.clearTimeout(this.moveTO);
                this.nMoveTime = 0;
                this.bDisabled = 0;

                this._callbacks[this.activeIndex]();
                //添加回调支持
                return;
            }
            if (this.nMoveTime == 0) {
                this.oPre = this.oContents[_activeIndex];
                this.oNow = this.oContents[_index];
                this.oTitlePre = this.oTitles[_activeIndex];
                this.oTitleNow = this.oTitles[_index];
                //this.nMoveTime = 0;
                this.bDisabled = 1;
            }
            //		var a = this.oPre.clientHeight - this.nPace + "px";
            //		var b = this.oNow.clientHeight + this.nPace + "px";
            //		this.oTitlePre.style.background = this.aColors[this.nMoveTime + 1];
            //		this.oTitleNow.style.background = this.aColors[6 - this.nMoveTime];

            //alert([this.oPre.clientHeight - this.aPace[this.nMoveTime], "px"].join(''));
            this.oPre.style.height = [this.oPre.clientHeight - this.aPace[this.nMoveTime], "px"].join('');
            this.oNow.style.height = [this.oNow.clientHeight + this.aPace[this.nMoveTime], "px"].join('');
            this.nMoveTime++;

            this.moveTO = window.setTimeout(this.move.Bind(this), this.nTimeout);
        }
    };


    var MutualTab = function () {
        this.init.apply(this, arguments);
    };
    MutualTab.prototype = {
        init: function (tabs, targets, type, subTargets, callbackObjArr) {
            this.tabs = [];
            var tempTabs = tabs.childNodes;
            for (var i = 0; i < tempTabs.length; i++) {
                if (tempTabs[i].nodeType == 1) {
                    this.tabs.push(tempTabs[i]);
                }
            };
            this.activeTab = null;//this.tabs[0];

            if (targets) {
                this.targets = [];
                var tempTargets = targets.childNodes;
                for (var j = 0; j < tempTargets.length; j++) {
                    if (tempTargets[j].nodeType == 1) {
                        this.targets.push(tempTargets[j]);
                    }
                };
                this.activeTarget = this.targets[0];
            }

            //		if (subTargets) {
            //			this.subTargets = [];
            //			var tempSubTargets = subTargets.childNodes;
            //			for (var h = 0; h < tempSubTargets.length; h++){
            //				if (tempSubTargets[h].nodeType == 1){
            //					this.subTargets.push(tempSubTargets[h]);
            //				}
            //			};
            //			this.activeSubTarget = this.subTargets[0];
            //		}

            if (callbackObjArr) {
                if (type != "mouseover") {
                    for (var k = 0; k < this.tabs.length; k++) {
                        this.tabs[k].onclick = this.showTab.Bind(this, k, callbackObjArr[k]);
                    }
                } else {
                    for (var k = 0; k < this.tabs.length; k++) {
                        this.tabs[k].onmouseover = this._over.Bind(this, k, callbackObjArr[k]);
                        this.tabs[k].onmouseout = this._hide.Bind(this, k, callbackObjArr[k]);
                    }
                }
            } else {
                if (type != "mouseover") {
                    for (var k = 0; k < this.tabs.length; k++) {
                        this.tabs[k].onclick = this.showTab.Bind(this, k);
                    }
                } else {
                    for (var k = 0; k < this.tabs.length; k++) {
                        this.tabs[k].onmouseover = this._over.Bind(this, k);
                        this.tabs[k].onmouseout = this._hide.Bind(this, k);
                    }
                }
            }
        },
        showTab: function (_index, callback) {
            if (this.tabs[_index] == this.activeTab) return;

            if (this.activeTab) this.activeTab.className = "";

            this.activeTab = this.tabs[_index];
            this.activeTab.className = "active";

            if (this.targets) {
                if (this.activeTarget) {
                    this.activeTarget.style.display = "none";
                }
                this.activeTarget = this.targets[_index];
                this.activeTarget.style.display = "";
            }

            //		if (this.subTargets) {
            //			this.activeSubTarget.style.display = "none";
            //			this.activeSubTarget = this.subTargets[_index];
            //			this.activeSubTarget.style.display = "";
            //		}

            if (callback) {
                this.activeCallback = callback;
                callback();
            }
        },
        _over: function (obj, _index, callback) {
            this._timeout = window.setTimeout(this.showTab.Bind(this, obj, _index, callback), 200);
        },
        _hide: function () {
            if (this._timeout) {
                window.clearTimeout(this._timeout);
            }
        }
    };
    /*待改进，增加开收盘功能*/
    var StockIndex = {
        _hqTpl: '<ul class="#RG#"><li class="sname">#TITLE#</li><li class="nowprice"><span class="#PBG#">#P#</span></li><li><span class="#PBG#">#CG#</span></li><li><span class="#PBG#">#CGP#</span></li></ul>',
        _linkTpl: '<a href="#URL#" target="_blank">#NAME#</a>',
        _linkUrl: 'http://quote.eastmoney.com/',
        tabObjs: [],
        jsFqc: 500000000,//5000
        imgFqc: 6000,
        init: function () {
            if (hqData["FUTURES"]) {

                for (var i in hqData["FUTURES"].set) {
                    var _code = hqData["FUTURES"].set[i][0];
                    if (_code == 'IFDYLX1' || _code == 'TFGZDJ1') {
                        if (_code == 'IFDYLX1' && window["IFCode"]) {
                            hqData["FUTURES"].api[0] = hqData["FUTURES"].api[0].replace(_code, window["IFCode"] + '1');
                            _code = window["IFCode"] + '1';
                            hqData["FUTURES"].set[i][2] = 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Picture/' + _code + 'RS.png';

                        }
                        if (_code == 'TFGZDJ1' && window["TFCode"]) {
                            hqData["FUTURES"].api[0] = hqData["FUTURES"].api[0].replace(_code, window["TFCode"] + '1');
                            _code = window["TFCode"] + '1';
                            hqData["FUTURES"].set[i][2] = 'http://hqgnqhpic.eastmoney.com/EM_Futures2010PictureProducter/Index.aspx?imagetype=RS&ID=' + _code;
                        }

                        hqData["FUTURES"].set[i][0] = _code;
                        hqData["FUTURES"].set[i][3] = 'http://quote.eastmoney.com/gzqh/' + _code.substring(0, _code.length - 1) + '.html';


                    }
                }
            }
            this._tabsElement = $("mTabs");
            this._contentElement = $("mContents");
            this._initContent();
            this._tabCtrl = new MutualTab(this._tabsElement, this._contentElement, 'mouseover', null, this.tabObjs);
            this._start();

            $("hq_FUND").lastChild.innerHTML += '<div style="border-top:1px solid #E4F3FF; text-align: right;height: 25px;line-height: 25px;padding-right: 10px;padding-top: 30px;"><a id="view_jx" class="view_jx" target="_blank" href="http://fund.eastmoney.com/fund.html">&gt;&gt;看基金净值排名</a></div>';
        },
        _initContent: function () {//初始化主体结构
            var tabs = this._tabsElement;
            var tabContent = this._contentElement;
            var tempTabs = tabs.childNodes;

            var setArr = null, showStyle = '', innerContent = '', _even = '', _active = '';
            for (var i = 0; i < tempTabs.length; i++) {
                if (tempTabs[i].nodeType == 1) {
                    this.tabObjs.push(this.show.Bind(this, eval("hqData[\"" + tempTabs[i].id + "\"]")));
                    //tabObjs.push(tempTabs[i].id);
                    //alert(tempTabs[i].innerText);

                    innerContent += '<div id="hq_' + tempTabs[i].id + '"' + showStyle + '>';
                    if (showStyle == '') showStyle = ' style="display:none;"';

                    setArr = eval("hqData[\"" + tempTabs[i].id + "\"].set");

                    for (var j in setArr) {
                        _even = '', _active = '';
                        if (j % 2 === 0) _even = ' class="grey"';
                        if (j == 0) _active = ' class="active"';

                        innerContent += '<dl' + _even + '>' +
                        '<dt' + _active + '></dt>' +
                        '<dd style="height:' + (j == 0 ? '140' : '0') + 'px;' + (j == setArr.length - 1 ? 'border-bottom:0;' : '') + '">' +
                        '<cite></cite>' +
                        '<div class="sinfo"></div>' +
                        '</dd>' +
                        '</dl>';
                    }

                    innerContent += '</div>';
                }
            }
            tabContent.innerHTML = innerContent;
        },
        _start: function () {//开启 市场自动切花待开发
            /*this.bjDate = new Date(window.StandardBJTime * 1000); // 21点25到7点切换至美股

             var _sTime = this.bjDate.getHours() * 100 + this.bjDate.getMinutes();
             var _day = this.bjDate.getDay();
             //		_sTime = 2210;
             //		_day = 0;

             if(_sTime >= 700 && _sTime < 2125) {
             this.show(oWmtdata.china);
             } else if(_day == 6 && _sTime <= 700) {
             this._tabCtrl.showTab(2,this.show.Bind(this,oWmtdata.america));
             } else if(_day != 0 && _day != 6) {
             this._tabCtrl.showTab(2,this.show.Bind(this,oWmtdata.america));
             } else {
             this.show(oWmtdata.china);
             }*/
            //this.show(hqData.CSI);
            this._tabCtrl.showTab(0, this.show.Bind(this, hqData.CSI));
        },
        show: function (aData) {//显示内容
            this._sMarket = aData.id;
            this._oSetNow = aData.set;
            var _imgCallback = [];
            var _hqcodeNow = [];

            if (!hqData[this._sMarket]._hqApi) {
                for (var i = 0; i < this._oSetNow.length; i++) {
                    _imgCallback.push(this._loadImg.Bind(this, i));
                    _hqcodeNow.push(this._oSetNow[i][0]);
                }
                if (typeof (aData.api) !== 'string') {
                    hqData[this._sMarket]._hqApi = aData.api;
                } else {
                    hqData[this._sMarket]._hqApi = aData.api.replace('{#IDS#}', _hqcodeNow.join(","));
                }
            }
            if (!hqData[this._sMarket]._containersNow) {
                hqData[this._sMarket]._containersNow = [];
                var _tmpchilds = $("hq_" + aData.id).childNodes;
                var _tmpFirst = null;
                for (var i = 0; i < _tmpchilds.length; i++) {
                    if (_tmpchilds[i].nodeType == 1) {
                        _tmpFirst = _tmpchilds[i].firstChild;
                        hqData[this._sMarket]._containersNow.push(_tmpFirst.nodeType == 1 ? _tmpFirst : _tmpFirst.nextSibling);
                    }
                }
            }

            this._loadhq();
            this._loadImg();

            if (!aData.hasscroll) {
                aData.hasscroll = new RollTab(aData, _imgCallback);
            }
        },
        _loadhq: function () {//读取行情
            if (this._hqtimeout) window.clearTimeout(this._hqtimeout);
            this._hqtimeout = window.setTimeout(this._loadhq.Bind(this), this.jsFqc);

            if (typeof (hqData[this._sMarket]._hqApi) !== 'string') {
                this._ios(hqData[this._sMarket]._hqApi, this._mergeData);
                //this._ios(hqData[this._sMarket]._hqApi, null);
            } else {
                this._io(hqData[this._sMarket]._hqApi.replace("{#RN#}", (new Date()).getTime()), this._fill.Bind(this));
            }
            this._fill();
        },
        _mergeData: function (jsnames) {//合并期货请求数据
            var jsName = "js_" + this._sMarket;
            if (!window[jsName]) window[jsName] = { futures: [] };
            var _type = "FUTURES";
            for (var i in jsnames) {
                if (window[jsnames[i]]) {
                    if (jsnames[i].indexOf('FUTURES') >= 0) {
                        //console.log(window[jsnames[i]].futures);
                        window[jsName].futures = window[jsName].futures.concat(window[jsnames[i]].futures)
                    }
                }
            }
            //console.log(window[jsName].futures);
            if (_type == 'FUTURES' && window[jsName].futures.length == hqData.FUTURES.set.length) {
                this._fill();
            }
        },
        _fill: function () {//根据数据开始填充
            var jsName = "js_" + this._sMarket;
            //console.log("_fill:" + jsName)
            //if (typeof (hqData[this._sMarket]._hqApi) !== 'string') jsName += '_Merge';
            var sData = window[jsName];
            //console.log(this._oSetNow[0][0]);
            for (var i = 0; i < this._oSetNow.length; i++) {
                //PATCH 20101229 切换页面导致script加载取消
                var showData = this._parseData(sData, this._oSetNow[i], i);
                //console.log(showData);
                if (showData.Stock[0] != "") {
                    if (showData.Stock.length > 1) {
                        this._setHtml(showData.Stock[1], hqData[this._sMarket]._containersNow[i]);
                        window.setTimeout(this._setHtml.Bind(this, showData.Stock[0], hqData[this._sMarket]._containersNow[i]), 1000);
                    } else {
                        this._setHtml(showData.Stock[0], hqData[this._sMarket]._containersNow[i]);
                    }
                    this._setHtml(showData.Info, hqData[this._sMarket]._containersNow[i].nextSibling.lastChild);
                }
            };
            //$('log').innerHTML += '#############<br />';
        },
        _parseData: function (sData, aSet, index) {//贴入具体行情内容并
            var _tmpHTML = '', _aHTML = '', _iHTML = '';

            if (sData) {
                switch (aSet[4]) {
                    case "cn":
                    case "hk":
                    case "eu":
                    case "fu":
                        if (sData.quotation && sData.quotation[index]) {
                            sData = sData.quotation[index].split(',');
                            var _redorgreen = this._setColorClass((sData[10] * 1), aSet[5]);
                            var _price = (sData[5] * 1).toFixed(2);
                            //console.log('_parseData:'+sData+" i:"+index);

                            if (this._oSetNow[index].prePrice && this._oSetNow[index].prePrice == _price) {
                                _tmpHTML = '';
                            } else {
                                _tmpHTML = this._arrReplace(this._hqTpl, [
                                    ['#TITLE#', this._linkTpl.replace('#URL#', this._makeUrl(this._linkUrl, aSet[3])).replace('#NAME#', aSet[1])],
                                    ['#P#', _price],
                                    ['#CG#', (sData[10] * 1) > 0 ? '+' + (sData[10] * 1).toFixed(2) : (sData[10] * 1).toFixed(2)],
                                    ['#CGP#', (sData[11] * 1) > 0 ? ['+', sData[11]].join('') : sData[11]],
                                    ['#RG#', _redorgreen]
                                ]);
                                //alert(_price);
                            }
                            /*_iHTML = '<ul>' +
                             '<li>成交:<i>' + sData[8] + '</i></li>' +
                             '<li>今开:<i>' + sData[4] + '</i></li>' +
                             '<li>最高:<i>' + sData[6] + '</i></li>' +
                             '<li>最低:<i>' + sData[7] + '</i></li>' +
                             '</ul>';*/
                        }
                        break;
                    case "qh":
                    case "wh":
                        if (sData.futures && sData.futures[index]) {
                            //$('log').innerHTML += sData.futures[index] + "<br />";
                            sData = sData.futures[index].split(',');
                            var _redorgreen = this._setColorClass((sData[17] * 1), aSet[5]);
                            var _price =  sData[5];
                            //console.log('_parseData:'+sData+" i:"+index);

                            if (this._oSetNow[index].prePrice && this._oSetNow[index].prePrice == _price) {
                                _tmpHTML = '';
                            } else {
                                _tmpHTML = this._arrReplace(this._hqTpl, [
                                    ['#TITLE#', this._linkTpl.replace('#URL#', this._makeUrl(this._linkUrl, aSet[3])).replace('#NAME#', aSet[1])],
                                    ['#P#', _price],
                                    ['#CG#',  sData[17] ],
                                    ['#CGP#', (sData[18] * 1) > 0 ? ['+', sData[18]].join('') : sData[18]],
                                    ['#RG#', _redorgreen]
                                ]);
                                //alert(_price);
                            }
                            /*_iHTML = '<ul>' +
                             '<li>成交:<i>' + sData[8] + '</i></li>' +
                             '<li>今开:<i>' + sData[4] + '</i></li>' +
                             '<li>最高:<i>' + sData[6] + '</i></li>' +
                             '<li>最低:<i>' + sData[7] + '</i></li>' +
                             '</ul>';*/
                        }
                        break;
                }

            } else {
                _tmpHTML = this._arrReplace(this._hqTpl, [
                    ['#TITLE#', this._linkTpl.replace('#URL#', this._makeUrl(this._linkUrl, aSet[3])).replace('#NAME#', aSet[1])],
                    ['#P#', '--'],
                    ['#CG#', '--'],
                    ['#CGP#', '--'],
                    ['#RG#', '']
                ]);
            }
            _iHTML = '<ul class="links">' + aSet[6] + '</ul>';

            //		加了箭头指示，缺陷是，箭头会一直存在，直至下一次价格有变动
            //		if (this._oSetNow[index].prePrice && this._oSetNow[index].prePrice - _price != 0) {
            //			var _arrow = this._oSetNow[index].prePrice - _price > 0 ? "↓" : "↑";
            //		}
            //		else {
            //			var _arrow = "";
            //		}
            //		_tmpHTML = _tmpHTML.replace("@AR@", _arrow);

            _aHTML = this._oSetNow[index].prePrice ? [_tmpHTML.replace(/#PBG#/g, ""), _tmpHTML.replace(/#PBG#/g, _redorgreen)] : [_tmpHTML.replace("#PBG#", "")];
            //var _aHTML = [_tmpHTML.replace(/@PBG@/g, ""), _tmpHTML.replace(/@PBG@/g, _redorgreen)];
            this._oSetNow[index].prePrice = _price;
            return { Stock: _aHTML, Info: _iHTML };
        },
        _setColorClass: function (value, rog) {//变色
            switch (rog) {
                case "redup":
                    return value != 0 ? (value > 0 ? "rup" : "gdown") : "";
                    break;
                case "greenup":
                    return value != 0 ? (value > 0 ? "gup" : "rdown") : "";
                    break;
            }
        },
        _arrReplace: function (targetTpl, dataArr) {//批量替换内容
            for (var i = 0; i < dataArr.length; i++) {
                targetTpl = targetTpl.replace(dataArr[i][0], dataArr[i][1]);
            }

            return targetTpl;
            //targetContainer.innerHTML = targetTpl;
        },
        _setHtml: function (sHtml, oContainer) {//插入HTML
            oContainer.innerHTML = sHtml;
        },
        _makeUrl: function (base, more) {//设置链接地址
            if (/^http/.test(more)) {
                return more;
            } else {
                return base + more;
            }
        },
        _loadImg: function (sID) {//加载并开启自刷新图片
            if (!hqData[this._sMarket].activeImg) {
                hqData[this._sMarket].activeImg = 0;
            }

            //滑动时回调，传sID进来，若图已初始化，则不强行读取，由更新线程更新
            if (sID != undefined) {
                hqData[this._sMarket].activeImg = sID;
                if (this._oSetNow[hqData[this._sMarket].activeImg].imgLoaded) return;
            }

            if (this._imgtimeout) {
                window.clearTimeout(this._imgtimeout);
            }
            this._imgtimeout = window.setTimeout(this._loadImg.Bind(this), this.imgFqc);

            var _index = hqData[this._sMarket].activeImg;

            if (this._oSetNow[_index][2] != "") {
                this._fillImg(this._oSetNow[_index], _index)
            }

            //表明已初始化
            if (!this._oSetNow[_index].imgLoaded) {
                this._oSetNow[_index].imgLoaded = true;
            }
        },
        _fillImg: function (aSet, index) {//加载图片
            var _oTemp = hqData[this._sMarket]._containersNow[index].nextSibling;
            var _container = _oTemp.nodeType == 1 ? _oTemp : _oTemp.nextSibling;
            _container = _container.firstChild;
            var _img = new Image();
            _img.src = [aSet[2], aSet[2].indexOf('?') >= 0 ? '&' : "?", 'r=' + (new Date()).getTime()].join("");
            if (_img.onreadystatechange) {
                _img.onreadystatechange = this.insertBc.Bind(this, _container, _img, aSet[3]);
            } else {
                _img.onload = this.insertBc.Bind(this, _container, _img, aSet[3]);
            }
        },
        insertBc: function (_container, _img, _haslink) {//像目标容器填充图片
            if (_img.onreadystatechange) {
                if (_img.readyState != 'loaded' && _img.readyState != 'complete') return;
            }
            _container.innerHTML = "";
            if (_haslink != "") {
                var _link = $C("A");
                _link.target = "_blank";
                _link.href = this._makeUrl(this._bfUrl, _haslink);
                _link.appendChild(_img);
                _container.appendChild(_link);
            } else {
                _container.appendChild(_img);
            }
            _img.height = 135;
            _img.width = 240;
        },
        refreshData: function () {
            //console.log("刷新数据");
            this._loadhq();
            this._loadImg();
        },
        getQueryString: function (uri, name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = uri.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        },
        _io: function (uri, cb) {//load 单个请求
            var _script = document.createElement("script");
            _script.type = "text/javascript";
            _script.charset = "utf-8";
            _script._fun = typeof cb != "undefined" ? cb : new Function();
            _script[document.all ? "onreadystatechange" : "onload"] = function () {
                if (document.all && this.readyState != "loaded" && this.readyState != "complete") { return; }
                this._fun(this);
                this._fun = null;
                this[document.all ? "onreadystatechange" : "onload"] = null;
                this.parentNode.removeChild(this);
            };
            _script.src = uri;
            var _ihead = document.getElementsByTagName("head").item(0);
            _ihead.appendChild(_script);
        },
        _ios: function (uris, cb) {//load 数组
            var hashTime = new Date().getTime(),
                uriHash = 'iosuris_' + hashTime,
                uriCount = uris.length,
                jsnames = [];
            var _jsTime = (new Date()).getTime();
            for (var i in uris) {
                jsnames.push(this.getQueryString(uris[i], "jsname"));
                this._io(uris[i].replace('{#RN#}', _jsTime), function (hash, uri) {
                    //var jsname = this.getQueryString(uri, "jsname");
                    //window[hash].push(uri);
                    uriCount--;
                }.Bind(this, uriHash, uris[i]));
            }
            var _checkUrls = setInterval(function () {
                //console.log((new Date().getTime() - _jsTime) + ":" + (this.jsFqc - 1000));
                if (new Date().getTime() - _jsTime >= this.jsFqc - 1000 || uriCount <= 0) {
                    clearInterval(_checkUrls);
                    if (typeof cb != "undefined") cb.Bind(this, jsnames)();
                }
                //if (typeof cb != "undefined" && uriCount <= 0) cb();
            }.Bind(this), 10);
        }
    };
    StockIndex.init();
    window.StockIndex = StockIndex;
})();