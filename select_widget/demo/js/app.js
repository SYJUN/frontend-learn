/*
 *功能：根据传入的数据  动态绘制图形
 *参数：{
 *   elementId：绘制图的标签id
 *   count：数据长度
 *   date：日期
 *   rate：显示在右侧Y轴数据
 *   rateName：右侧数据标题名称
 *   closePrice：显示在左侧Y轴数据
 *   closePriceName ：左侧数据标题名称
 *   minvalue：左侧数据的最小值，可不传值，默认去数据的最小值
 *
 * }
 *
 * */
function chartInfo(elementId,count,date,rate,rateName,closePrice,closePriceName,minvalue) {
    jQuery('#'+ elementId).highcharts({
        title: {
            text: '<b>历史投票结果</b>',
            style: { display: 'none' }
        },
        plotOptions: {
            series: {
                events: {       //数据对应条按钮点击事件
                    legendItemClick: function (event) {
                        return false;
                    }
                }
            }
        },
        xAxis: [
            {
                //设置X 轴网格线
                gridLineColor: '#ddd',
                gridLineWidth: 1,

                tickInterval: count > 3 ? parseInt(count/2) - 1 : 1,    // x 轴间隔显示的信息条数
                categories: date    //显示的信息数据

            }
        ],
        yAxis: [{       //右边 Y 轴
            title: {
                text: '',
                style: {
                    color: '#FF2D30'
                }
            },
            min: 0,
            labels: {
                format: '{value}%',
                style: {
                    color: '#FF2D30'
                }
            },
            opposite: true
        },{         //左边 Y 轴
            labels: {
                format: '{value}',
                style: {
                    color: '#008726'
                }
            },
            //min: minvalue,        //最小值不设置默认为数据的最小值 向下取整
            title: {
                text: '',
                style: {
                    color: '#008726'
                }
            }
        }
        ],
        tooltip: {
            shared: true
        },
        credits: {
            text: '',
            href: 'http://www.eastmoney.com'
        },
        legend: {
            //layout: 'vertical',
            align: 'center',
            verticalAlign: 'bottom',
            x: 0,
            y: 0,
            //floating: true,
            backgroundColor: '#FFFFFF'
        },
        series: [{
            name: rateName,
            type: 'spline',
            color: '#FF2D30',
            data: rate,
            tooltip: {
                valueSuffix: '%'
            }
        },{
            name: closePriceName,
            type: 'spline',
            color: '#008726',
            yAxis: 1,
            data: closePrice,
            tooltip: {
                valueSuffix: ''
            }
        }]
    });
}
function dataHandle(data,arg){
    var i,
        r = parseInt(arg[0]),
        p = parseInt(arg[1]),
        len = data.VoteResults.length,
        rateName = "",
        closePriceName = "",
        dateArr = [],
        closePriceArr = [],
        RateArr = [],
        rateNameArr = ["看涨比例","盘整比例","看跌比例","重仓比例","中等仓位比例","轻仓比例"],
        closePriceNameArr = ["上证指数","深成指数","创业板指数","沪深300"];

    rateName = rateNameArr[r];
    closePriceName = closePriceNameArr[p];
    for( i = 0;i < len;i++){
        dateArr.push(data.VoteResults[i].VoteDate);

        switch (r){
            case 0:
                RateArr.push(parseFloat(data.VoteResults[i].RiseRate) );
                rateName = rateNameArr[r];
                break;
            case 1:
                RateArr.push(parseFloat(data.VoteResults[i].FallRate) );
                rateName = rateNameArr[r];
                break;
            case 2:
                RateArr.push(parseFloat(data.VoteResults[i].ConsolidationRate) );
                rateName = rateNameArr[r];
                break;
            case 3:
                RateArr.push(parseFloat(data.VoteResults[i].HighPositionRate) );
                rateName = rateNameArr[r];
                break;
            case 4:
                RateArr.push(parseFloat(data.VoteResults[i].MidPositionRate) );
                rateName = rateNameArr[r];
                break;
            case 5:
                RateArr.push(parseFloat(data.VoteResults[i].LowPositionRate) );
                rateName = rateNameArr[r];
                break;
            default :
                RateArr.push(parseFloat(data.VoteResults[i].RiseRate) );
                rateName = rateNameArr[r];
                break;
        }
        switch (p){
            case 0:
                closePriceArr.push(parseFloat(data.VoteResults[i].SseTodayClosePrice));
                closePriceName = closePriceNameArr[p];
                break;
            case 1:
                closePriceArr.push(parseFloat(data.VoteResults[i].SzseTodayClosePrice));
                closePriceName = closePriceNameArr[p];
                break;
            case 2:
                closePriceArr.push(parseFloat(data.VoteResults[i].GemTodayClosePrice));
                closePriceName = closePriceNameArr[p];
                break;
            case 3:
                closePriceArr.push(parseFloat(data.VoteResults[i].Csi300TodayClosePrice));
                closePriceName = closePriceNameArr[p];
                break;
            default :
                closePriceArr.push(parseFloat(data.VoteResults[i].SseTodayClosePrice));
                closePriceName = closePriceNameArr[p];
                break;
        }
    }
    dateArr = dateArr.reverse();
    return {
        "count":data["Count"],
        "date":dateArr,
        "rate":RateArr,
        "rateName":rateName,
        "closePrice": closePriceArr,
        "closePriceName":closePriceName,
        "minvalue":parseInt(Math.min.apply(null,closePriceArr))
    }
}

var select = {
    voteId:"vote_result",
    contrastId:"contrast",
    voteFnValue:0,          //投票结果函数返回的值, 默认为0
    contrastIdFnValue:0,    //对比指数函数返回的值, 默认为0
    init:function(data){
        if(!this.voteId) return;
        if(!this.contrastId) return;
        this.voteFn(data);
        this.contrastIdFn(data);
        this.handleFn(data);
    },
    handleFn:function(data){
        var valueArr = [this.voteFnValue,this.contrastIdFnValue];
        var handle = dataHandle(data,valueArr);
        chartInfo(
            "chartInfo",
            handle.count,
            handle.date,
            handle.rate,
            handle.rateName,
            handle.closePrice,
            handle.closePriceName,
            handle.minvalue
        );
    },
    voteFn:function(data){
        var _t = this;
        $('#'+_t.voteId).SelectSimu({
            width : 100,
            zIndex : 2,
            listNum : 6,
            listValue : ['0','1','2','3','4','5'],
            listOption : ['看涨比例','盘整比例','看跌比例','重仓比例','中等仓位比例','轻仓比例'],
            imgSrc : 'img/icon_down.png',
            changeFn:function(value){
                _t.voteFnValue = value;
                _t.handleFn(data);
            }
        });
    },
    contrastIdFn:function(data){
        var _t = this;
        $('#'+ _t.contrastId).SelectSimu({
            width : 100,
            zIndex : 2,
            listNum : 4,
            listValue : ['0','1','2','3'],
            listOption : ['上证指数','深成指数','创业板指数','沪深300'],
            imgSrc : 'img/icon_down.png',
            changeFn:function(value){
                _t.contrastIdFnValue = value;
                _t.handleFn(data);
            }
        });
    }
};
select.init(tableData);
