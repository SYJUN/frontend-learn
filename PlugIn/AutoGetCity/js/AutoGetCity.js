;
(function ($, doc) {
    var citySelector = {
        cities: [
            ["北京", "北京|东城|西城|崇文|宣武|朝阳|丰台|石景山|海淀|门头沟|房山|通州|顺义|昌平|大兴|平谷|怀柔|密云|延庆"],
            ["上海", "上海|黄浦|卢湾|徐汇|长宁|静安|普陀|闸北|虹口|杨浦|闵行|宝山|嘉定|浦东|金山|松江|青浦|南汇|奉贤|崇明"],
            ["天津", "和平|东丽|河东|西青|河西|津南|南开|北辰|河北|武清|红挢|塘沽|汉沽|大港|宁河|静海|宝坻|蓟县"],
            ["重庆", "万州|涪陵|渝中|大渡口|江北|沙坪坝|九龙坡|南岸|北碚|万盛|双挢|渝北|巴南|黔江|长寿|綦江|潼南|铜梁|大足|荣昌|壁山|梁平|城口|丰都|垫江|武隆|忠县|开县|云阳|奉节|巫山|巫溪|石柱|秀山|酉阳|彭水|江津|合川|永川|南川"],
            ["河北", "石家庄|邯郸|邢台|保定|张家口|承德|廊坊|唐山|秦皇岛|沧州|衡水"],
            ["山西", "太原|大同|阳泉|长治|晋城|朔州|吕梁|忻州|晋中|临汾|运城"],
            ["内蒙古", "呼和浩特|包头|乌海|赤峰|呼伦贝尔|通辽|乌兰察布|鄂尔多斯|巴彦淖尔"],
            ["辽宁", "沈阳|大连|鞍山|抚顺|本溪|丹东|锦州|营口|阜新|辽阳|盘锦|铁岭|朝阳|葫芦岛"],
            ["吉林", "长春|吉林|四平|辽源|通化|白山|松原|白城"],
            ["黑龙江", "哈尔滨|齐齐哈尔|牡丹江|佳木斯|大庆|绥化|鹤岗|鸡西|黑河|双鸭山|伊春|七台河"],
            ["江苏", "南京|镇江|苏州|南通|扬州|盐城|徐州|连云港|常州|无锡|宿迁|泰州|淮安"],
            ["浙江", "杭州|宁波|温州|嘉兴|湖州|绍兴|金华|衢州|舟山|台州|丽水"],
            ["安徽", "合肥|芜湖|蚌埠|马鞍山|淮北|铜陵|安庆|黄山|滁州|宿州|池州|淮南|巢湖|阜阳|六安|宣城|亳州"],
            ["福建", "福州|厦门|莆田|三明|泉州|漳州|南平|龙岩|宁德"],
            ["江西", "南昌|景德镇|九江|鹰潭|萍乡|新馀|赣州|吉安|宜春|抚州|上饶"],
            ["山东", "济南|青岛|淄博|枣庄|东营|烟台|潍坊|济宁|泰安|威海|日照|莱芜|临沂|德州|聊城|滨州|菏泽"],
            ["河南", "郑州|开封|洛阳|平顶山|安阳|鹤壁|新乡|焦作|濮阳|许昌|漯河|三门峡|南阳|商丘|信阳|周口|驻马店|济源"],
            ["湖北", "武汉|宜昌|荆州|黄石|荆门|黄冈|十堰|随州|咸宁|孝感|鄂州|襄阳"],
            ["湖南", "长沙|常德|株洲|湘潭|衡阳|岳阳|邵阳|益阳|娄底|怀化|郴州|永州|张家界"],
            ["广东", "广州|深圳|珠海|汕头|东莞|中山|佛山|韶关|江门|湛江|茂名|肇庆|惠州|梅州|汕尾|河源|阳江|清远|潮州|揭阳|云浮"],
            ["广西", "南宁|柳州|桂林|梧州|北海|崇左|来宾|防城港|钦州|贵港|玉林|贺州|百色|河池"],
            ["海南", "海口|三亚|三沙"],
            ["四川", "成都|绵阳|自贡|攀枝花|泸州|德阳|广元|遂宁|内江|乐山|资阳|宜宾|南充|达川|雅安|巴中|广安|眉山"],
            ["贵州", "贵阳|六盘水|遵义|安顺|铜仁|毕节"],
            ["云南", "昆明|昭通|曲靖|玉溪|普洱|保山|丽江|临沧"],
            ["西藏", "拉萨"],
            ["陕西", "西安|铜川|宝鸡|咸阳|渭南|汉中|安康|商洛|延安|榆林"],
            ["甘肃", "兰州|嘉峪关|金昌|白银|天水|酒泉|张掖|武威|定西|陇南|平凉|庆阳"],
            ["宁夏", "银川|石嘴山|吴忠|固原|中卫"],
            ["青海", "西宁|海东"],
            ["新疆", "乌鲁木齐|克拉玛依"],
            ["其它", "澳门|香港|台湾"]
        ],
        hotCities: ["北京", "上海", "广州", "深圳"]
    };

    //创建 自动获取城市的类
    /*
     *
     * 参数 ：{
     *             city ： 获取城市数据信息
     *             hotCity ：获取热门城市数据信息
     *             showHotCity ：是否显示热门城市区域
     *             autoLocate ： 是否自动定位城市
     *             effect ：展示的效果，可传两个值[fade,slide]，默认效果为：default
     *       }
     * author:焰
     * starTime：2016/06/14
     * versions:1.0
     * */

    var AutoGetCity = (function () {
        function AutoGetCity(element, settings) {
            this.settings = $.extend(true, $.fn.AutoGetCity.default, settings || {});
            this.element = element;
            if (this.settings.cities && this.element) {
                this.init();
            }
        }

        AutoGetCity.prototype = {
            init: function () {
                var _t = this;
                this.options = {
                    AutoGetCityId: 'AutoGetCity', 
                    AutoGetCityCName: 'citiesBox',
                    hotCityCName: 'hotCity',
                    hotCitiesCName: 'hotCities',
                    seleCityCName: 'selector_cityName',
                    provencesCName: 'provences',
                    provenceListCName: 'provenceList',
                    citiesItemsCName: 'provName',
                    citiesCName: 'cities',
                    curbgCName: 'curbg'
                };
                //创建选择城市内容盒子
                this.$citiesBox = $('<div id="' + this.options.AutoGetCityId + '" class="' + this.options.AutoGetCityCName + '"><b class="triangle_up bg_a"></b><b class="triangle_up bg_b"></b><span class="closeBtn">关闭</span></div>');

                //渲染页面内容
                this.render();
                this.$autoGetCity = $('#' + this.options.AutoGetCityId);
                this.$autoGetCity.hide();
                this.element.on('click', function () {
                    var _top = $(this).offset().top + 40,
                        _left = $(this).offset().left,
                        _winWidth = $(doc).width();
                    if (_winWidth - _left > 0) {
                        _t.$autoGetCity.css({'top': _top, 'left': _left});
                    }
                    _t.showCitiesList(_t.$autoGetCity, _t.settings.effect);
                });
                if (this.settings.autoLocate) this.autoPosition();

                this.provencesItemEvent();

                $(doc).click(function (e) {
                    e.stopPropagation();
                    if (!$('#' + _t.options.AutoGetCityId).is(':hidden')) {
                        if (_t.$currentCity && _t.$currentCity.hasClass(_t.options.curbgCName)) {
                            _t.hideCitiesList(_t.$currentCities, _t.settings.effect);
                            _t.$currentCity.removeClass(_t.options.curbgCName);
                        }
                    }
                });
            },
            //页面渲染函数
            render: function () {
                //热门城市
                if (this.settings.showHotCity && this.settings.hotCity) {
                    var hotcityHtml = '<div class="' + this.options.hotCityCName + '"><h4>热门城市</h4>' + this.createCitiesList(this.settings.hotCity, this.options.hotCitiesCName, this.options.seleCityCName) + '</div>';
                    this.$citiesBox.append(hotcityHtml);
                }
                //省份区域
                var provencesData = this.settings.cities,
                    i, len = provencesData.length,
                    provencesArr = [],         //记录省份
                    citiesArr = [];            //记录所有城市
                for (i = 0; i < len; i++) {
                    provencesArr.push(provencesData[i][0]);
                    citiesArr.push(provencesData[i][1]);
                }
                this.prov_cities = [];      //将所有城市分类处理
                for (i = 0; i < citiesArr.length; i++) {
                    var curItem = citiesArr[i];
                    if (curItem.indexOf('|') > 0) {
                        this.prov_cities.push(curItem.split('|'));
                    } else {
                        var saveArr = [];
                        saveArr.push(curItem);
                        this.prov_cities.push(saveArr);
                    }
                }
                var $provencesListHtml = '<div class="' + this.options.provencesCName + '"><h4>选择省份和城市</h4>' + this.createProvenceList(provencesArr, this.options.provenceListCName, this.options.citiesItemsCName) + '</div>';

                this.$citiesBox.append($provencesListHtml);
                $('body').append(this.$citiesBox);
            },
            //生成城市列表函数
            createCitiesList: function (citiesData, ulcName, licName) {
                var i = 0,
                    len = citiesData.length,
                    strHtml = '<ul class="' + ulcName + '">';
                for (; i < len; i++) {
                    strHtml += '<li class="' + licName + '">' + citiesData[i] + '</li>';
                }
                return strHtml + '</ul>';
            },
            createProvenceList: function (provenceData, ulcName, licName) {
                var i = 0,
                    len = provenceData.length,
                    strHtml = '<ul class="' + ulcName + '">';
                for (; i < len; i++) {
                    strHtml += '<li class="' + licName + '" data-provNumber="' + i + '"><span data-nowItem="false">' + provenceData[i] + '</span>' +
                    this.createCitiesList(this.prov_cities[i], this.options.citiesCName, this.options.seleCityCName) + '</li>';
                }
                return strHtml + '</ul>';
            },
            //选择省份事件函数
            provencesItemEvent: function () {
                var _t = this,
                    $citiesItem = $('.' + this.options.provencesCName).find('.' + this.options.citiesItemsCName);
                $('.' + this.options.citiesCName).hide();

                this.closeFn();
                if (this.settings.hotCity) {
                    this.citiesItemEvent($('.' + this.options.hotCitiesCName));
                }
                $citiesItem.on('click', 'span', function (e) {
                    e.stopPropagation();
                    $(this).attr('data-nowitem', 'true').parent().siblings().find('span').attr('data-nowitem', 'false');
                    $(this).addClass(_t.options.curbgCName).parent().siblings().find('span').removeClass(_t.options.curbgCName);
                    if ($(this).attr('data-nowitem')) {
                        _t.$currentCities = $(this).siblings('.' + _t.options.citiesCName);
                        _t.$currentCity = $(this);
                        _t.showCitiesList(_t.$currentCities, _t.settings.effect);
                        _t.hideCitiesList($(this).parent().siblings().find('.' + _t.options.citiesCName), _t.settings.effect);
                        _t.citiesItemEvent(_t.$currentCities);
                    }
                });
            },
            //关闭按钮
            closeFn: function () {
                var _t = this;
                $('#' + this.options.AutoGetCityId).find('.closeBtn').bind('click', function () {
                    _t.hideCitiesList(_t.$autoGetCity, _t.settings.effect);
                    if (_t.$currentCity && _t.$currentCity.hasClass(_t.options.curbgCName)) {
                        _t.$currentCity.removeClass(_t.options.curbgCName);
                        _t.hideCitiesList(_t.$currentCities, _t.settings.effect);
                    }
                });
            },
            //显示
            showCitiesList: function ($city, effect) {
                switch (effect) {
                    case 'fade':
                        return $city.fadeIn();
                    case 'slide':
                        return $city.slideDown();
                    default:
                        return $city.show();
                }
            },
            //隐藏
            hideCitiesList: function ($city, effect) {
                switch (effect) {
                    case 'fade':
                        return $city.fadeOut();
                    case 'slide':
                        return $city.slideUp();
                    default:
                        return $city.hide();
                }
            },
            //城市列表子项事件函数
            citiesItemEvent: function ($city) {
                var _t = this;
                $city.on('click', '.' + this.options.seleCityCName, function (e) {
                    e.stopPropagation();
                    _t.fillCityText($(this).text());
                });
            },
            fillCityText: function (content) {
                this.element.val(content);
                this.hideCitiesList(this.$autoGetCity, this.settings.effect);

                if (this.$currentCity && this.$currentCity.hasClass(this.options.curbgCName)) {
                    this.$currentCity.removeClass(this.options.curbgCName);
                    this.hideCitiesList(this.$currentCities, this.settings.effect);
                }
            },
            //自动定位
            autoPosition: function () {
                var _t = this;
                var url = 'http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js';
                this.loadScript(url, function () {
                    var myprovince = remote_ip_info['province'];          //省份
                    var mycity = remote_ip_info['city'];                  //城市
                    var mydistrict = remote_ip_info['district'];          //区县
                    _t.element.val(mycity);
                });
            },
            //加载外部
            loadScript: function (url, callback) {
                var script = doc.createElement('script');
                script.type = 'text/javascript';
                if (script.readyState) {
                    script.onreadystatechange = function () {
                        if (script.readyState == 'loaded' || script.readyState == 'complete') {
                            script.onreadystatechange = null;
                            if (callback) callback();
                        }
                    };
                } else {
                    script.onload = function () {
                        if (callback) callback();
                    };
                }
                script.src = url;
                doc.getElementsByTagName('head')[0].appendChild(script);
            }
        };
        return AutoGetCity;
    })();

    $.fn.AutoGetCity = function (options) {
        return this.each(function () {
            var $this = $(this),
                instance = $this.data('AutoGetCity');
            if(!instance){
                instance = new AutoGetCity($this, options);
                $this.data('AutoGetCity',instance);
            }
        });
    };
    $.fn.AutoGetCity.default = {
        cities: citySelector['cities'],
        hotCity: citySelector['hotCities'],
        showHotCity: true,
        autoLocate: true,
        effect: 'fade' //slide  fade
    };
})(jQuery, document);


