//返回元素计算后的值
function computerStyle( element ) {
    var style = null;
    if (window.getComputedStyle) {
        style = window.getComputedStyle(element);
    } else {
        style = element.currentStyle;
    }
    return style;
}

/*  自定义getElementsByClassName方法
 *根据元素className得到元素集合
 *@param    fatherId 父元素的ID ，默认为document
 *@param    tagName 子元素的标签名
 *@param    className 用空格分开的className 字符串
 * */
function getElementsByClassName( fatherId, tagName, cName ) {
    var node = fatherId && document.getElementById(fatherId) || document;
    tagName = tagName || "*";
    cName = cName.split(" ");
    var classNameLength = cName.length;
    var len = null;
    var i = null;
    for (i = 0, len = classNameLength; i < len; i++) {
        //创建匹配类名的正则
        cName[i] = new RegExp("(^|\\s)" + cName[i].replace(/\-/g, "\\-") + "(\\s|$)");
    }
    var elements = node.getElementsByTagName(tagName);
    var result = [];
    var n = 0;
    for (i = 0, len = elements.length; i < len; i++) {
        var element = elements[i];
        while (cName[n++].test(element.className)) {    //优化循环
            if (n === classNameLength) {
                result[result.length] = element;
                break;
            }
        }
        n = 0;
    }
    return result;
}


//轮播
var Slider = (function () {
    //轮播动画构造函数
    function Slider( adv ) {
        this.WIDTH = adv.width;                  //每个广告li的宽度
        this.duration = adv.duration;           //每次滚动的总时长
        this.interval = adv.interval;           //滚动动画每一步的时间间隔
        this.wait = adv.wait;                    //自动轮播时，每个广告轮播的时间间隔；每次启动新动画，都要先停旧动画

        this.timer = null;                       //记录当前正在轮播的动画的序号
        this.canAuto = true;                    //是否可以执行自动轮播
        this.imgList = adv.imgList;             //要轮播的图片ul
        this.circleList = adv.circleList;      //广告序号的ul

        this.btn_prev = adv.btn_prev;
        this.btn_next = adv.btn_next;
    }

//初始化
    Slider.prototype.init = function ( wrap ) {
        var self = this;
        var len = arrImgs.length;
        //获取到所有的图片，并且动态设置其宽度
        self.imgList.style.width = self.WIDTH * len + "px";
        //获取所有的图片序号,并动态添加到页面
        var i, indexs = [];
        for (i = 0; i < len; i++) {
            indexs[i] = i + 1;
        }
        self.circleList.innerHTML = '<li class="on">' + indexs.join("</li><li>") + '</li>';

        var ULwidth = i * ( parseFloat(computerStyle(self.circleList.getElementsByTagName("li")[0]).width) + parseFloat(computerStyle(self.circleList.getElementsByTagName("li")[0]).marginLeft) * 2 );
        //var marginLeft = -self.circleList.clientWidth/2;  //有误差

        var marginLeft = -ULwidth / 2;
        self.circleList.style.marginLeft = marginLeft + "px";

        //刷新一次页面
        self.updateView();
        //为每个序号按钮绑定mouseover事件
        self.circleList.onmouseover = function () {
            var e = window.event || arguments[0];
            var target = e.srcElement || e.target;
            //目标元素的 innerHTML-1(索引值) 与图片的ID值 始终是一一对应的，也就是说不管图片在数组的什么位置，id值都是与序号按钮的 innerHTML-1(索引值) 相互对应
            if (target.nodeName == "LI" && target.innerHTML - 1 != arrImgs[0].imgId) {
                //self.circleList.getElementsByClassName("on")[0].className = "";           //IE9(包括IE9)以下 不支持  getElementsByClassName()方法
                getElementsByClassName(self.circleList, "li", "on")[0].className = "";    //自定义 getElementsByClassName() 函数  兼容IE低版本
                self.circleList.getElementsByTagName("li")[arrImgs[0].imgId].className = "on";

                //n = 新位置 - 原位置
                self.move(target.innerHTML - 1 - arrImgs[0].imgId);
            }
        };
        wrap.onmouseover = function () {
            self.canAuto = false;
            self.btn_prev.style.display = "block";
            self.btn_next.style.display = "block";
        };
        wrap.onmouseout = function () {
            self.canAuto = true;
            self.btn_prev.style.display = "none";
            self.btn_next.style.display = "none";

        };
        self.startAutoMove();

    };
//更新数组
    Slider.prototype.updateView = function () {
        var self = this;
        self.btn_prev.style.display = "none";
        self.btn_next.style.display = "none";
        var i, lis = [], len = arrImgs.length;
        for (i = 0; i < len; i++) {
            lis[i] = '<li><a href="' + arrImgs[i].imgLinkUrl + '"><img src="' + arrImgs[i].imgUrl + '" title="' + arrImgs[i].imgTitle + '"/></a></li>';
        }
        self.imgList.innerHTML = lis.join("");
        //删除旧index上的on类
        getElementsByClassName(self.circleList, "li", "on")[0].className = "";
        self.circleList.getElementsByTagName("li")[arrImgs[0].imgId].className = "on";

        //左右按钮点击事件
        self.btn_prev.onclick = function () {
            self.move(-1);
        };
        self.btn_next.onclick = function () {
            self.move(1);
        };
    };
//移动方法:将ul移动n个位置，  左移：先移动，再调换元素的位置    右移：先换位置，再移动
    Slider.prototype.move = function ( n ) {
        var self = this;
        //先停止正在播放的动画
        clearTimeout(self.timer);
        self.timer = null;

        if (n < 0) {              //n<0 向右移动
            //从倒数-n开始删除-n个元素，保存在dels中，将剩余arrImgs拼接到dels结尾，在保存回arrImgs中
            var dels = arrImgs.splice(arrImgs.length - (-n), -n);
            arrImgs = dels.concat(arrImgs);
            //再次刷新页面
            self.updateView();
            self.imgList.style.left = self.WIDTH * n + "px";
        }
        self.moveStep(n);
    };
//反复移动一步
    Slider.prototype.moveStep = function ( n ) {
        var self = this;
        //计算每步距离
        var step = self.WIDTH * n / (self.duration / self.interval);
        var left = parseFloat(computerStyle(self.imgList).left) - step;
        self.imgList.style.left = left + "px";

        if ((n < 0 && left < 0) || (n > 0 && left > self.WIDTH * (-n))) {
            self.timer = setTimeout(function () {
                self.moveStep(n);
            }, self.interval);
        } else {
            if (n > 0) {
                //从数组开头位置删除n个元素，保存在dels中，将dels拼接到arrImgs结尾，再存回arrImgs中
                var dels = arrImgs.splice(0, n);
                arrImgs = arrImgs.concat(dels);
                self.updateView();
            }
            //每次移动都需要将self的imgList 的 left 重置为 0
            self.imgList.style.left = 0;
            //只要手动轮播停止，就启动自动轮播
            self.startAutoMove();
        }
    };
    Slider.prototype.startAutoMove = function () {
        var self = this;
        self.timer = setTimeout(function () {
            if (self.canAuto) {
                self.move(1);
            } else {
                self.startAutoMove();
            }
        }, self.wait);
    };

    return Slider;
})();

