angular
		.module('pagingApp')
		.controller('pagingController',pagingController);

		pagingController.$inject = ['$http'];
		pagingController.$inject = ['pagingService'];

		function pagingController($http,pagingService){
		//function pagingController($http){
			var vm = this;
			vm.totalItems = 0;
			vm.currentPage = 1;
			vm.pageSzie = 10
			vm.comments = [];
			vm.pageChanged = getComments;
			getComments();			
			function getComments(){
				var postData = {
					pageIndex: vm.currentPage,
					pageSize:vm.pageSzie
				};		
				pagingService($http).getComments(postData).then(function(response){
					vm.comments = response.data.Results;
					vm.totalItems = response.data.totalItems;
				},function(response){
					
				}).finally();

				/*
				$http({
					url: "data.php",
					method: "POST", 
					data:postData,
					headers: {
					"Content-Type": "application/json; charset=utf-8"
					}
				}).then(function(response){
					vm.comments = response.data.Results;
					vm.totalItems = response.data.totalItems;
				},function(response){
					
				}).finally();
				*/


			}
		}