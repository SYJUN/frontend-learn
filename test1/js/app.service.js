angular
		.module('pagingApp')
		.factory('pagingService', pagingService);

	pagingService.$inject = ['$http'];
	function pagingService($http){

		function getComments(postData){
			return $http({
				url: "data.php",
				dataType: "json",
				method: "POST", 
				data:postData,
				headers: {
					"Content-Type": "application/json; charset=utf-8"
				}
			});
		}
		return{
			getComments: getComments
		}
		// var service = {
		// 	getComments : getComments
		// }
		// function getComments(postData){
		// 	return $http({
		// 		url: "data.php",
		// 		dataType: "json",
		// 		method: "POST", 
		// 		data:postData,
		// 		headers: {
		// 			"Content-Type": "application/json; charset=utf-8"
		// 		}
		// 	});
		// }
		// return service;

	}