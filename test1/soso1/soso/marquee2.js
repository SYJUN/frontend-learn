$(function(){

	var boxWidth = $("#marquee").width()+ "px";
	var leftWidth = - $("#text1").width() + "px";
	var num = 15;
	function init(){
		$("#text1").css("left",boxWidth);

		$("#text1").stop().animate({left: leftWidth},15000,"linear",switchover);
	}
	init();

	function switchover(){
		timer=setTimeout(calc,1000);
		$("#text1").css("left",boxWidth);
	}
	function calc(){
		if(num-->0){
			$("#text1").hide();
			$("#text2").fadeIn();
			timer=setTimeout(calc,1000);
		}else{
			$("#text1").show();
			$("#text2").fadeOut();
			num = 15;
			$("#text1").stop().animate({left: leftWidth},15000,"linear",switchover);
		}
	}

})
