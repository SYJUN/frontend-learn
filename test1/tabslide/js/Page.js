/**
 * Created by Administrator on 2016/3/3.
 */
//最新专题展示动画类
function ListLayOut(array, picobj, ulobj) {
    var index = 0;
    var isloop = true;
    var stoptime = 1;
    var lilist = new Array(array.length);
    var curimglink;

    this.array = array;
    this.picobj = picobj;
    this.ulobj = ulobj;

    this.DefineArea = function() {
        var me = this;
        me.picobj.innerHTML = "";
        me.picobj.appendChild(me.SetPic());

        me.ulobj.innerHTML = "";
        for (var i = 0; i < me.array.length; i++) {
            lilist[i] = me.SetLi(i);
            me.ulobj.appendChild(lilist[i]);
        }
    }

    this.Loop = function() {
        if (isloop) {
            var me = this;
            if (index > me.array.length - 1) index = 0;
            me.Change();
            index++;
        }
        else {
            stoptime++;
            if (stoptime > 3) {
                stoptime = 1;
                isloop = true;
            }
        }
    }

    this.Change = function() {
        curimglink.href = array[index][1];
        curimglink.title = array[index][2]
        var img = curimglink.firstChild;
        img.alt = array[index][2];
        img.src = array[index][0];

        for (var j = 0; j < lilist.length; j++)
            lilist[j].className = "";

        lilist[index].className = "check";
    }

    this.SetPic = function() {
        var img = document.createElement("img");
        img.alt = array[index][2];
        img.src = array[index][0];
        var link = document.createElement("a");
        link.setAttribute("target", "_blank");
        link.href = array[index][1];
        link.title = array[index][2];
        link.appendChild(img);
        curimglink = link;

        return link;
    }

    this.SetLi = function(i) {
        var link = document.createElement("a");
        link.setAttribute("target", "_blank");
        link.href = array[i][1];
        link.innerHTML = array[i][2];
        var li = document.createElement("li");
        if (i == index) li.className = "check";
        li.appendChild(link);

        li.onmouseover = function() {
            index = i;
            isloop = false;

            curimglink.href = array[index][1];
            curimglink.title = array[index][2]
            var img = curimglink.firstChild;
            img.alt = array[index][2];
            img.src = array[index][0];

            for (var j = 0; j < lilist.length; j++)
                lilist[j].className = "";

            lilist[index].className = "check";
        }

        return li;
    }
}