/**
 * Created by mac on 2017/6/6.
 */

(function(){
    var btn  = document.getElementById('btn');
    var show_wrap = document.getElementById('demo');
    var span = show_wrap.getElementsByTagName('span')[0];
    var index = 0;

    btn.onclick = function(e) {
        e.stopPropagation();
        var classname = show_wrap.className;
        index++;
        if (classname.indexOf('d1') < 0) {
            show_wrap.className = 'demo d1';
        } else {
            show_wrap.className = 'demo d2'
        }

        span.innerText = index;
        // console.log(show_wrap.className)
    };
})();