;
(function ($, wd, doc) {
    var MobileSlide = function (options) {
        var self = this;
        self.settings = {
            speed: 200,
            maxWidth: $(window).width(),
            maxHeight: $(window).height(),
            maskOpacity: 0.5
        };
        $.extend(self.settings, options || {});
        //创建遮罩层和弹出框
        self.popupMask = $('<div id="M_slide_mask"></div>');
        self.popupWin = $('<div id="M_slide_popup"></div>');

        //设置遮罩层的透明度
        self.popupMask.css({
            opacity: self.settings.maskOpacity,
            filter: 'alpha(opacity=' + (self.settings.maskOpacity) + ')'
        });

        //保存body
        self.bodyNode = $(doc.body);
        //渲染剩余的DOM，并且插入到body
        self.renderDOM();

        //图片预览区域
        self.picViewArea = self.popupWin.find('div.slide_pic_view');
        //图片
        self.popupPic = self.popupWin.find('img.slide_image');

        //定义组名
        self.groupName = null;
        //定义一个空数组，用来存储组名数据
        self.groupData = [];//只能放置同一组数据
        //事件委托，获取数组数据
        self.bodyNode.on('click', '.js_Mslide,*[data-role=Mslide]', function (e) {
            e.stopPropagation();
            var currentGroupName = $(this).attr('data-group');
            if (currentGroupName != self.groupName) {
                self.groupName = currentGroupName;
                self.getGroup();
                self.MobileEvent();
            }
            //初始化弹出
            self.initPopup($(this));
        });
        this.popupMask.click(function () {
            $(this).fadeOut();
            self.popupWin.fadeOut();
        });
        this.moveLeft = false;
        this.moveRight = false;

    };
    MobileSlide.prototype = {
        initPopup: function (cur) {
            var self = this,
                sourceSrc = cur.attr('data-source'),
                curId = cur.attr('data-id');
            self.showMaskAndPopup(sourceSrc, curId);
        },
        showMaskAndPopup: function (sourceSrc, curId) {
            var self = this;
            self.popupPic.hide();
            var winW = $(wd).width(),
                winH = $(wd).height(),
                maxW = self.settings.maxWidth < winW ? this.settings.maxWidth : winW,
                maxH = self.settings.maxHeight < winH ? self.settings.maxHeight : winH;
            var w = (maxW || winW) / 2,
                h = (maxH || winH) / 2;
            self.picViewArea.css({
                width: w,
                height: h
            });
            self.popupMask.fadeIn();
            self.popupWin.fadeIn();

            var topAnimate = (winH - h) / 2;
            self.popupWin.css({
                width: w,
                height: h,
                marginLeft: -w / 2,
                top: -h
            });
            //加载图片
            self.loadPicSize(sourceSrc);

            //根据当前点击的元素ID获取在当前组别里面的索引
            self.index = self.getIndexOf(curId);
            var groupDataLength = self.groupData.length;
            if(groupDataLength > 1){
                if(self.index === 0) {
                    self.moveLeft = true;
                    self.moveRight = false;
                }else if(self.index === groupDataLength-1){
                    self.moveLeft = false;
                    self.moveRight = true;
                }else{
                    self.moveLeft = true;
                    self.moveRight = true;
                }
            }else{
                self.moveLeft = false;
                self.moveRight = false;
            }

        },
        loadPicSize: function (sourceSrc) {
            var self = this;
            self.popupPic.css({width: 'auto', height: 'auto'}).hide();
            self.preLoadImg(sourceSrc, function () {
                self.popupPic.attr('src', sourceSrc);
                var picW = self.popupPic.width(),
                    picH = self.popupPic.height();
                self.changePic(picW, picH);
            });
        },
        changePic: function (w, h) {
            var self = this,
                winWidth = $(window).width(),
                winHeight = $(window).height();

            var maxW = this.settings.maxWidth < winWidth ? this.settings.maxWidth : winWidth,
                maxH = this.settings.maxHeight < winHeight ? this.settings.maxHeight : winHeight;
            //如果图片的宽高大于浏览器市口的宽高比例，就看是否溢出
            var scale = Math.min((maxW || winWidth) / w, (maxH || winHeight) / h, 1);
            w = w * scale;
            h = h * scale;

            self.picViewArea.animate({
                width: w,
                height: h
            }, self.settings.speed);
            var top = (winHeight - h) / 2;

            self.popupWin.animate({
                width: w,
                height: h,
                marginLeft: -(w / 2),
                top: top
            }, self.settings.speed, function () {
                self.popupPic.css({
                    width: w,
                    height: h
                }).fadeIn();
            });

        },
        preLoadImg: function (src, callback) {
            var img = new Image();
            if (!!window.ActiveXObject) {     //IE
                img.onreadystatechange = function () {
                    if (this.readyState == 'complete') {
                        callback();
                    }
                }
            } else {
                img.onload = function () {
                    callback();
                }
            }
            img.src = src;
        },
        getIndexOf: function (curId) {
            var index = 0;
            $(this.groupData).each(function (i) {
                index = i;
                if (this.id === curId) return false;
            });
            return index;
        },
        getGroup: function () {
            var self = this;
            //根据当前的组别名称获取页面中所有相同组别的对象
            var groupList = this.bodyNode.find('*[data-group=' + this.groupName + ']');
            //清空数组数据
            self.groupData.length = 0;
            groupList.each(function () {
                self.groupData.push({
                    src: $(this).attr('data-source'),
                    id: $(this).attr('data-id'),
                    caption: $(this).attr('data-caption')
                });
            });

        },
        renderDOM: function () {
            var self = this;
            var strDom = '<div class="slide_pic_view"><img src="" alt="" class="slide_image"/></div>';
            self.popupWin.html(strDom);
            self.bodyNode.append(self.popupMask, self.popupWin);
        },
        goTo: function (dir) {
            var src;
            if(dir === 'next'){//move left
                this.index++;
                if(this.index >= this.groupData.length - 1){
                    this.moveLeft = false;
                }
                if(this.index != 0){
                    this.moveRight = true;
                }
                src = this.groupData[this.index].src;
                this.loadPicSize(src);
            }else if(dir === 'prev') {//move right
                this.index--;
                if (this.index <= 0) {
                    this.moveRight = false;
                }
                if (this.index != this.groupData.length - 1) {
                    this.moveLeft = true;
                }
                src = this.groupData[this.index].src;
                this.loadPicSize(src);
            }
        },
        MobileEvent:function(){
            var self = this;
            //触控事件坐标变量
            var startx = 0;
            var starty = 0;
            var endx = 0;
            var endy = 0;
            var documentWidth = wd.screen.availWidth;
            doc.addEventListener('touchstart', function (event) {
                startx = event.touches[0].pageX;
                starty = event.touches[0].pageY;
            });
            doc.addEventListener('touchmove', function (event) {
                event.preventDefault();
            });
            doc.addEventListener('touchend', function (event) {
                endx = event.changedTouches[0].pageX;
                endy = event.changedTouches[0].pageY;

                var deltax = endx - startx;
                var deltay = endy - starty;

                if (Math.abs(deltax) < 0.1 * documentWidth && Math.abs(deltay) < 0.1 * documentWidth) return;

                if (Math.abs(deltax) >= Math.abs(deltay)) {
                    if (deltax > 0 ) {
                        //move right
                        //console.log('right')
                        if(self.moveRight) self.goTo('prev');
                    } else {
                        //move left
                        //console.log('left')
                        if(self.moveLeft) self.goTo('next');
                    }
                } else {
                    if (deltay > 0) {
                        //move down
                        //console.log('down')
                    } else {
                        //move up
                        //console.log('up')
                    }
                }
            });
        }
    };

    wd["MobileSlide"] = MobileSlide;


})(jQuery, window, document);





















