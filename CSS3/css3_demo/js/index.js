;(function($){
    $(function(){
        var $headImg = $('#team_img_head'),
            $team_modal = $('#team_modal'),
            $closeBtn = $team_modal.find('.close');

        $headImg.find('li').on('mouseover',function(){
            $(this).find('.team_bg').addClass('forwards1').removeClass('forwards2');
        }).on('mouseleave',function(){
            $(this).find('.team_bg').addClass('forwards2').removeClass('forwards1');
        });

        $headImg.on('click','li',function(){
            var $index = $(this).index();
            $team_modal.show().find('.modal_cont_item').eq($index).show().siblings('.modal_cont_item').hide();
            return false;
        });
        $closeBtn.on('click',function(){
            $team_modal.hide().find('.modal_cont_item').hide();
        });
    });
})(jQuery);











